import 'dart:ui';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<bool> _selections = List.generate(3, (_) => false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              height: 100,
              child: Stack(
                children: [
                  Image.asset('assets/images/snowman.png'),
                  Positioned.fill(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 5,
                        sigmaY: 5,
                      ),
                      child: Container(
                        color: Colors.black.withOpacity(0),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ShaderMask(
              shaderCallback: (bounds) => RadialGradient(
                center: Alignment.topLeft,
                radius: 1.0,
                colors: [Colors.yellow, Colors.deepOrange],
                tileMode: TileMode.mirror,
              ).createShader(bounds),
              child: Text(
                'AppVesto',
                style: TextStyle(
                  fontSize: 50,
                  color: Colors.white,
                ),
              ),
            ),
            ToggleButtons(
              children: [Icon(Icons.store), Icon(Icons.smartphone), Icon(Icons.search)],
              isSelected: _selections,
              onPressed: (int index) {
                setState(
                  () {
                    _selections[index] = !_selections[index];
                  },
                );
              },
              color: Colors.grey,
              selectedColor: Colors.deepOrange,
            ),
            SizedBox(
              height: 200,
              child: Center(
                child: ListWheelScrollView(
                  itemExtent: 50,
                  children: List.generate(
                    50,
                    (index) => Container(
                      height: 300,
                      width: double.infinity,
                      color: Colors.blue,
                      child: Center(child: Text('$index')),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 200,
              child: DraggableScrollableSheet(builder: (ctx, scrollController) {
                return SingleChildScrollView(
                  controller: scrollController,
                  child: Column(
                    children: [
                      Text(
                        'Draggable and Scrollable piece of sheet',
                        style: TextStyle(
                          fontSize: 50,
                          color: Colors.black,
                        ),
                      ),
                      FlutterLogo(
                        size: 100,
                      ),
                    ],
                  ),
                );
              }),
            )
          ],
        ),
      ),
    );
  }
}
