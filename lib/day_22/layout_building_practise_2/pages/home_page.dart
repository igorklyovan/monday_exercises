import 'package:exercises/day_22/layout_building_practise_2/shared/custom_app_bar.dart';
import 'package:exercises/day_22/layout_building_practise_2/shared/custom_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  static const String routeName = '/home';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final _color = Color.fromRGBO(233, 93, 85, 1);

    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(color: Color.fromRGBO(244, 234, 221, 1)),
          ),
          Positioned(
            right: deviceSize.width * 0.06,
            bottom: deviceSize.height * 0.44,
            child: Container(
              height: deviceSize.height * 0.65,
              width: deviceSize.width * 0.6,
              decoration: BoxDecoration(border: Border.all(width: 2, color: _color), shape: BoxShape.circle),
            ),
          ),
          Positioned(
            left: deviceSize.width * 0.5,
            bottom: deviceSize.height * 0.56,
            child: Container(
              height: deviceSize.height * 0.6,
              width: deviceSize.width * 0.85,
              decoration: BoxDecoration(shape: BoxShape.circle, color: _color),
            ),
          ),
          Positioned(
            left: deviceSize.width * 0.85,
            top: deviceSize.height * 0.1,
            child: Container(
              height: deviceSize.height * 0.3,
              width: deviceSize.width * 0.3,
              decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 2, color: Colors.white)),
            ),
          ),
          Positioned(
            left: deviceSize.width * 0.03,
            top: deviceSize.height * 0.52,
            child: Container(
              height: deviceSize.height * 0.965,
              width: deviceSize.width * 0.35,
              decoration: BoxDecoration(
                border: Border.all(width: 2, color: _color),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            right: deviceSize.width * 0.75,
            top: deviceSize.height * 0.45,
            child: Container(
              height: deviceSize.height * 1,
              width: deviceSize.width * 0.45,
              decoration: BoxDecoration(color: _color, shape: BoxShape.circle),
            ),
          ),
          Positioned(
            right: deviceSize.width * 0.9,
            top: deviceSize.height * 0.78,
            child: Container(
              height: deviceSize.height * 0.11,
              width: deviceSize.width * 0.2,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 2,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Positioned(
            right: deviceSize.width * 0.1,
            left: deviceSize.width * 0.1,
            top: deviceSize.height * 0.45,
            child: Container(
              height: deviceSize.height * 0.3,
              width: deviceSize.width * 0.8,
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(width: 2, color: Colors.black),
                ),
                color: Colors.transparent,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Sample subtitle',
                    style: TextStyle(fontSize: deviceSize.height * 0.028),
                  ),
                  Text(
                    'Sample long title',
                    style: TextStyle(fontSize: deviceSize.height * 0.04, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Lorem ipsum dolor sit amet,\n consectetur adipiscing elit.',
                    style: TextStyle(fontSize: deviceSize.height * 0.02),
                  ),
                ],
              ),
            ),
          )
//          CustomAppBar(),
        ],
      ),
      appBar: CustomAppBar(),
      bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}
