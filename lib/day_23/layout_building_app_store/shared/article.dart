import 'package:flutter/material.dart';

class Article {
  final IconData icon;
  final String image;
  final String title;
  final String genre;
  final String type;
  final double price;
  final int index;
//  final transitionAnimation;

  Article({
    @required this.icon,
    @required this.image,
    @required this.title,
    @required this.genre,
    @required this.type,
    @required this.price,
    @required this.index,
//    @required this.transitionAnimation,
  });
}
