import 'package:flutter/material.dart';

import '../main_drawer.dart';

class BlackPage extends StatelessWidget {
  static const String routeName = '/black';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: MainDrawer(),
      body: Container(
        color: Colors.black,
      ),
    );
  }
}
