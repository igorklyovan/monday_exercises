import 'dart:developer';

import 'package:exercises/day_48/bloc_practise/bloc/counter/counter_bloc.dart';
import 'package:exercises/day_48/bloc_practise/bloc/counter/counter_events.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CounterBloc _counterBloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          StreamBuilder(
            stream: _counterBloc,
            builder: (ctx, data) {
              return Text(
                '${data.data ?? 0}',
                style: TextStyle(fontSize: 30),
              );
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RaisedButton(
                onPressed: () => _counterBloc.add(CounterEvent.incrementEvent),
                child: Icon(Icons.add),
              ),
              RaisedButton(
                onPressed: () => _counterBloc.add(CounterEvent.decrementEvent),
                child: Icon(Icons.remove),
              ),
            ],
          ),
          RaisedButton(
            onPressed: () => _counterBloc.add(CounterEvent.clear),
            child: Icon(
              Icons.restore_from_trash,
              color: Colors.white,
            ),
            color: Colors.red,
          )
        ],
      ),
    );
  }
}
