import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<String> value = Future<String>.delayed(
    Duration(seconds: 5),
    () {
      return 'yes';
    },
  );
  Color _color = Colors.yellow;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: NotificationListener<ScrollNotification>(
          onNotification: (info) {
            print(info);
            if (info.metrics.pixels ==
                info.metrics.maxScrollExtent){
              setState(() {
                _color = Colors.deepPurple;
              });
            }
          },
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomPaint(
                  size: Size(deviceSize.width * 0.8, deviceSize.height * 0.5),
                  painter: MyPainter(),
                ),
                Container(
                  color: Colors.green,
                  height: deviceSize.height * 0.2,
                  width: deviceSize.width * 0.3,
                ),
                Container(
                  color: _color,
                  height: deviceSize.height,
                  width: deviceSize.width * 0.3,
                ),
                FutureBuilder(
                  future: value,
                  builder: (ctx, snapshot) {
                    if (snapshot.hasData) {
                      return FlutterLogo();
                    } else
                      return CircularProgressIndicator();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 5
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    Offset center = Offset(size.width / 2, size.height / 2);

    canvas.drawCircle(center, 100, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
