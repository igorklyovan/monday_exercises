import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  IconData icon;
  String routeName;

  MyButton(this.icon, this.routeName);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Colors.blue)),
        child: Icon(icon),
        onPressed: () {
          Navigator.of(context).pushReplacementNamed(routeName);
        },
      ),
    );
  }
}
