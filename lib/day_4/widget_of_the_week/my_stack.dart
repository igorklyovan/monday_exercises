import 'package:flutter/material.dart';

class MyStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colors.purple,
          height: 88,
          width: 88,
        ),
        Container(
          color: Colors.green,
          height: 70,
          width: 70,
        ),
        Container(
          color: Colors.blue,
          height: 50,
          width: 50,
        )
      ],
    );
  }
}
