import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool serviceEnabled;
  bool locationStatus = false;
  Position currentPosition;

  void update() => setState(() {});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            currentPosition != null
                ? Text('Your coordinates: longitude(${currentPosition.longitude}) and latitude(${currentPosition.latitude})')
                : Text(
                    'Here will be displayed location',
                    style: TextStyle(fontSize: 30.0),
                  ),
            RaisedButton(
              onPressed: () async {
                serviceEnabled = await Geolocator.isLocationServiceEnabled();
                if (serviceEnabled) {
                  locationStatus = await Permission.location.isGranted;
                  await Permission.values.request();
                  if (await Permission.location.isGranted) {
                    currentPosition = await Geolocator.getCurrentPosition();
                  }
                  update();
                } else {}
              },
              child: Text('Get permissions'),
            ),
          ],
        ),
      ),
    );
  }
}
