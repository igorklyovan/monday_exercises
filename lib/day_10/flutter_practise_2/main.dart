import './widgets/button.dart';
import 'package:flutter/material.dart';
import './pages/counter_page.dart';
import './pages/image_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      home: MyHomePage(),
      routes: {
        CounterPage.routeName: (context) => CounterPage(),
        ImagesPage.routeName: (context) => ImagesPage(),
      },
    );
  }
}
