import 'dart:collection';

import 'package:exercises/day_33/redux_study_practise_1/store/app/reducer.dart';
import 'package:exercises/day_33/redux_study_practise_1/store/pages/counter_page_state/counter_page_actions.dart';
import 'package:flutter/material.dart';

class CounterPageState {
  final double counter;
  final double counter2;
  final Color appBarColor;
  final bool isAppBarColorSwitch;

  CounterPageState({this.counter, this.counter2, this.appBarColor, this.isAppBarColorSwitch});

  factory CounterPageState.initial() {
    return CounterPageState(counter: 0, counter2: 10, appBarColor: Colors.grey[850], isAppBarColorSwitch: false);
  }

  CounterPageState copyWith({double counter, double counter2, Color appBarColor, bool isAppBarColorSwitch}) {
    return CounterPageState(
        counter: counter ?? this.counter,
        counter2: counter2 ?? this.counter2,
        appBarColor: appBarColor ?? this.appBarColor,
        isAppBarColorSwitch: isAppBarColorSwitch ?? this.isAppBarColorSwitch);
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
        MultiplyCounterAction: (dynamic action) => _multiplyCounter(action as MultiplyCounterAction),
        CleanCounterAction: (dynamic action) => _cleanCounter(),
        ChangeAppBarColorAction: (dynamic action) => _changeAppBarColor(),
      }),
    ).updateState(action, this);
  }

  CounterPageState _incrementCounter() {
    return copyWith(counter: counter + 1);
  }

  CounterPageState _decrementCounter() {
    return copyWith(counter: counter - 1);
  }

  CounterPageState _cleanCounter() {
    return copyWith(counter: 0);
  }

//  CounterPageState _changeAppBarColor(ChangeAppBarColorAction action){
//    return copyWith(appBarColor: action.newColor);
//  }

  CounterPageState _changeAppBarColor() {
    return copyWith(isAppBarColorSwitch: !isAppBarColorSwitch);
  }

  CounterPageState _multiplyCounter(MultiplyCounterAction action) {
    return copyWith(counter: action.counter2 * 2);
  }
}
