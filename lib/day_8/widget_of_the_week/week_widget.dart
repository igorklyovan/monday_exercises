import 'dart:math';

import 'package:flutter/material.dart';

class WeekWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Column(
      children: [
        Row(
          children: [
            Transform.scale(
                scale: 2,
                child: Opacity(
                  opacity: 0.5,
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(12), color: Colors.yellow),
                    width: 30,
                    height: 30,
                  ),
                )),
            Transform.rotate(
                angle: pi / 4,
                child: Container(
                  child: Icon(Icons.arrow_forward),
                )),
            Transform.rotate(
                angle: pi / 4,
                child: Container(
                  child: Icon(Icons.arrow_forward),
                )),
          ],
        ),
        ClipOval(child: Image.asset('assets/images/snowman.png')),
      ],
    );
  }
}
