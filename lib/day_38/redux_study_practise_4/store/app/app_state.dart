import 'package:exercises/day_38/redux_study_practise_4/store/pages/another_page_state/another_page_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/another_page_state/another_page_state_epics.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/home_page_state/home_page_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/home_page_state/home_page_state_epics.dart';
import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final AnotherPageState anotherPageState;
  final HomePageState homePageState;

  AppState({
    @required this.anotherPageState,
    @required this.homePageState,
  });

  factory AppState.initial() {
    return AppState(
      anotherPageState: AnotherPageState.initial(),
      homePageState: HomePageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    return AppState(
      anotherPageState: state.anotherPageState.reducer(action),
      homePageState: state.homePageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    AnotherPageStateEpics.indexEpic,
    HomePageStateEpics.indexEpic,
  ]);
}
