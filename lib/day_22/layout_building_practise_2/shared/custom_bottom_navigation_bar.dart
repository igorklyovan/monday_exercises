import 'package:carousel_slider/carousel_slider.dart';
import 'package:exercises/day_22/layout_building_practise_2/provider/pointer_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  @override
  _CustomBottomNavigationBarState createState() => _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> with SingleTickerProviderStateMixin {
  AnimationController _opacityController;

  @override
  void initState() {
    _opacityController = AnimationController(
        vsync: this, duration: Duration(seconds: 2), reverseDuration: Duration(seconds: 2), value: 1);
    _opacityController.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    _opacityController.dispose();
    _opacityController.removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final pointerData = Provider.of<PointerProvider>(context, listen: false);

    return Padding(
      padding: const EdgeInsets.only(bottom: 0.1),
      child: CarouselSlider.builder(
        itemCount: 6,
        itemBuilder: (ctx, index) => Container(
          child: Container(
            height: deviceSize.height * 0.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Consumer<PointerProvider>(
                  builder: (ctx, pointer, ch) {
                    return Card(
                      color: Colors.black26,
                      child: SizedBox(
                        height: deviceSize.height * 0.1,
                        width: deviceSize.width * 0.15,
                        child: Icon(
                          Icons.android_rounded,
                          color: pointer.currentIndex == index ? Colors.yellow : Colors.white,
                          size: deviceSize.height * 0.04,
                        ),
                      ),
                      elevation: 2,
                    );
                  },
                ),
                Consumer<PointerProvider>(
                  builder: (ctx, pointer, ch) {
                    return Opacity(
//                            opacity: pointer.currentIndex == index ? 1 : 0 ,
                      opacity: pointer.currentIndex == index ? _opacityController.value : 0,
                      child: Container(
                        height: deviceSize.height * 0.01,
                        width: deviceSize.width * 0.06,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(width: 1, color: Colors.black),
                            color: Colors.yellow),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        ),
        options: CarouselOptions(
          height: 200,
          viewportFraction: 0.21,
          onPageChanged: (index, _) {
            pointerData.currentIndex = index;
            _opacityController.forward();
          },
        ),
      ),
    );
  }

  void update() => setState(() {});
}
