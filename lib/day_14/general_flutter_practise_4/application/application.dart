import '../pages/black_page.dart';
import '../pages/purple_page.dart';
import '../pages/yellow_page.dart';
import 'package:flutter/material.dart';

import '../pages/home_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: HomePage.routeName,
      routes: {
        HomePage.routeName: (ctx) => HomePage(),
        PurplePage.routeName: (ctx) => PurplePage(),
        YellowPage.routeName: (ctx) => YellowPage(),
        BlackPage.routeName: (ctx) => BlackPage()
      },
    );
  }
}
