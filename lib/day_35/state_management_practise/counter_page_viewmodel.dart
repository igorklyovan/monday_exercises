import 'package:exercises/day_35/state_management_practise/store/app/app_state.dart';
import 'package:exercises/day_35/state_management_practise/store/pages/counter_page_state/counter_page_selectors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class CounterPageViewModel {
  final Color homeColor;

  final void Function(Color) changeHomerColor;

  CounterPageViewModel({@required this.homeColor, @required this.changeHomerColor});

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
        homeColor: CounterPageSelectors.getHomeColorValue(store),
        changeHomerColor: CounterPageSelectors.getChangeHomeColorFunction(store));
  }
}
