import 'package:exercises/day_35/redux_study_practise_2/store/app/app_state.dart';
import 'package:exercises/day_35/redux_study_practise_2/store/pages/counter_page_state/counter_page_selectors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import 'model/brewer.dart';

class CounterPageViewModel {
  final Color homeColor;
  final List<Brewer> brewers;
  final void Function() getBrewers;

//  final void Function(Color) changeHomerColor;

  CounterPageViewModel({@required this.homeColor, @required this.brewers, @required this.getBrewers});

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      homeColor: CounterPageSelectors.getHomeColorValue(store),
      brewers: CounterPageSelectors.getBrewers(store),
      getBrewers: CounterPageSelectors.getBrewersFunction(store),
    );
  }
}
