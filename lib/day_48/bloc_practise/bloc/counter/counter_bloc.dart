import 'package:exercises/day_48/bloc_practise/bloc/counter/counter_events.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounterBloc extends Bloc<CounterEvent, int> {
  int _counter = 0;

  @override
  Stream<int> mapEventToState(CounterEvent event) async* {
    switch (event) {
      case CounterEvent.incrementEvent:
        _counter++;
        break;
      case CounterEvent.decrementEvent:
        _counter--;
        break;
      case CounterEvent.clear:
        _counter = 0;
        break;
    }
    yield _counter;
  }

  CounterBloc() : super(0);
}
