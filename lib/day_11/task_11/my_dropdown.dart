import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './providers/items.dart';

class MyDropDown extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final itemsData = Provider.of<Items>(context);
    final items = itemsData.items;
    String selectedItem = Provider.of<Items>(context).selectedItem;
    return DropdownButton<String>(
      value: selectedItem,
      onChanged: (String string) => itemsData.selectedItem = string,
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      selectedItemBuilder: (BuildContext context) {
        return items.map((String item) {
          return Text(
            item,
            style: TextStyle(fontSize: 24),
          );
        }).toList();
      },
      items: items.map((String item) {
        return DropdownMenuItem<String>(
          child: Text(item),
          value: item,
        );
      }).toList(),
    );
  }
}
