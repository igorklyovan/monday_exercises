import 'package:flutter/cupertino.dart';

abstract class ColorEvent {}

class ChangeColorEvent extends ColorEvent {
  final Color color;

  ChangeColorEvent({@required this.color});
}
