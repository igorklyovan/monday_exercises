import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:exercises/day_42/redux_practice_5/store/pages/counter_state/counter_selectors.dart';
import 'package:exercises/day_42/redux_practice_5/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class ThirdPageViewModel {
  final double counter;

  ThirdPageViewModel({
    @required this.counter,
  });

  static ThirdPageViewModel fromStore(Store<AppState> store) {
    return ThirdPageViewModel(
      counter: CounterSelectors.getCounter(store),
    );
  }
}
