import 'package:exercises/day_42/redux_practice_5/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:exercises/day_42/redux_practice_5/pages/home_page_vm.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
      converter: HomePageViewModel.fromStore,
      builder: (BuildContext context, HomePageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(),
          backgroundColor: Colors.green,
          drawer: CustomDrawer(),
          body: Center(
            child: Text(
              '${viewModel.counter}',
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      },
    );
  }
}
