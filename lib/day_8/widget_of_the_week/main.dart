import 'package:flutter/material.dart';

import './week_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                  decoration: BoxDecoration(color: Colors.blue[200]),
                  child: Text(
                    "SnowMan",
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  )),
              ListTile(
                  leading: Icon(Icons.mode_edit),
                  title: Text(
                    "Rename",
                  )),
              ListTile(
                  leading: Icon(Icons.add),
                  title: Text(
                    "Add some snow",
                  )),
              ListTile(
                  leading: Icon(Icons.wb_sunny),
                  title: Text(
                    "Heat up",
                  )),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text('WeekWidget'),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          child: WeekWidget(),
        ),
      ),
    );
  }
}
