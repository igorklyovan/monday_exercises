import 'package:exercises/day_19/practise_8_localization/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './application/application.dart';

void main() {
  runApp(
    ChangeNotifierProvider.value(
      value: LanguageProvider(),
      child: Application(),
    ),
  );
}
