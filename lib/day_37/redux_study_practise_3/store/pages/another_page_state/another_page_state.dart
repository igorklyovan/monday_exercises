import 'dart:collection';

import 'package:exercises/day_35/redux_study_practise_2/model/brewer.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/app/reducer.dart';
import 'package:flutter/material.dart';

class AnotherPageState {
  AnotherPageState();

  factory AnotherPageState.initial() {
    return AnotherPageState();
  }

  AnotherPageState copyWith() {
    return AnotherPageState();
  }

  AnotherPageState reducer(dynamic action) {
    return Reducer<AnotherPageState>(
      actions: HashMap.from({}),
    ).updateState(action, this);
  }

//  AnotherPageState _changeHomeColor(ChangeHomeColorAction action) {
//    return copyWith();
//  }
}
