import 'package:exercises/day_27/api_practise_3/data/models/product.dart';
import 'package:flutter/material.dart';

class ProductPage extends StatelessWidget {
  Product _product;

  ProductPage(this._product);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          height: deviceSize.height * 0.8,
          width: deviceSize.width * 0.8,
          child: Card(
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Spacer(),
                  Text(
                    '${_product.name}',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: Image.network(
                      _product.image,
                      fit: BoxFit.cover,
                    ),
                  ),
//                              Spacer(flex: 3,),
                  const SizedBox(height: 10),

                  Spacer(),
                  Text('brand: ${_product.brand}'),
                  Spacer(),
                  Text('price: ${_product.price}'),
                  Spacer(),
                  _product.rating != 0
                      ? Text(
                          'rating:${_product.rating}',
                          style: TextStyle(color: Colors.orangeAccent, fontWeight: FontWeight.bold),
                        )
                      : Text('rating is not set yet'),
                  Spacer(),
                  Text('${_product.description}'),
                  Spacer(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
