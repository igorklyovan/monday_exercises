import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _heightController;
  AnimationController _textOpacityController;
  AnimationController _colorController;
  Animation<Color> _colorBackgroundAnimation;
  Animation<Color> _colorTextAnimation;
  bool isContainerTaped = false;
  bool isButtonTaped = false;

//  AnimatedContainer _textOpacityController;

  @override
  void initState() {
    _controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000), reverseDuration: Duration(milliseconds: 1000));
    _heightController = Tween<double>(begin: 5, end: 200).animate(_controller);
    _colorController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000), reverseDuration: Duration(milliseconds: 1000));
    _textOpacityController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000), reverseDuration: Duration(milliseconds: 200));
    _controller.addListener(update);
    _textOpacityController.addListener(update);
    _colorController.addListener(update);
    _colorBackgroundAnimation = ColorTween(begin: Colors.yellow, end: Colors.blue).animate(_colorController);
    _colorTextAnimation = ColorTween(begin: Colors.black, end: Colors.white).animate(_colorController);
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _textOpacityController.forward();
      } else if (status == AnimationStatus.reverse) {
        _textOpacityController.reverse();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _controller.removeListener(update);
    _textOpacityController.removeListener(update);
    _colorController.removeListener(update);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              if (!isContainerTaped) {
                _controller.forward();
                isContainerTaped = !isContainerTaped;
              } else if (isContainerTaped) {
                _controller.reverse();
                isContainerTaped = !isContainerTaped;
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 22),
              height: _heightController.value,
              width: double.infinity,
              decoration: BoxDecoration(color: Colors.yellow, border: Border.all(color: Colors.orange, width: 10)),
              child: Center(
                child: Opacity(
                  opacity: _textOpacityController.value,
                  child: Text(
                    'Test message!',
                    style: TextStyle(color: Colors.orange, fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18), side: BorderSide(color: _colorTextAnimation.value)),
              color: _colorBackgroundAnimation.value,
              textColor: _colorTextAnimation.value,
              onPressed: () {
                if (!isButtonTaped) {
                  _colorController.forward();
                  isButtonTaped = !isButtonTaped;
                } else if (isButtonTaped) {
                  _colorController.reverse();
                  _controller.reverse();
                  isButtonTaped = !isButtonTaped;
                }
              },
              child: Text(
                'Tap me!',
                style: TextStyle(fontSize: 25),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void update() => setState(() {});
}
