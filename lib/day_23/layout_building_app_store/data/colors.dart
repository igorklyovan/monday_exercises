import 'package:flutter/material.dart';

class AppColors {
  static const Color kWhite = Color(0xFFFFFFFF);
  static const Color kBottomNavigationColor = Color(0xFF272727);
  static const Color kGrey = Color(0xFF6A6A6A);
  static const Color kSendButtonColor = Color(0xFF33B5FF);
  static const Color kSplashScreenBackgroundColor = Color(0xFF009EF8);
  static const Color kBottomNavigationButtonColor = Color(0xFF32A6E8);
}
