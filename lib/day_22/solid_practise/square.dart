import 'package:exercises/day_22/solid_practise/shape.dart';

class Square implements Shape{
  double length = 10;

  @override
  double area (){
    return length*length;
  }

}