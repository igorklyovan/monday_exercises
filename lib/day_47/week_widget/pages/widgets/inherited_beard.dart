import 'package:flutter/cupertino.dart';

class InheritedBeard extends InheritedWidget {
  final String beardType;

  InheritedBeard({this.beardType, Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => true;

  static InheritedBeard of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(InheritedBeard) as InheritedBeard;
}
