import '../res/consts.dart';
import 'package:http/http.dart' as http;

class PhotoService {
  Future<dynamic> getPhotos() async {
    var response = await http.get(Constants.kPictureUrl);
    return response.body;
  }
}