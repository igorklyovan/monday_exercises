import 'dart:collection';

import 'package:exercises/day_35/redux_study_practise_2/model/brewer.dart';
import 'package:exercises/day_35/redux_study_practise_2/store/pages/counter_page_state/counter_page_actions.dart';
import 'package:exercises/day_35/state_management_practise/store/app/reducer.dart';
import 'package:flutter/material.dart';

class CounterPageState {
  final Color homeColor;
  final List<Brewer> brewers;

  CounterPageState({
    this.homeColor,
    this.brewers,
  });

  factory CounterPageState.initial() {
    return CounterPageState(
      homeColor: Colors.grey[850],
      brewers: [],
    );
  }

  CounterPageState copyWith({Color homeColor, List<Brewer> brewers}) {
    return CounterPageState(
      homeColor: homeColor ?? this.homeColor,
      brewers: brewers ?? this.brewers,
    );
  }

  CounterPageState reducer(dynamic action) {
    return Reducer<CounterPageState>(
      actions: HashMap.from({
        ChangeHomeColorAction: (dynamic action) => _changeHomeColor(action as ChangeHomeColorAction),
        AddBrewerAction: (dynamic action) => _getBrewers(action.brewers),
      }),
    ).updateState(action, this);
  }

  CounterPageState _changeHomeColor(ChangeHomeColorAction action) {
    return copyWith(homeColor: action.newColor);
  }

  CounterPageState _getBrewers(List<Brewer> brewers) {
    return copyWith(brewers: brewers);
  }
}
