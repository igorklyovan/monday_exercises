import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/model/brewer.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/another_page_state/another_page_selectors.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class AnotherPageViewModel {
  final List<Brewer> brewers;
  final void Function() getBrewers;
  final void Function(String) push;

  AnotherPageViewModel({
    @required this.push,
    @required this.brewers,
    @required this.getBrewers,
  });

  static AnotherPageViewModel fromStore(Store<AppState> store) {
    return AnotherPageViewModel(
        push: NavigationSelectors.push(store),
        getBrewers: AnotherPageSelectors.getBrewersFunction(store),
        brewers: AnotherPageSelectors.getBrewers(store));
  }
}
