import 'package:exercises/day_27/api_practise_3/data/models/product.dart';
import 'package:exercises/day_27/api_practise_3/pages/product_page.dart';
import 'package:exercises/day_27/api_practise_3/repository/product_repository.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Product> _products = [];
  TextEditingController _textController;
  bool _validate = false;
  bool _isLoading = false;

  @override
  void initState() {
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('API'),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Builder(
                  builder: (ctx) {
                    return TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Name of the brand',
                          errorText: _validate ? 'Value Can\'t Be Empty' : null),
                      controller: _textController,
                      textInputAction: TextInputAction.search,
                      onSubmitted: (brand) async {
                        if (_textController.text.isEmpty) {
                          setState(() {
                            _validate = true;
                          });
                        } else {
                          setState(() {
                            _validate = false;
                          });
                          setState(() {
                            _isLoading = true;
                          });
                          _products = await ProductRepository().getProducts(brand);
                          setState(() {
                            _isLoading = false;
                          });
                          if (_products.length == 0) {
                            Scaffold.of(ctx).showSnackBar(
                              SnackBar(content: Text('Try another brand!')),
                            );
                          }
                        }
                      },
                    );
                  },
                ),
              ),
              if (_isLoading) CircularProgressIndicator(),
              if (_products.length > 0)
                SizedBox(
                  height: deviceSize.height * 0.8,
                  child: GridView.builder(
                      itemCount: _products.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2, crossAxisSpacing: 4.0, mainAxisSpacing: 4.0),
                      itemBuilder: (ctx, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => ProductPage(_products[index])),
                            );
                          },
                          child: Card(
                            elevation: 5,
                            child: Column(
                              children: [
                                Text(
                                  _products[index].name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                  child: Image.network(
                                    _products[index].image,
                                    fit: BoxFit.cover,
                                    height: deviceSize.height * 0.15,
                                  ),
                                ),
                                const SizedBox(height: 10),
                              ],
                            ),
                          ),
                        );
                      }),
                )
            ],
          ),
        ),
      ),
    );
  }
}
