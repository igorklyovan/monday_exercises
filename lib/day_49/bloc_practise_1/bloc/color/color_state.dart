import 'package:flutter/cupertino.dart';

abstract class ColorState {}

class ChangedColorState extends ColorState {
  final Color newColor;

  ChangedColorState({@required this.newColor}) : assert(newColor != null);
}

class ColorEmptyState extends ColorState {}
