import 'package:flutter/material.dart';

class SplashPhoto {
  String id;
  String url;
  String date;
  int likes;

  SplashPhoto({
    @required this.id,
    @required this.url,
    @required this.date,
    @required this.likes,
  });

  factory SplashPhoto.fromJson(Map<String, dynamic> jsonData) {
    return SplashPhoto(
      id: jsonData['id'],
      date: jsonData['updated_at'],
      url: jsonData['urls']['regular'],
      likes: jsonData['likes'],
    );
  }
}
