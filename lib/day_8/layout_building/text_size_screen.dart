import 'package:flutter/material.dart';

import 'package:auto_size_text/auto_size_text.dart';

class TextSizeExampleScreen extends StatelessWidget {
  static const routeName = '/test-size-example';

  @override
  Widget build(BuildContext context) {
    final routeArgs = ModalRoute.of(context).settings.arguments as Map<String, String>;
    final appBarTitle = routeArgs['title'];

    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
      ),
      body: Center(
        child: SizedBox(
          width: 200.0,
          height: 140.0,
          child: AutoSizeText(
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
            style: TextStyle(fontSize: 30.0),
            maxLines: 3,
          ),
        ),
      ),
    );
  }
}
