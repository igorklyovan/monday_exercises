import 'package:exercises/day_18/provider_listview/providers/list_view_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum ButtonAction { generateLength, updateSort, removeRandom }

class MyButton extends StatelessWidget {
  IconData _icon;
  ButtonAction _action;
  Color _buttonColor;

  MyButton(this._icon, this._action, this._buttonColor);

  @override
  Widget build(BuildContext context) {
    final listProviderData = Provider.of<ListViewProvider>(context, listen: false);
//    final counterData = Provider.of<Counters>(context);
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
        color: _buttonColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Colors.blue)),
        child: Icon(_icon),
        onPressed: () {
          if (_action == ButtonAction.generateLength) {
            listProviderData.generateLength();
          } else if (_action == ButtonAction.updateSort) {
            listProviderData.changeSortType();
          } else if (_action == ButtonAction.removeRandom) {
            listProviderData.removeRandom();
          }
        },
      ),
    );
  }
}
