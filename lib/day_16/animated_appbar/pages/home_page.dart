import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _heightController;
  bool isAppBarTapped = false;

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1), reverseDuration: Duration(seconds: 1));
    _heightController = Tween<double>(begin: 50, end: 100).animate(_controller);
    _controller.addListener(update);
    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(update);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(_heightController.value),
        child: AppBar(
          elevation: (_heightController.value / 10) - 5,
          title: Center(child: Text('Appbar')),
          actions: [
            FlatButton(
                onPressed: () {
                  if (!isAppBarTapped) {
                    _controller.forward();
                    isAppBarTapped = !isAppBarTapped;
                  } else {
                    _controller.reverse();
                    isAppBarTapped = !isAppBarTapped;
                  }
                },
                child: Icon(Icons.shuffle))
          ],
        ),
      ),
    );
  }

  void update() => setState(() {});
}
