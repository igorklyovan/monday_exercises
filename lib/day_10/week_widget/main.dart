import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _counter = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Text("Expanded", style: TextStyle(fontSize: 25)),
            ),
          ),
          Flexible(
            flex: 1,
            child: InkWell(
              onTap: () {
                setState(() {
                  _counter++;
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spacer(),
                  Expanded(
                    flex: _counter,
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(color: Colors.blue),
                    ),
                  ),
                  Spacer(),
                  Expanded(
                    flex: _counter + 1,
                    child: Container(
                      width: 50,
                      height: 50,
                      color: Colors.blue,
                    ),
                  ),
                  Spacer()
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Flexible(
            flex: 1,
            child: Text(
              'IgnorePointer',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Flexible(
            flex: 1,
            child: IgnorePointer(
              ignoring: true,
              child: RaisedButton(
                onPressed: () {
                  setState(() {
                    _counter++;
                  });
                },
                color: Colors.yellow,
                child: Text('Ignore me!', style: TextStyle(fontSize: 25)),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Flexible(
            flex: 1,
            child: Text(
              'ClipRRect',
              style: TextStyle(fontSize: 25),
            ),
          ),
          Flexible(
            flex: 4,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.network(
                  'https://upload.wikimedia.org/wikipedia/commons/5/59/500_x_300_Ramosmania_rodriguesii_%28Rubiaceae%29.jpg',
                  fit: BoxFit.cover,
                  loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
                    if (loadingProgress == null) return child;
                    return Center(
                      child: CircularProgressIndicator(
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                            : null,
                      ),
                    );
                  },
                )),
          ),
          Flexible(
              flex: 1,
              child: Text(
                "Positioned",
                style: TextStyle(fontSize: 25),
              )),
          Flexible(
            flex: 4,
            child: Stack(
              children: [
                Container(
                  height: 200,
                  width: 200,
                  color: Colors.blue,
                ),
                Positioned(
                    top: 25,
                    left: 50,
                    child: Container(
                      height: 20,
                      width: 20,
                      color: Colors.red,
                    )),
                Positioned(
                    top: 25,
                    right: 50,
                    child: Container(
                      height: 20,
                      width: 20,
                      color: Colors.red,
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
