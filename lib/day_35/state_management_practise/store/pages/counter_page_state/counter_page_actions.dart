import 'package:flutter/cupertino.dart';

class ChangeHomeColorAction {
  final Color newColor;

  ChangeHomeColorAction({@required this.newColor});
}

//class ChangeAppBarColorAction {}
