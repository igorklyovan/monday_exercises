import 'dart:collection';

import 'package:exercises/day_42/redux_practice_5/store/app/reducer.dart';
import 'package:exercises/day_42/redux_practice_5/store/pages/counter_state/counter_actions.dart';
import 'package:flutter/material.dart';

class CounterState {
  final double counter;

  CounterState({@required this.counter});

  factory CounterState.initial() {
    return CounterState(counter: 0);
  }

  CounterState copyWith({double counter}) {
    return CounterState(
      counter: counter ?? this.counter,
    );
  }

  CounterState reducer(dynamic action) {
    print('${action.runtimeType}');
    return Reducer<CounterState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
      }),
    ).updateState(action, this);
  }

  CounterState _incrementCounter() {
    return copyWith(counter: counter + 1);
  }

  CounterState _decrementCounter() {
    return copyWith(counter: counter - 1);
  }
}
