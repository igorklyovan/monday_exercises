import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class ThirdPageViewModel {
  final void Function(String) push;

  ThirdPageViewModel({
    @required this.push,
  });

  static ThirdPageViewModel fromStore(Store<AppState> store) {
    return ThirdPageViewModel(
      push: NavigationSelectors.push(store),
    );
  }
}
