import 'package:exercises/day_44/redux_practice_6/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/pages/third_page_vm.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ThirdPageViewModel>(
      converter: ThirdPageViewModel.fromStore,
      builder: (BuildContext context, ThirdPageViewModel viewModel) {
        return Scaffold(
            appBar: AppBar(),
            drawer: CustomDrawer(),
            backgroundColor: viewModel.mainColor,
            body: Center(
              child: Text(
                viewModel.text,
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                  color: viewModel.secondColor,
                ),
              ),
            ));
      },
    );
  }
}
