import 'package:exercises/day_37/redux_study_practise_3/helpers/route_service/route_service.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:exercises/day_37/redux_study_practise_3/pages/home_page.dart';

import 'package:redux_epics/redux_epics.dart';


void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
//      EpicMiddleware(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(
    MyApp(
      store: store,
    ),
  );
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Redux practise',
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: RouteService.onGenerateRoute,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(title: 'Redux practise Home Page'),
      ),
    );
  }
}
