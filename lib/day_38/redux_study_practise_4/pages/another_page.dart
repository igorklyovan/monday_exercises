import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/pages/another_page_vm.dart';
import 'package:exercises/day_38/redux_study_practise_4/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class AnotherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: CustomDrawer(),
      body: StoreConnector<AppState, AnotherPageViewModel>(
          onInitialBuild: (vm) {
            vm.getBrewers();
          },
          converter: AnotherPageViewModel.fromStore,
          builder: (BuildContext context, AnotherPageViewModel viewModel) {
            return Center(
              child: Center(
                child: ListView.builder(
                  itemBuilder: (ctx, index) {
                    return Card(
                      elevation: 5,
                      child: Column(
                        children: [
                          Text(
                            viewModel.brewers[index].name,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                            child: Text(
                              viewModel.brewers[index].city,
                            ),
                          ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    );
                  },
                  itemCount: viewModel.brewers.length,
                ),
              ),
            );
          }),
    );
  }
}
