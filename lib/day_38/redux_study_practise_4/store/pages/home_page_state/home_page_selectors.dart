import 'package:exercises/day_38/redux_study_practise_4/model/show.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/home_page_state/home_page_actions.dart';
import 'package:redux/redux.dart';

class HomePageSelectors {
  static List<Show> getShows(Store<AppState> store) {
    return store.state.homePageState.shows;
  }

  static void Function(String showName) getShowsFunction(Store<AppState> store) {
    return (String showName) => store.dispatch(GetShowsAction(showName));
  }
}
