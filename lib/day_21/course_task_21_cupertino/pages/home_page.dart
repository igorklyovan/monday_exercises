import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isShowDatePicker = false;
  bool isShowColumn = false;
  DateTime _dateTime;
  double index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Container(
                    padding: EdgeInsets.all(20),
                    child: isShowColumn
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              _dateTime != null ? Text('${_dateTime}') : Text('Choose date'),
                              isShowDatePicker
                                  ? SizedBox(
                                      height: 200,
                                      child: CupertinoDatePicker(
                                        initialDateTime: DateTime.now(),
                                        mode: CupertinoDatePickerMode.dateAndTime,
                                        use24hFormat: true,
                                        onDateTimeChanged: (date) {
                                          setState(() {
                                            _dateTime = date;
                                          });
                                        },
                                      ),
                                    )
                                  : Text('PLaceHolder for Date :)'),
                              RaisedButton(
                                onPressed: () {
                                  setState(() {
                                    isShowDatePicker = !isShowDatePicker;
                                  });
                                },
                                child: isShowDatePicker ? Icon(Icons.arrow_back_ios) : Icon(Icons.date_range),
                              ),
                            ],
                          )
                        : Text('Do switch')),
                CupertinoSwitch(
                  value: isShowColumn,
                  onChanged: (val) {
                    setState(() {
                      isShowColumn = val;
                    });
                  },
                ),
              ],
            ),
            Text('$index'),
            CupertinoSlider(
              value: index,
              onChanged: (newIndex) {
                setState(() => index = newIndex);
              },
            ),
          ],
        ),
      ),
    );
  }
}
