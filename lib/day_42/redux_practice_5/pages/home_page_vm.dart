import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:exercises/day_42/redux_practice_5/store/pages/counter_state/counter_selectors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class HomePageViewModel {
  final double counter;


  HomePageViewModel({@required this.counter});

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      counter: CounterSelectors.getCounter(store),
    );
  }
}
