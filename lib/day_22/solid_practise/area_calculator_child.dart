
import 'package:exercises/day_22/solid_practise/area_calculator.dart';
import 'package:exercises/day_22/solid_practise/shape.dart';

class AreaCalculatorChild extends AreaCalculator{
  AreaCalculatorChild(List<Shape> shapes) : super(shapes);
}