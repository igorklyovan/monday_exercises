class Human {
  String firstName;
  String lastName;
  String profession;
  int age;

  Human(this.firstName, this.lastName, this.profession, this.age);
}
