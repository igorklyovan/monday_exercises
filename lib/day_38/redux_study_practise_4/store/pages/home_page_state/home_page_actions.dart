import 'package:exercises/day_38/redux_study_practise_4/model/show.dart';

class GetShowsAction {
  final String showName;

  GetShowsAction(this.showName);
}

class AddShowsAction {
  List<Show> shows;

  AddShowsAction({this.shows});
}
