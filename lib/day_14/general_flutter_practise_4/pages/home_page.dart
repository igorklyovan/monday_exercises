import 'package:flutter/material.dart';

import '../main_drawer.dart';

class HomePage extends StatelessWidget {
  static const String routeName = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: MainDrawer(),
      body: Container(
        color: Colors.red,
      ),
    );
  }
}
