import 'package:flutter/cupertino.dart';

class Brewer {
  final int id;
  final String name;
  final String type;
  final String city;
  final String country;
  final String phone;

  Brewer({
    @required this.id,
    @required this.name,
    @required this.type,
    @required this.city,
    @required this.country,
    @required this.phone,
  });

  factory Brewer.fromJson(Map<String, dynamic> jsonData) {
    return Brewer(
      id: jsonData['id'],
      type: jsonData['type'],
      name: jsonData['name'],
      city: jsonData['city'],
      country: jsonData['country'],
      phone: jsonData['phone'],
    );
  }
}
