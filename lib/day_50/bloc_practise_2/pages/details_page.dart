import 'package:exercises/day_50/bloc_practise_2/model/splash_photo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DetailsPage extends StatelessWidget {
  SplashPhoto splashPhoto;

  DetailsPage({@required this.splashPhoto});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.network(splashPhoto.url),
          Text('${DateFormat.yMMMd().format(DateTime.parse(splashPhoto.date))}'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                Icons.favorite,
                color: Colors.red,
              ),
              Text('${splashPhoto.likes}'),
            ],
          )
        ],
      ),
    );
  }
}
