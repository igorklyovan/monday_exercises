import 'package:intl/intl.dart';

class DateHelper {
  static String getMDY(DateTime date) => DateFormat.yMMMd().format(date);

  static String getTime(DateTime date) => DateFormat.Hm().format(date);

  static String getJmTime(DateTime date) => DateFormat.jm().format(date);

  static String getFullDate(DateTime date) => "${getMDY(date)}, ${getTime(date)}";
}

void main() {
  print(DateHelper.getFullDate(DateTime.now()));
}
