import '../models/human.dart';

void main() {
  final List<Human> humans = [
    Human('Jack', "CEO"),
    Human('Andrew', "Programmer"),
    Human('Alex', "Seller"),
    Human("Elon", "Entrepreneur")
  ];

  for (int i = 0; i < humans.length; i++) {
    print('${humans[i].name}`s profession is ${humans[i].proffesion} ');
  }

  print('');

  for (Human human in humans) {
    print('${human.name} is ${human.proffesion}');
  }

  var updatedHumans = humans.map((human) {
    if (human.name == 'Jack') {
      human.proffesion = 'Programmer';
    }
    return human;
  });

  print('\nupdated list\n');

  updatedHumans.forEach((human) {
    print('${human.name} is ${human.proffesion}');
  });

  print('\n'); // asdasdasdasd
  int i = 0;
  while (i <= 3) {
    print(humans[i].name);
    i++;
  }
  print('\n');

  int j = 1;
  do {
    print('Hello!');
  } while (j < 1);
}
