import 'package:exercises/day_37/redux_study_practise_3/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          appBar: AppBar(),
          drawer: CustomDrawer(),
          body: Center(
            child: Text(
              'Home Page',
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
  }
}
