import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/counter_provider.dart';

import '../pages/counter_page.dart';
import '../pages/buttons_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: CounterProvider(),
      child: MaterialApp(
        title: 'Provider practise',
        initialRoute: ButtonsPage.routeName,
        routes: {ButtonsPage.routeName: (ctx) => ButtonsPage(), CounterPage.routeName: (ctx) => CounterPage()},
      ),
    );
  }
}
