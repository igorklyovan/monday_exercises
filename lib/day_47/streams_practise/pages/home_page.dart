import 'dart:async';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  StreamController<double> _redController;
  StreamController<double> _greenController;
  StreamController<double> _blueController;

  double red = 0;
  double redSlider = 0;
  double green = 0;
  double greenSlider = 0;
  double blue = 0;
  double blueSlider = 0;

  @override
  void initState() {
    super.initState();
//    _color = Color.fromRGBO(0, 0, 0, 1);
    _redController = StreamController.broadcast();
    _greenController = StreamController.broadcast();
    _blueController = StreamController.broadcast();
    _redController.stream.listen((event) {
      print(event);
      setState(() {
        red = event;
      });
    });
    _greenController.stream.listen((event) {
      setState(() {
        green = event;
      });
    });
    _blueController.stream.listen((event) {
      setState(() {
        blue = event;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();

    _redController.close();
    _greenController.close();
    _blueController.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Streams background'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          StreamBuilder(
            stream: _redController.stream,
            initialData: red,
            builder: (ctx, snapshot) {
              return Slider(
                value: redSlider,
                min: 0,
                max: 255,
                activeColor: Colors.red,
                onChanged: (value) {
                  redSlider = value;
                  return _redController.add(redSlider);
                },
              );
            },
          ),
          StreamBuilder(
            stream: _greenController.stream,
            initialData: green,
            builder: (ctx, snapshot) {
              return Slider(
                value: greenSlider,
                min: 0,
                max: 255,
                activeColor: Colors.green,
                onChanged: (value) {
                  greenSlider = value;
                  return _greenController.add(greenSlider);
                },
              );
            },
          ),
          StreamBuilder(
            stream: _blueController.stream,
            initialData: blue,
            builder: (ctx, snapshot) {
              return Slider(
                value: blueSlider,
                min: 0,
                max: 255,
                activeColor: Colors.blue,
                onChanged: (value) {
                  blueSlider = value;
                  return _blueController.add(blueSlider);
                },
              );
            },
          ),
        ],
      ),
      backgroundColor: Color.fromRGBO(red.toInt(), green.toInt(), blue.toInt(), 1),
    );
  }
}
