import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:exercises/day_8/layout_building/button_size_screen.dart';
import 'package:exercises/day_8/layout_building/examples_screen.dart';
import 'package:exercises/day_8/layout_building/text_size_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layouts',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
      ),
      initialRoute: '/',
      routes: {
        '/': (ctx) => ExamplesScreen(),
        TextSizeExampleScreen.routeName: (ctx) => TextSizeExampleScreen(),
        ButtonSizeExampleScreen.routeName: (ctx) => ButtonSizeExampleScreen()
      },
    );
  }
}
