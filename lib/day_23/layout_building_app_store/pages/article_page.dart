import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:exercises/day_23/layout_building_app_store/data/dummy_data.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/article_app_bar.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/article.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/custom_bottom_bar.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/today_stack.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

const SCALE_ANIMATION = 100;
const POP = 130;

class ArticlePage extends StatefulWidget {
  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<Offset> _offsetAnimation;
  AnimationController _heightController;
  Animation _heightAnimation;

  Animation _heightBackAnimation;

  AnimationController _closeController;
  Animation _closeAnimation;

  double _initPoint;

  double _verticalDistance;

  bool _needPop;
  bool _isTop;
  bool _isAppBarScroll = false;
  bool _isAppBarShow = false;

  @override
  void initState() {
    _needPop = true;
    _isTop = true;

    _heightController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _closeController = AnimationController(vsync: this, duration: Duration(milliseconds: 400));

    _closeAnimation = Tween<double>(begin: 1.0, end: 0.75).animate(_closeController);

    _heightAnimation =
        Tween<double>(begin: .9, end: 1).animate(CurvedAnimation(curve: Curves.easeIn, parent: _heightController));

    _heightBackAnimation =
        Tween<double>(begin: 0.6, end: 1).animate(CurvedAnimation(curve: Curves.easeIn, parent: _heightController));

    _heightController.forward();

    _animationController = AnimationController(
      duration: const Duration(milliseconds: 700),
      vsync: this,
    );
    _offsetAnimation = Tween<Offset>(
      begin: const Offset(0.0, 1.5),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeOut,
    ));

    Future.delayed(Duration(milliseconds: 700), () {
      setState(() {
        _isAppBarShow = true;
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _closeController.removeListener(update);
    _closeController.dispose();
    _heightController.removeListener(update);
    _heightController.dispose();
    super.dispose();
  }

  void update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final Article article = ModalRoute.of(context).settings.arguments;
    final deviceSize = MediaQuery.of(context).size;

    return Material(
      type: MaterialType.transparency,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        extendBodyBehindAppBar: true,
        extendBody: true,
        appBar: ArticleAppBar(
          isShow: _isAppBarShow,
          isAppBarScroll: _isAppBarScroll,
        ),
        body: AnimatedBuilder(
          animation: _closeAnimation,
          builder: (_, _child) {
            return Transform.scale(
              scale: _closeAnimation.value,
              child: _child,
            );
          },
          child: SizeTransition(
            sizeFactor: _needPop ? _heightAnimation : _heightBackAnimation,
            child: Listener(
              onPointerDown: (opm) {
                _initPoint = opm.position.dy;
              },
              onPointerUp: (opm) {
                if (_needPop) {
                  _closeController.reverse();
                }
              },
              onPointerMove: (opm) {
                _verticalDistance = -_initPoint + opm.position.dy;
                if (_verticalDistance >= 0) {
                  if (_isTop == true && _verticalDistance < SCALE_ANIMATION) {
                    double _scaleValue = double.parse(
                      (_verticalDistance / 100).toStringAsFixed(2),
                    );
                    _closeController.animateTo(
                      _scaleValue,
                      duration: Duration(milliseconds: 0),
                      curve: Curves.linear,
                    );
                  } else if (_isTop == true && _verticalDistance >= SCALE_ANIMATION && _verticalDistance < POP) {
                    _closeController.animateTo(
                      1,
                      duration: Duration(milliseconds: 0),
                      curve: Curves.linear,
                    );
                  } else if (_isTop == true && _verticalDistance >= POP) {
                    if (_needPop) {
                      _needPop = false;
                      _closeController.fling(velocity: 1).then(
                        (_) {
                          _heightController.reverse();
                          Navigator.of(context).pop();
                        },
                      );
                    }
                  }
                } else {
                  _isTop = false;
                }
              },
              child: NotificationListener<ScrollNotification>(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollUpdateNotification) {
                    double scrollDistance = scrollNotification.metrics.pixels;
                    if (scrollDistance >= 445 || scrollDistance <= 1190) {
                      _animationController.forward();
                      setState(() {
                        _isAppBarScroll = true;
                      });
                    }
                    if (scrollDistance <= 445 || scrollDistance >= 1190) {
                      _animationController.reverse();
                      if (scrollDistance <= 445) {
                        setState(() {
                          _isAppBarScroll = false;
                        });
                      }
                    }

                    if (scrollDistance <= 3) {
                      _isTop = true;
                    }
                  }
                  return true;
                },
                child: Hero(
                  tag: "article_tag${article.index}",
                  child: ColoredBox(
                    color: Colors.white,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          TodayStack(
                            icon: article.icon,
                            image: article.image,
                            title: article.title,
                            type: article.type,
                            genre: article.genre,
                            price: article.price,
                            isRounded: false,
                          ),
                          Padding(
                            padding: EdgeInsets.all(20.0),
                            child: Text(
                              DummyData.dummyText,
                              style: TextStyle(
                                fontSize: 19.0,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            height: 220.0,
                            width: double.infinity,
                            color: Color(0xFFF8F3FE),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Icon(
                                  article.icon,
                                  size: 50.0,
                                ),
                                Text(
                                  article.title,
                                  style: TextStyle(color: Colors.black, fontSize: 21.0),
                                ),
                                Text(
                                  article.genre,
                                  style: TextStyle(color: Colors.grey, fontSize: 14.0),
                                ),
                                Container(
                                  height: deviceSize.height * 0.04,
                                  width: deviceSize.width * 0.2,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(15.0)), color: Colors.blue),
                                  child: Center(
                                    child: Text(
                                      '\$${article.price}',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 50.0),
                          Container(
                            height: deviceSize.height * 0.1,
                            width: deviceSize.width * 0.6,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              color: Color(0xFFF8F3FE),
                            ),
                            child: InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Share story',
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.blue,
                                    ),
                                  ),
                                  SizedBox(width: 4.0),
                                  Icon(
                                    CupertinoIcons.share,
                                    color: Colors.blue,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 50.0),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: SlideTransition(
          position: _offsetAnimation,
          child: CustomBottomBar(article: article),
        ),
      ),
    );
  }
}
