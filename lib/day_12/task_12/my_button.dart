import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/counters.dart';

enum ButtonAction { plus, minus }

class MyButton extends StatelessWidget {
  IconData icon;
  ButtonAction action;

  MyButton(this.icon, this.action);

  @override
  Widget build(BuildContext context) {
    final counterData = Provider.of<Counters>(context);
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Colors.blue)),
        child: Icon(icon),
        onPressed: () {
          if (action == ButtonAction.plus) {
            counterData.counter = counterData.counter + 1;
          } else if (action == ButtonAction.minus) {
            counterData.counter = counterData.counter - 1;
          }
        },
      ),
    );
  }
}
