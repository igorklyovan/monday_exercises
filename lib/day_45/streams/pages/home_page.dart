import 'dart:isolate';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Stream<Future<bool>> _stream;
  StreamSubscription _streamSubscription;
  final Connectivity _connectivity = Connectivity();
  bool isInternet;

  @override
  void initState() {
    _stream = Stream<Future<bool>>.periodic(Duration(milliseconds: 300), (_) async => await _hasInternetConnection());

    _streamSubscription = _stream.listen(
      (event) => event.then(
        (isInternet) {
          if (isInternet)
            setState(() => isInternet = true);
          else
            setState(() => isInternet = false);
        },
      ),
    );

    super.initState();
  }

//  Future<SendPort> initIsolate() async {
//    Completer completer = Completer<SendPort>();
//    ReceivePort isolateToMainStream = ReceivePort();
//
//    isolateToMainStream.listen(
//      (data) {
//        if (data is SendPort) {
//          SendPort mainToIsolateStream = data;
//          completer.complete(mainToIsolateStream);
//        } else {
//          print('[isolateToMainStream] $data');
//        }
//      },
//    );
//
//
//    return compute(myIsolate, isolateToMainStream.sendPort);
//  }

//  void myIsolate(SendPort isolateToMainStream) {
//    ReceivePort mainToIsolateStream = ReceivePort();
//    isolateToMainStream.send(mainToIsolateStream.sendPort);
//
//    mainToIsolateStream.listen((data) {
//      print('[mainToIsolateStream] $data');
//    });
//
//    isolateToMainStream.send('This is from myIsolate()');
//  }

  Future<bool> _hasInternetConnection() async {
    if (await _connectivity.checkConnectivity() == ConnectivityResult.none)
      return false;
    else
      return true;
  }

  @override
  void dispose() {
    _streamSubscription.cancel();
    _stream.asBroadcastStream().drain();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
            onTap: () async {
//              SendPort mainToIsolateStream = await initIsolate();
//              mainToIsolateStream.send('This is from main()');
            },
            child: Row(children: [Text('Internet connection:$isInternet')])),
      ),
    );
  }
}
