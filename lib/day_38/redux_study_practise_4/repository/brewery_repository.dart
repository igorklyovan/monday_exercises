import 'dart:convert';

import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/model/brewer.dart';
import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/service/brewery_service.dart';


class BreweryRepository {
  Future<List<Brewer>> getBreweries() async {
    List<Brewer> breweries = [];
    List jsonData = [];
    try {
      var response = await BreweryService().getBreweries();
      jsonData = json.decode(response);
      print(jsonData);
    } catch (er) {
      print('$er');
    }

    for (int i = 0; i < jsonData.length; i++) {
      breweries.add(Brewer.fromJson(jsonData[i]));
    }

    return breweries;
  }
}
