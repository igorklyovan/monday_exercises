import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/language_state/language_selectors.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_selectors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class AnotherPageViewModel {
  final Color mainColor;
  final Color secondColor;
  final String text;
  final void Function(Color) changeMainColor;
  final void Function(Color) changeSecondColor;

  AnotherPageViewModel(
      {@required this.mainColor,
      @required this.secondColor,
      @required this.changeMainColor,
      @required this.changeSecondColor,
      @required this.text});

  static AnotherPageViewModel fromStore(Store<AppState> store) {
    return AnotherPageViewModel(
      mainColor: ThemeSelectors.getMainColor(store),
      secondColor: ThemeSelectors.getSecondColor(store),
      changeMainColor: ThemeSelectors.changeMainColor(store),
      changeSecondColor: ThemeSelectors.changeSecondColor(store),
      text: LanguageSelectors.getAnotherName(store),
    );
  }
}
