import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  File _storedImage;

  Future<void> _takePicture(ImageSource source) async {
    final picker = ImagePicker();
    final imageFile = await picker.getImage(source: source, maxWidth: 600);
    setState(() {
      _storedImage = File(imageFile.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _storedImage == null
              ? Container(
                  color: Colors.green,
                )
              : Image.file(
                  _storedImage,
                  fit: BoxFit.cover,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                ),
          Align(
            alignment: Alignment.bottomCenter,
            child: RaisedButton(
              child: Text('Camera'),
              onPressed: () {
                return _takePicture(ImageSource.camera);
              },
            ),
          )
        ],
      ),
    );
  }
}
