import 'package:exercises/day_24/api_practise_2/data/models/show.dart';
import 'package:exercises/day_24/api_practise_2/repository/shows_repository.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Show> _shows = [];
  TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: SizedBox(
          height: deviceSize.height * 0.9,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(border: OutlineInputBorder(), labelText: 'Name of the show',),
                  controller: _controller,
                  onSubmitted: (showName) async {
                    _shows = await ShowsRepository().getShows(showName);
                    setState(() {});
                  },
                ),
              ),
              _shows.length > 0
                  ? SizedBox(
                      height: deviceSize.height * 0.6,
                      width: deviceSize.width * 0.9,
                      child: ListView.builder(
                          itemCount: _shows.length,
                          itemBuilder: (ctx, i) {
                            return Container(
                              height: deviceSize.height * 0.9,
                              width: deviceSize.width * 0.7,
                              child: Card(
                                elevation: 5,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Spacer(),
                                    ClipRRect(
                                      borderRadius: BorderRadius.all(Radius.circular(12)),
                                      child: Image.network(
                                        _shows[i].image,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
//                              Spacer(flex: 3,),
                                    const SizedBox(height: 10),
                                    Text(
                                      '${_shows[i].name}',
                                      style: TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    Spacer(),
                                    Text('genre: ${_shows[i].genres}'),
                                    Spacer(),
                                    Text('language: ${_shows[i].language}'),
                                    Spacer(),
                                  ],
                                ),
                              ),
                            );
                          }),
                    )
                  : Image.asset('assets/images/snowman.png'),
            ],
          ),
        ),
      ),
    ));
  }
}
