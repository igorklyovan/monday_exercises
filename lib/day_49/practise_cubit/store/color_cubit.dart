import 'package:bloc/bloc.dart';
import 'package:exercises/day_49/practise_cubit/store/color_state.dart';
import 'package:flutter/material.dart';

class ColorCubit extends Cubit<ColorState>{
  ColorCubit() : super(ColorInitial());

  changeColor (Color color){
    emit(ColorChange(color: color));
  }


}

