import 'package:exercises/day_38/redux_study_practise_4/model/show.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/home_page_state/home_page_selectors.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class HomePageViewModel {
  final List<Show> shows;
  final void Function(String showName) getShows;
  final void Function(String) push;

//  final void Function(Color) changeHomerColor;

  HomePageViewModel({
    @required this.shows,
    @required this.getShows,
    @required this.push,
  });

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      push: NavigationSelectors.push(store),
      shows: HomePageSelectors.getShows(store),
      getShows: HomePageSelectors.getShowsFunction(store),
    );
  }
}
