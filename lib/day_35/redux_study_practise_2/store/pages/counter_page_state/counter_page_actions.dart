import 'package:exercises/day_35/redux_study_practise_2/model/brewer.dart';
import 'package:flutter/cupertino.dart';

class ChangeHomeColorAction {
  final Color newColor;

  ChangeHomeColorAction({@required this.newColor});
}

class GetBrewerAction {}

class AddBrewerAction {
  List<Brewer> brewers;

  AddBrewerAction({this.brewers});
}
//class ChangeAppBarColorAction {}
