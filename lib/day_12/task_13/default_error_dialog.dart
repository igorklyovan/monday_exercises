import 'package:flutter/material.dart';

import './dialog.dart';

class DefaultErrorDialog implements MyDialog {
  @override
  Widget widget;

  DefaultErrorDialog(this.widget);

  @override
  void show(BuildContext ctx) {
    showDialog(context: ctx, builder: (ctx) => widget);
  }
}
