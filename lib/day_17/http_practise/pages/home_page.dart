import 'package:exercises/day_17/http_practise/repository/photo_repository.dart';
import 'package:flutter/material.dart';
import '../repository/photo_repository.dart';
import '../models/photo.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

List<Photo> items = [];

Future<void> _getPhotos() async {
  List<Photo> photos = await PhotoRepository().getPhotos();
  items = photos;
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    _getPhotos().then((_) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(items.length);
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: deviceSize.width,
        height: deviceSize.height,
        child: items.length < 0
            ? CircularProgressIndicator()
            : ListView.builder(
                itemCount: items.length,
                itemBuilder: (ctx, index) {
                  return Image.network(items[index].urls['regular']);
                }),
      ),
    );
  }
}
