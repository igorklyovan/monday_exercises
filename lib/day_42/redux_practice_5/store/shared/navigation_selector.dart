import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';

class NavigationSelectors {
  static void Function(String route) push(Store<AppState> store) {
    return (String route) {
      return store.dispatch(NavigateToAction.push(route));
    };
  }
}
