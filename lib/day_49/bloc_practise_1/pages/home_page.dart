import 'package:exercises/day_49/bloc_practise_1/bloc/color/color_bloc.dart';
import 'package:exercises/day_49/bloc_practise_1/bloc/color/color_state.dart';
import 'package:exercises/day_49/bloc_practise_1/pages/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ColorBloc, ColorState>(
      builder: (ctx, state) {
        if (state is ColorEmptyState) {
          return Scaffold(
            backgroundColor: Colors.grey,
            body: Container(),
            appBar: AppBar(),
            drawer: CustomDrawer(),
          );
        } else if (state is ChangedColorState) {
          return Scaffold(
            backgroundColor: state.newColor,
            body: Container(),
            appBar: AppBar(),
            drawer: CustomDrawer(),
          );
        }
        return Scaffold(
          body: Center(
            child: Text('Nothing to show!'),
          ),
        );
      },
    );
  }
}
