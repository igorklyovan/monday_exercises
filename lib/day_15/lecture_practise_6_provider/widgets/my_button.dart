import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/counter_provider.dart';

enum ButtonAction { plus, minus }

class MyButton extends StatelessWidget {
  IconData icon;
  ButtonAction action;

  MyButton(this.icon, this.action);

  @override
  Widget build(BuildContext context) {
    final counterData = Provider.of<CounterProvider>(context, listen: false);

    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Colors.blue)),
        child: Icon(icon),
        onPressed: () {
          if (action == ButtonAction.plus) {
            counterData.incrementCounter();
          } else if (action == ButtonAction.minus) {
            counterData.decrementCounter();
          }
        },
      ),
    );
  }
}
