import 'dart:convert';

import 'package:exercises/day_27/api_practise_3/data/models/product.dart';
import 'package:exercises/day_27/api_practise_3/service/product_service.dart';

class ProductRepository {
  Future<List<Product>> getProducts(String brand) async {
    List<Product> products = [];
    List jsonData = [];
    try {
      var response = await ProductService().getProducts();
       jsonData = json.decode(response);
      print(jsonData);
    } catch (er) {
      print('$er');
    }

    for (int i = 0; i < jsonData.length; i++) {
      products.add(Product.fromJson(jsonData[i]));
    }

    return products;
  }
}
