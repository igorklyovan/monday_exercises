import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/titles.dart';
import './my_text.dart';

import './my_button.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Titles(),
      child: MaterialApp(
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: MyText(),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Spacer(),
              Expanded(flex: 2, child: MyButton("Text 1")),
              Spacer(),
              Expanded(flex: 2, child: MyButton('Text 2')),
              Spacer()
            ],
          ),
          Row(
            children: [
              Spacer(),
              Expanded(flex: 2, child: MyButton("Text 3")),
              Spacer(),
              Expanded(flex: 2, child: MyButton('Text 4')),
              Spacer()
            ],
          )
        ],
      ),
    );
  }
}
