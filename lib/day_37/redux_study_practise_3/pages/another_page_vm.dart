import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class AnotherPageViewModel {
  final void Function(String) push;

  AnotherPageViewModel({
    @required this.push,
  });

  static AnotherPageViewModel fromStore(Store<AppState> store) {
    return AnotherPageViewModel(
      push: NavigationSelectors.push(store),
    );
  }
}
