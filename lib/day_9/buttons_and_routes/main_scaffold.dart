import 'package:exercises/day_9/buttons_and_routes/main_drawer.dart';
import 'package:flutter/material.dart';

class MainScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: MainDrawer(),
      body: Center(child: Text('Navigation Drawer Example',style: TextStyle(fontSize: 30),)),
    );
  }
}
