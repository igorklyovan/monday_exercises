import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return SafeArea(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: deviceSize.height * 0.01, vertical: deviceSize.height * 0.02),
        padding: EdgeInsets.only(bottom: deviceSize.height * 0.001),
        height: deviceSize.height * 0.1,
        width: deviceSize.width * 0.9,
        child: AppBar(
          title: Text(
            "Custom AppBar",
            style: TextStyle(color: Colors.black, fontSize: deviceSize.height * 0.02),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          shape: Border.symmetric(horizontal: BorderSide(color: Colors.black)),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.black,
                size: deviceSize.height * 0.02,
              ),
              onPressed: () {},
              tooltip: 'Menu',
            ),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(200);
}
