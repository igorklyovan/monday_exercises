import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class HomePageViewModel {
  final void Function(String) push;

//  final void Function(Color) changeHomerColor;

  HomePageViewModel({
    @required this.push,
  });

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      push: NavigationSelectors.push(store),
    );
  }
}
