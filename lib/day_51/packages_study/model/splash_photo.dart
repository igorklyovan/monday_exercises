import 'package:flutter/material.dart';

class SplashPhoto {
  String id;
  String url;
  String date;
  String blurHash;

  SplashPhoto({
    @required this.id,
    @required this.url,
    @required this.date,
    @required this.blurHash,
  });

  factory SplashPhoto.fromJson(Map<String, dynamic> jsonData) {
    return SplashPhoto(
      id: jsonData['id'],
      date: jsonData['updated_at'],
      url: jsonData['urls']['regular'],
      blurHash: jsonData['blur_hash'],
    );
  }
}
