import 'package:flutter/material.dart';

import '../my_painter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> _list = _items();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 15),
//          CustomPaint(
//            size: Size(500, 500),
//            painter: MyPainter(mediaQuery.width * 0.25),
//          ),
          Stack(
            children: [
              CustomPaint(
                size: Size(500, 500),
                painter: MyPainter(mediaQuery.width * 0.25),
              ),
              Container(
                height: mediaQuery.height * 0.9,
                width: mediaQuery.width,
                child: Center(
                  child: Container(
                      height: 400,
                      width: 400,
                      child: ListView.builder(
                        itemCount: _list.length,
                        itemBuilder: (ctx, index) {
                          return _list[index];
                        },
//                        children: _list,
                      )),
                ),
              ),
            ],
          ),

          RaisedButton(
              child: Icon(Icons.shuffle),
              onPressed: () {
                setState(() {
                  _list = _list.reversed.toList();
                });
              })
        ],
      ),
    );
  }
}

List<Widget> _items() {
  return List.generate(100, (index) {
    return Column(
      children: [
        Divider(),
        ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: Container(
            decoration: BoxDecoration(color: index % 2 == 0 ? Colors.red : Colors.black, shape: BoxShape.circle),
            child: Center(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                '${index + 1}',
                style: TextStyle(color: Colors.white),
              ),
            )),
          ),
        ),
      ],
    );
  });
}
