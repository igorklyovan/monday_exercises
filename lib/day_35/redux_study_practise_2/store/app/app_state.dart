import 'package:exercises/day_35/redux_study_practise_2/store/pages/counter_page_state/counter_page_state_epics.dart';
import 'package:exercises/day_35/state_management_practise/store/pages/counter_page_state/counter_page_state.dart';
import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final CounterPageState counterPageState;

  AppState({
    @required this.counterPageState,
  });

  factory AppState.initial() {
    return AppState(
      counterPageState: CounterPageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    return AppState(
      counterPageState: state.counterPageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    CounterPageStateEpics.indexEpic,
  ]);
}
