import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class ButtonSizeExampleScreen extends StatelessWidget {
  static const routeName = '/button-size-example';

  @override
  Widget build(BuildContext context) {
    final routeArgs = ModalRoute.of(context).settings.arguments as Map<String, String>;
    final appBarTitle = routeArgs['title'];
    ScreenUtil.init(context, allowFontScaling: false);

    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                height: 0.2.sh,
                width: 0.4.sw,
                child: RaisedButton(
                  color: Colors.blue,
                  child: Text("Press Me!"),
                  onPressed: () {},
                )),
            SizedBox(
              width: 20,
            ),
            SizedBox(
                height: 0.2.sh,
                width: 0.4.sw,
                child: RaisedButton(
                  color: Colors.blue,
                  child: Text("Press Me!"),
                  onPressed: () {},
                )),
          ],
        ),
      ),
    );
  }
}
