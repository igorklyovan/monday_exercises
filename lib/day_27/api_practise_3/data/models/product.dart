import 'package:flutter/material.dart';

class Product {
  int id;
  String brand;
  String name;
  double price;
  String image;
  String description;
  double rating;

  Product({
    @required this.id,
    @required this.brand,
    @required this.name,
    @required this.price,
    @required this.image,
    @required this.description,
    @required this.rating,
  });

  factory Product.fromJson(Map<String, dynamic> jsonData) {
    return Product(
      id: jsonData['id'],
      brand: jsonData['brand'],
      name: jsonData['name'],
      price: double.parse(jsonData['price']),
      image: jsonData['image_link'],
      description: jsonData['description'],
      rating: jsonData['rating'] != null ?jsonData['rating']: 0,
    );
  }
}
