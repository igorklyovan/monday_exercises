import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyHomePage());
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(),
        body: CustomScrollView(
          slivers: [
            const SliverAppBar(
              floating: true,
              pinned: true,
              expandedHeight: 100,
              flexibleSpace: FlexibleSpaceBar(
                titlePadding: EdgeInsets.symmetric(horizontal: 150, vertical: 10),
                title: Text(
                  'Demo',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(_containers()),
            ),
          ],
        ));
  }
}

List<Widget> _containers() {
  List<Widget> list = [];
  List<Color> colorList = [Colors.yellow, Colors.red, Colors.orange, Colors.purple, Colors.blue, Colors.orange];

  for (int i = 0; i < colorList.length; i++) {
    list.add(Container(
      width: double.infinity,
      height: 200,
      color: colorList[i],
    ));
  }
  return list;
}
