import 'package:exercises/day_19/practise_8_localization/dictionary/flutter_delegate.dart';
import 'package:exercises/day_19/practise_8_localization/dictionary/models/supported_locales.dart';
import 'package:exercises/day_19/practise_8_localization/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../pages/home_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageProvider>(
      builder: (ctx, provider, _) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
          supportedLocales: SupportedLocales.instance.getSupportedLocales,
          localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
          home: HomePage(),
        );
      },
    );
  }
}
