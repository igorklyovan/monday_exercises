import 'package:exercises/day_49/bloc_practise_1/bloc/color/color_events.dart';
import 'package:exercises/day_49/bloc_practise_1/bloc/color/color_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ColorBloc extends Bloc<ColorEvent, ColorState> {

  @override
  Stream<ColorState> mapEventToState(ColorEvent event) async* {
    if (event.runtimeType == ChangeColorEvent) {
      yield ChangedColorState(newColor: (event as ChangeColorEvent).color);
    }
  }

  ColorBloc() : super(ColorEmptyState());
}
