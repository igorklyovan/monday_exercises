import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:exercises/day_42/redux_practice_5/store/pages/counter_state/counter_selectors.dart';
import 'package:exercises/day_42/redux_practice_5/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class CustomDrawerViewModel {
  final void Function(String) push;
  final void Function() incrementCounter;
  final void Function() decrementCounter;


  CustomDrawerViewModel({
    @required this.push,
    @required this.incrementCounter,
    @required this.decrementCounter,
  });

  static CustomDrawerViewModel fromStore(Store<AppState> store) {
    return CustomDrawerViewModel(
      push: NavigationSelectors.push(store),
      incrementCounter: CounterSelectors.incrementCounter(store),
      decrementCounter: CounterSelectors.decrementCounter(store),
    );
  }
}
