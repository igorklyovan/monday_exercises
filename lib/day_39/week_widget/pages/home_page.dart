import 'package:exercises/day_39/week_widget/pages/custom_table.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  bool _checkboxVal = false;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        CheckboxListTile(
          value: _checkboxVal,
          checkColor: Colors.orangeAccent,
          activeColor: Colors.blue,
          title: Text(
            'Light',
            textAlign: TextAlign.center,
          ),
          secondary: Icon(Icons.lightbulb_outline),
          onChanged: (bool value) {
            setState(() {
              _checkboxVal = value;
            });
          },
        ),
        DataTable(sortAscending: true, sortColumnIndex: 0, columns: [
          DataColumn(label: Text('Name')),
          DataColumn(
            label: Text('Points'),
            numeric: true,
          ),
        ], rows: [
          DataRow(cells: [
            DataCell(
              Text('Ferrari'),
              onTap: () {
                _controller.reverse();
              },
            ),
            DataCell(Text('10')),
          ]),
          DataRow(selected: true, cells: [
            DataCell(Text('Porsche'), onTap: () {
              _controller.forward();
            }),
            DataCell(Text('21')),
          ]),
        ]),
        FadeTransition(
          opacity: _controller,
          child: FlutterLogo(
            size: 50.0,
          ),
        ),
        InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (_) => CustomTable(),
            ));
          },
          child: FadeInImage.assetNetwork(
              height: 300,
              placeholder: 'assets/images/snowman.png',
              image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M'
                  'Xx8aHVtYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80'),
        ),
      ],
    ));
  }
}
