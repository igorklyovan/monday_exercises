import '../oop_tasks/request.dart';

class SubRequest extends Request {
  SubRequest(String url) : super(url);

  @override
  String get type => 'Type is http';
}

void main (){
  SubRequest req = SubRequest('google.com');

  print(req.type);
  print(req.url);
}



