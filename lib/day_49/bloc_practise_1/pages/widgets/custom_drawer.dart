import 'package:exercises/day_49/bloc_practise_1/bloc/color/color_bloc.dart';
import 'package:exercises/day_49/bloc_practise_1/bloc/color/color_events.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ColorBloc _colorBloc = BlocProvider.of<ColorBloc>(context);
    return Drawer(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FlatButton(
              onPressed: () {
                _colorBloc.add(ChangeColorEvent(color: Color(0xFFeb5757)));
              },
              color: Colors.red,
              child: Icon(
                Icons.palette,
                color: Colors.black,
              ),
            ),
            FlatButton(
              onPressed: () {
                _colorBloc.add(ChangeColorEvent(color: Color(0xFF5DB075)));
              },
              child: Icon(
                Icons.palette,
                color: Colors.white,
              ),
              color: Colors.green,
            ),
            FlatButton(
              onPressed: () {
                _colorBloc.add(ChangeColorEvent(color: Color(0xFF6e767e)));
              },
              child: Icon(
                Icons.language,
                color: Colors.white,
              ),
              color: Color(0xFF6e767e),
            ),
          ],
        ),
      ),
    );
  }
}
