import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              LimitedBox(
                maxHeight: 200,
                child: Container(
                  color: Colors.purple,
                ),
              ),
              Container(
                height: 200,
                child: FittedBox(
                  fit: BoxFit.cover,
                  child: Text(
                    'Fitted Box',
                    style: TextStyle(fontSize: 500),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              ConstrainedBox(
                constraints: BoxConstraints(maxHeight: 250, maxWidth: 100),
                child: Text(
                  'Constrained Box',
                  textAlign: TextAlign.center,
                ),
              ),
              Tooltip(
                message: 'Document',
                child: Icon(
                  Icons.description,
                  size: 50,
                ),
              ),
              Placeholder(
                fallbackHeight: 150,
                color: Colors.green,
              )
            ],
          ),
        ),
      ),
    );
  }
}
