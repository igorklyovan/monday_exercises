import 'package:exercises/day_14/firebase_example/pages/sign_in.dart';
import 'package:flutter/material.dart';

import '../pages/home_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Provider practise',
      home: SignIn(),
      debugShowCheckedModeBanner: false,
    );
  }
}
