import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_actions.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class ThemeSelectors {
  static Color getMainColor(Store<AppState> store) {
    Color mainColor = store.state.themeState.mainColor;
    return mainColor;
  }

  static Color getSecondColor(Store<AppState> store) {
    Color secondColor = store.state.themeState.secondColor;
    return secondColor;
  }

  static void Function(MaterialColor color) changeMainColor(Store<AppState> store) {
    return (Color color) => store.dispatch(ChangeMainColorAction(color: color));
  }

  static void Function(MaterialColor color) changeSecondColor(Store<AppState> store) {
    return (Color color) => store.dispatch(ChangeSecondColorAction(color: color));
  }
}
