import 'package:flutter/material.dart';

class ColoredPage extends StatefulWidget {
  @override
  _ColoredPageState createState() => _ColoredPageState();
}

class _ColoredPageState extends State<ColoredPage> {
  double aspectRatio = 3/2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Hero(
                tag: 'snowman_colored',
                child: ColorFiltered(
                    colorFilter: ColorFilter.mode(Colors.red, BlendMode.saturation),
                    child: AspectRatio(aspectRatio: aspectRatio,child: Image.asset('assets/images/snowman.png')))),
            Text(
                'Lorem superposés valise pourparlers rêver chiots rendez-vous naissance Eiffel myrtille. Grèves Arc de Triomphe encore pourquoi sentiments baguette pédiluve une projet sentiments saperlipopette vachement le. Brume éphémère baguette Bordeaux en fait sommet avoir minitel.'),
            RaisedButton(child: Text('Ratio'),onPressed: (){
              setState(() {
                aspectRatio = 5/2;

              });

            })
          ],
        ),
      ),
    );
  }
}
