import 'dart:collection';

import 'package:exercises/day_35/redux_study_practise_2/model/brewer.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/app/reducer.dart';
import 'package:flutter/material.dart';

class HomePageState {
  HomePageState();

  factory HomePageState.initial() {
    return HomePageState();
  }

  HomePageState copyWith() {
    return HomePageState();
  }

  HomePageState reducer(dynamic action) {
    return Reducer<HomePageState>(
      actions: HashMap.from({}),
    ).updateState(action, this);
  }

//  HomePageState _changeHomeColor() {
//    return copyWith();
//  }
}
