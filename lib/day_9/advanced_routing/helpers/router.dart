import 'package:flutter/material.dart';


import '../layout_building/button_size_screen.dart';
import '../layout_building/text_size_screen.dart';
import '../layout_building/examples_screen.dart';

class MyRouter {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    const String textExampleRoute = '/text-size-example';
    const String btnExampleRoute = '/button-size-example';

    MaterialPageRoute lastPage;
    MaterialPageRoute currentPage;

    switch (settings.name) {
      case '/':
        currentPage = MaterialPageRoute(builder: (_) => ExamplesScreen());
        lastPage = currentPage;
        print(currentPage);
        return currentPage;
      case textExampleRoute:
        lastPage = currentPage;
        var data = settings.arguments as Map<String, Object>;
        currentPage = MaterialPageRoute(builder: (_) => TextSizeExampleScreen(data));
        print(currentPage);
        return currentPage;
      case btnExampleRoute:
        var data = settings.arguments as Map<String, Object>;
        currentPage = MaterialPageRoute(builder: (_) => ButtonSizeExampleScreen(data));
        lastPage = currentPage;
        print(currentPage);
        return currentPage;
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
