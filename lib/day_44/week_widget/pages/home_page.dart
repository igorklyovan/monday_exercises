import 'package:exercises/day_44/week_widget/pages/widgets/container_transition.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  AnimationController _controller;
  AnimationController _buttonController;
  AnimationController _orangeController;
  Animation<double> _rotateAnimation;
//  Animation<double> _rotateOrangeAnimation;
  bool isCrossFade = true;
  Widget _myAnimatedWidget = FlutterLogo(
    size: 200,
    key: ValueKey(1),
  );

  @override
  void initState() {
    super.initState();
    _buttonController = AnimationController(vsync: this, duration: Duration(seconds: 1))..repeat();
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 50))..repeat();
    _orangeController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );
    _rotateAnimation = Tween<double>(begin: 5, end: 200).animate(_controller);
//    _rotateOrangeAnimation = Tween<double>(begin: 5, end: 200).animate(_orangeController);
    _controller.addListener(update);
    _orangeController.addListener(update);
    _buttonController.addListener(update);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _orangeController.dispose();
    _buttonController.dispose();
    _controller.removeListener(update);
    _orangeController.removeListener(update);
    _buttonController.removeListener(update);
  }

  void update() => setState(() {});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SpinningContainer(
                controller: _buttonController,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    isCrossFade = !isCrossFade;
                  });
                },
                child: AnimatedCrossFade(
                  firstChild: CircleAvatar(
                    minRadius: 15,
                    child: Image.asset(
                      'assets/images/snowman.png',
                      height: 150,
                      width: 150,
                      color: Colors.black,
                    ),
                  ),
                  secondChild: Container(
                    height: 150,
                    width: 150,
                    color: Colors.purple,
                  ),
                  duration: Duration(seconds: 1),
                  crossFadeState: isCrossFade ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                ),
              ),
              AnimatedBuilder(
                animation: _rotateAnimation,
                child: CircleAvatar(
                  minRadius: 15,
                  child: Image.asset(
                    'assets/images/snowman.png',
                    height: 200,
                    width: 300,
                  ),
                ),
                builder: (ctx, child) {
                  return Transform.rotate(
                    angle: _rotateAnimation.value,
                    child: child,
                  );
                },
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    _myAnimatedWidget = Image.asset(
                      'assets/images/snowman.png',
                      height: 200,
                      width: 300,
                      color: Colors.deepOrange,
                      key: ValueKey(2),
                    );
                  });
                },
                child: AnimatedSwitcher(
                  transitionBuilder: (ch, animation) => ScaleTransition(
                    child: ch,
                    scale: animation,
                  ),
                  duration: Duration(seconds: 5),
                  child: _myAnimatedWidget,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
