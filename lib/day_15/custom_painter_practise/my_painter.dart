import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  double radius;

  MyPainter(this.radius);

  @override
  void paint(Canvas canvas, Size size) {
     canvas.drawCircle(Offset(200, 200), radius, (Paint()..color = Colors.yellow));
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
