import 'package:flutter/material.dart';
import 'dart:convert';

class Photo {
  String id;
  Map<String,dynamic> urls;
  String date;

  Photo({@required this.id, @required this.urls, @required this.date});

  factory Photo.fromJson(Map<String, dynamic> jsonData) {
   return Photo(id: jsonData['id'], date: jsonData['updated_at'], urls: jsonData['urls']);
  }
}

//class Urls {
//  String raw;
//  String full;
//  String regular;
//  String small;
//  String thumb;
//
//  Urls({this.raw, this.full, this.regular, this.small, this.thumb});
//
//  Urls.fromJson(Map<String, dynamic> json) {
//    raw = json['raw'];
//    full = json['full'];
//    regular = json['regular'];
//    small = json['small'];
//    thumb = json['thumb'];
//  }
//}
