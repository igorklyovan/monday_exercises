import 'package:flutter/material.dart';
import 'package:exercises/day_9/buttons_and_routes/main_drawer.dart';


import 'package:auto_size_text/auto_size_text.dart';

class TextSizeExampleScreen extends StatelessWidget {
  static const routeName = '/text-size-example';
  final Map<String, Object> data;

  TextSizeExampleScreen(this.data);

  @override
  Widget build(BuildContext context) {
    final appBarTitle = data['title'];

    return Scaffold(
      appBar: AppBar(title: Text(appBarTitle)),
      drawer: MainDrawer(),
      body: Center(
        child: SizedBox(
          width: 200.0,
          height: 140.0,
          child: AutoSizeText(
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
            style: TextStyle(fontSize: 30.0),
            maxLines: 3,
          ),
        ),
      ),
    );
  }
}
