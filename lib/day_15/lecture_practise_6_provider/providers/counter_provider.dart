import 'package:flutter/material.dart';
import 'dart:math';


class CounterProvider with ChangeNotifier {
  int _firstCounter = 0;

  int _secondCounter = 0;

  int get firstCounter => _firstCounter;

  int get secondCounter => _secondCounter;

  void incrementCounter() {
    bool _random = Random().nextBool();

    if (_random) {
      _firstCounter++;
    } else {
      _secondCounter++;
    }
    notifyListeners();
  }

  void decrementCounter() {
    bool _random = Random().nextBool();
    if (_random) {
      _firstCounter--;
    } else {
      _secondCounter--;
    }
    notifyListeners();
  }
}
