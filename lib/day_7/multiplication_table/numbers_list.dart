import 'package:flutter/material.dart';

List<Widget> numbers() {
  Color _containerColor;
  List<Widget> list = [];
  int number;
  Color _textColor;
  for (int i = 0; i <= 10; i++) {
    for (int j = 0; j <= 10; j++) {
      number = i * j;
      _textColor = Colors.green;
      if (i == 0) {
        _containerColor = Color.fromRGBO(161, 200, 68, 1);
        number = j;
        _textColor = Colors.white;
        if (j == 0) {
          number = null;
        }
      } else if (j == 0) {
        _containerColor = Color.fromRGBO(161, 200, 68, 1);
        number = i;
        _textColor = Colors.white;
      } else if (i == j && i != 0 && j != 0) {
        _containerColor = Color.fromRGBO(255, 246, 131, 1);
      } else if (i == 0 && j == 0) {
        number = null;
      } else if (i > j) {
        _containerColor = Color.fromRGBO(254, 246, 168, 0.8);
      } else {
        _containerColor = Colors.white;
      }

      list.add(Container(
        decoration: BoxDecoration(border: Border.all(color: Color.fromRGBO(92, 121, 78, 1)), color: _containerColor),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: FittedBox(
            child: Text(
              '${number == null ? '' : number}',
              style: TextStyle(color: _textColor),
            ),
          ),
        ),
      ));
    }
  }
  return list;
}