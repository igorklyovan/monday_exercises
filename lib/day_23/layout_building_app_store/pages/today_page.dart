import 'package:exercises/day_23/layout_building_app_store/data/dummy_data.dart';
import 'package:exercises/day_23/layout_building_app_store/data/routes.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/custom_bottom_navigation_bar.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/article.dart';
import 'package:exercises/day_23/layout_building_app_store/shared/today_stack.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TodayPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(left: 12.0, right: 12.0, top: 25.0),
        width: double.infinity,
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              automaticallyImplyLeading: false,
              backgroundColor: Colors.transparent,
              expandedHeight: 100.0,
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text('${DateFormat('EEEE, MMMM d').format(DateTime.now())}',
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 40.0,
                        width: 40.0,
                        decoration: BoxDecoration(shape: BoxShape.circle),
                        child: FlutterLogo(),
                      ),
                      Text(
                        'Today',
                        style: TextStyle(
                          fontSize: 30.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (ctx, index) {
                  return InkWell(
                    child: Hero(
                      flightShuttleBuilder: (ctx, anim, direction, ctx1, ctx2) {
                        return Material(
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                TodayStack(
                                  icon: DummyData.icon,
                                  image: DummyData.image,
                                  title: DummyData.title,
                                  genre: DummyData.genre,
                                  price: DummyData.price,
                                  type: DummyData.type,
                                  isRounded: DummyData.isRounded,
                                ),
                                Padding(
                                  padding: EdgeInsets.all(20),
                                  child: Text(
                                    DummyData.dummyText,
                                    style: TextStyle(
                                      fontSize: 19.0,
                                      color: Colors.grey[700],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      tag: 'article_tag$index',
                      child: Column(
                        children: [
                          TodayStack(
                            icon: DummyData.icon,
                            image: DummyData.image,
                            title: DummyData.title,
                            genre: DummyData.genre,
                            type: DummyData.type,
                            price: DummyData.price,
                            isRounded: DummyData.isRounded,
                          ),
                          const SizedBox(
                            height: 23.0,
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).pushNamed(
                        Routes.kArticlePage,
                        arguments: Article(
                          icon: DummyData.icon,
                          image: DummyData.image,
                          title: DummyData.title,
                          type: DummyData.type,
                          genre: DummyData.genre,
                          price: DummyData.price,
                          index: index,
                        ),
                      );
                    },
                  );
                },
                childCount: 3,
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: CustomBottomNavigationBar(),
    );
  }
}
