import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/pages/home_page_vm.dart';
import 'package:exercises/day_38/redux_study_practise_4/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(),
        drawer: CustomDrawer(),
        body: StoreConnector<AppState, HomePageViewModel>(
//    onInitialBuild: (vm) {
//    vm.getBrewers();
//    },
            converter: HomePageViewModel.fromStore,
            builder: (BuildContext context, HomePageViewModel viewModel) {
              return Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                child: SingleChildScrollView(
                  child: SizedBox(
                    height: deviceSize.height * 0.9,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Name of the show',
                            ),
                            controller: _controller,
                            onSubmitted: (showName) async {
//                        viewModel.shows = await ShowsRepository().getShows(showName);
                              viewModel.getShows(showName);
                            },
                          ),
                        ),
                        viewModel.shows.length > 0
                            ? SizedBox(
                                height: deviceSize.height * 0.6,
                                width: deviceSize.width * 0.9,
                                child: ListView.builder(
                                    itemCount: viewModel.shows.length,
                                    itemBuilder: (ctx, i) {
                                      return Container(
                                        height: deviceSize.height * 0.9,
                                        width: deviceSize.width * 0.7,
                                        child: Card(
                                          elevation: 5,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Spacer(),
                                              ClipRRect(
                                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                                child: Image.network(
                                                  viewModel.shows[i].image,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
//                              Spacer(flex: 3,),
                                              const SizedBox(height: 10),
                                              Text(
                                                '${viewModel.shows[i].name}',
                                                style: TextStyle(fontWeight: FontWeight.bold),
                                              ),
                                              Spacer(),
                                              Text('genre: ${viewModel.shows[i].genres}'),
                                              Spacer(),
                                              Text('language: ${viewModel.shows[i].language}'),
                                              Spacer(),
                                            ],
                                          ),
                                        ),
                                      );
                                    }),
                              )
                            : Image.asset('assets/images/snowman.png'),
                      ],
                    ),
                  ),
                ),
              );
            }));
  }
}
