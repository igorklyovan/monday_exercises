import 'package:exercises/day_44/redux_practice_6/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_44/redux_practice_6/pages/another_page_vm.dart';
import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:flutter/material.dart';

class AnotherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AnotherPageViewModel>(
      converter: AnotherPageViewModel.fromStore,
      builder: (BuildContext context, AnotherPageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(),
          drawer: CustomDrawer(),
          backgroundColor: viewModel.mainColor,
          body: Center(
            child: Text(
              viewModel.text,
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.bold,
                color: viewModel.secondColor,
              ),
            ),
          ),
        );
      },
    );
  }
}
