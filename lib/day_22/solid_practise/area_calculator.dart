import 'package:exercises/day_22/solid_practise/shape.dart';

class AreaCalculator {
  List<Shape> shapes;

  AreaCalculator(this.shapes);

  void output() {
    shapes.forEach((element) {
      print(element.runtimeType);
    });
  }

  void sum() {
    shapes.forEach((element) {
      print(element.area());
    });
  }
}
