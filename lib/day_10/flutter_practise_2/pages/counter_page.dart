import 'package:flutter/material.dart';
import '../widgets/button.dart';
import '../pages/image_page.dart';

class CounterPage extends StatefulWidget {
  static final String routeName = '/';

  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  int counter = 0;

  void incrementCounter() {
    setState(() {
      counter++;
    });
  }

  void decrementCounter() {
    setState(() {
      counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MyButton(Icons.arrow_back, CounterPage.routeName),
                MyButton(Icons.arrow_forward, ImagesPage.routeName),
              ],
            ),
          ),
          Text(
            '$counter',
            style: TextStyle(fontSize: 25),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FlatButton(
                  onPressed: incrementCounter,
                  child: Icon(Icons.add),
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0), side: BorderSide(color: Colors.blue)),
                  color: Colors.green,
                ),
                FlatButton(
                  onPressed: decrementCounter,
                  child: Icon(Icons.remove),
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0), side: BorderSide(color: Colors.blue)),
                  color: Colors.red,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
