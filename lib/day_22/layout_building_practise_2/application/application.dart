import 'package:exercises/day_22/layout_building_practise_2/provider/pointer_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../pages/home_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Provider practise',
      home: ChangeNotifierProvider.value(value: PointerProvider(), child: HomePage()),
      debugShowCheckedModeBanner: false,
    );
  }
}
