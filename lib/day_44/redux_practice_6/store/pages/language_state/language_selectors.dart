import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/language_state/language_actions.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_actions.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class LanguageSelectors {
  static String getMainName(Store<AppState> store) {
    String mainText = store.state.languageState.mainText;
    return mainText;
  }

  static String getAnotherName(Store<AppState> store) {
    String anotherText = store.state.languageState.anotherText;
    return anotherText;
  }

  static String getThirdName(Store<AppState> store) {
    String thirdText = store.state.languageState.thirdText;
    return thirdText;
  }

  static void Function() changeLanguage(Store<AppState> store) {
    return () => store.dispatch(ChangeLanguageAction());
  }
}
