import 'package:flutter/material.dart';

import './pages/home_page.dart';
import './pages/black_page.dart';
import './pages/purple_page.dart';
import './pages/yellow_page.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 120,
              width: double.infinity,
              padding: EdgeInsets.all(20),
              alignment: Alignment.centerLeft,
              color: Colors.grey,
              child: Text(
                "Navigation Menu",
                style: TextStyle(fontSize: 25),
              ),
            ),
            Divider(),
            SizedBox(
              height: 20,
            ),
            CustomButton('Red page', HomePage.routeName),
            CustomButton('Purple page', PurplePage.routeName),
            CustomButton('Yellow page', YellowPage.routeName),
            CustomButton('Black page', BlackPage.routeName),
          ],
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  String title;
  String routeName;

  CustomButton(this.title, this.routeName);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: InkWell(
        child: Container(
          height: 600,
          child: Card(
            elevation: 5,
            child: Center(child: Text(title)),
          ),
        ),
        onTap: () {
          Navigator.of(context).pushReplacementNamed(routeName);
        },
      ),
    );
  }
}
