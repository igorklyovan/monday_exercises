import 'package:flutter/material.dart';

class ProductDTO {
  int id;
  String brand;
  String name;
  double price;
  String image;
  String description;
  double rating;

  ProductDTO({
    @required this.id,
    @required this.brand,
    @required this.name,
    @required this.price,
    @required this.image,
    @required this.description,
    @required this.rating,
  });

  factory ProductDTO.fromJson(Map<String, dynamic> jsonData) {
    return ProductDTO(
      id: jsonData['id'],
      brand: jsonData['brand'],
      name: jsonData['name'],
      price: jsonData['price'],
      image: jsonData['image_link'],
      description: jsonData['description'],
      rating: jsonData['rating'],
    );
  }
}
