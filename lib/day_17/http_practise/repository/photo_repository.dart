import 'package:exercises/day_17/http_practise/service/photo_service.dart';
import 'dart:convert';
import '../models/photo.dart';

class PhotoRepository {
  Future<List<Photo>> getPhotos() async {
    List<Photo> photos = [];

    var response = await PhotoService().getPhotos();
    List jsonData = json.decode(response);

    for (int i = 0; i < jsonData.length; i++) {
      photos.add(Photo.fromJson(jsonData[i]));
    }
    return photos;
  }
}
