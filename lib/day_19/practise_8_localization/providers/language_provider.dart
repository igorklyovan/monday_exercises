import 'package:exercises/day_19/practise_8_localization/dictionary/flutter_dictionary.dart';
import 'package:flutter/material.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLane(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}
