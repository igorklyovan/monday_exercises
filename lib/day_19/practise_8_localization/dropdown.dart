import 'package:exercises/day_19/practise_8_localization/dictionary/models/language.dart';
import 'package:exercises/day_19/practise_8_localization/providers/language_provider.dart';

import 'dictionary/flutter_delegate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'dictionary/flutter_dictionary.dart';

class MainDropDown extends StatefulWidget {
  String dropdownValue = FlutterDictionaryDelegate.getCurrentLocale;

  @override
  _MainDropDownState createState() => _MainDropDownState();
}

class _MainDropDownState extends State<MainDropDown> {
  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageProvider>(builder: (ctx, provider, ch) {
      return Directionality(
        textDirection: TextDirection.rtl,
        child: DropdownButton<String>(
          value: widget.dropdownValue,
          onChanged: (String newValue) {
            setState(() {
              widget.dropdownValue = newValue;
              provider.setNewLane(Locale(newValue));
              if (FlutterDictionary(Locale(FlutterDictionaryDelegate.getCurrentLocale)).isRTL) {
                print('llll');
                Scaffold.of(context).showSnackBar(SnackBar(content: Text("This is RTL language!")));
              }
            });
          },
          items: FlutterDictionaryDelegate.getSupportedLocales.map((val) {
            return DropdownMenuItem(
              child: Text(
                val.toString(),
                style: TextStyle(fontSize: 21),
              ),
              value: val.toString(),
            );
          }).toList(),
        ),
      );
    });
  }
}
