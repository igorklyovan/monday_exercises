import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MySlider extends StatefulWidget {
  @override
  _MySliderState createState() => _MySliderState();
}

class _MySliderState extends State<MySlider> {
  double _currentValue = 0;
  RangeValues selectedRange = RangeValues(0.1, 0.5);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Slider(
            min: 0,
            max: 20,
            value: _currentValue,
            label: _currentValue.round().toString(),
            onChanged: (value) {
              setState(() {
                _currentValue = value;
              });
            }),
        RangeSlider(
            values: selectedRange,
            onChanged: (newRange) {
              setState(() {
                selectedRange = newRange;
              });
            }),
        CupertinoSlider(
            min: 0,
            max: 20,
            value: _currentValue,
            onChanged: (value) {
              setState(() {
                _currentValue = value;
              });
            }),
      ],
    );
  }
}
