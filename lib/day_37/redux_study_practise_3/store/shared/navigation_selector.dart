import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';

class NavigationSelectors {
  static void Function(String route) push(Store<AppState> store) {
    return (String route) {
      return store.dispatch(NavigateToAction.push(route));
    };
  }
}
