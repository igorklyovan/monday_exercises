import 'package:exercises/day_50/bloc_practise_2/bloc/images/image_event.dart';
import 'package:exercises/day_50/bloc_practise_2/bloc/images/image_state.dart';
import 'package:exercises/day_50/bloc_practise_2/model/splash_photo.dart';
import 'package:exercises/day_50/bloc_practise_2/reposity/photo_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ImageBloc extends Bloc<ImageEvent, ImageState> {

  @override
  Stream<ImageState> mapEventToState(ImageEvent event) async* {


    if (event.runtimeType == FetchImageEvent) {
      print('11111');
      final List<SplashPhoto> _splashPhotos = await PhotoRepository().getPhotos();
      print(_splashPhotos);

      yield ImageFetchedState(photos: _splashPhotos);
    }
  }

  ImageBloc() : super(ImageEmptyState());
}
