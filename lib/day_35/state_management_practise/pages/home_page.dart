import 'package:exercises/day_35/state_management_practise/counter_page_viewmodel.dart';
import 'package:exercises/day_35/state_management_practise/store/app/app_state.dart';
import 'package:exercises/day_35/state_management_practise/store/pages/counter_page_state/counter_page_selectors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
//      onInit: (Store<AppState> store) => CounterPageSelectors.getCounterValue(store),
//      onInitialBuild: (CounterPageViewModel vm) => {print('Initial Bild Hello')},
      builder: (BuildContext context, CounterPageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(),
          backgroundColor: viewModel.homeColor,
          drawer: Drawer(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlatButton(
                    onPressed: () {
                      viewModel.changeHomerColor(Colors.green);
                    },
                    color: Colors.green,
                    child: Text("Green"),
                  ),
                  FlatButton(
                    onPressed: () {
                      viewModel.changeHomerColor(Colors.red);
                    },
                    child: Text("Red"),
                    color: Colors.red,
                  ),
                  FlatButton(
                    onPressed: () {
                      viewModel.changeHomerColor(Colors.orangeAccent);
                    },
                    child: Text("Orange"),
                    color: Colors.orangeAccent,
                  ),
                ],
              ),
            ),
          ),
          body: Center(
            child: Text('Redux'),
          ),
        );
      },
    );
  }
}
