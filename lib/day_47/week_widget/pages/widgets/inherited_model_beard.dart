import 'package:flutter/cupertino.dart';

class InheritedModelBeard extends InheritedModel<String> {
  final String beardType;

  InheritedModelBeard({this.beardType, Widget child}) : super(child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => true;

  static InheritedModelBeard of(BuildContext context) => context.inheritFromWidgetOfExactType(InheritedModelBeard);

  @override
  bool updateShouldNotifyDependent(covariant InheritedModel oldWidget, Set dependencies) {
    if(dependencies.contains('first')
//    &&
//        beardType != oldWidget.
    ){
      return true;
    }
    return false;

  }
}
