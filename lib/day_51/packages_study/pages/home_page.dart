import 'package:exercises/day_51/packages_study/model/splash_photo.dart';
import 'package:exercises/day_51/packages_study/reposity/photo_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        initialData: [],
        future: PhotoRepository().getPhotos(),
        builder: (ctx, snapshot) {
          return snapshot.connectionState == ConnectionState.done
              ? GridView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        SizedBox(
                          height: 200.0,
                          width: 250.0,
                          child: BlurHash(
                            image: '${(snapshot.data as List<SplashPhoto>)[index].url}',
                            hash: (snapshot.data as List<SplashPhoto>)[index].blurHash,
                          ),
                        ),
                      ],
                    );
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2))
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
