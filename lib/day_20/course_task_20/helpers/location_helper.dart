const kGoogleApiKey = 'pk.eyJ1IjoiaWhvcmtseW92YW4iLCJhIjoiY2tqOXlycmlvMDVwcTM0bWU1cDk0YXBjdSJ9.R3L6cl0lVoRp5Lw1jsTkHA';

class LocationHelper {
  static String generateLocationPreviewImage({
    double latitude,
    double longitude,
  }) {
    return "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/pin-s-l+000($longitude,$latitude)/$longitude,$latitude,14,0,60/600x600?access_token=pk.eyJ1IjoiaWhvcmtseW92YW4iLCJhIjoiY2tqOXlycmlvMDVwcTM0bWU1cDk0YXBjdSJ9.R3L6cl0lVoRp5Lw1jsTkHA";
  }
}
