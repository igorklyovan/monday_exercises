enum CounterEvent {
  incrementEvent,
  decrementEvent,
  clear
}