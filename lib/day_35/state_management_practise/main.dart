import 'package:exercises/day_35/state_management_practise/store/app/app_state.dart';
import 'package:exercises/day_35/state_management_practise/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'counter_page_viewmodel.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
//    middleware: [
//      EpicMiddleware(AppState.getAppEpic),
//      NavigationMiddleware<AppState>(),
//    ],
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Redux practise',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(title: 'Redux practise Home Page'),
      ),
    );
  }
}
