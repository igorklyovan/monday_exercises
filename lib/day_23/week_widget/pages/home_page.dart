import 'package:exercises/day_23/week_widget/widgets/my_slider.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          tabs: [Tab(text: "RichText"), Tab(text: "Slider"), Tab(text: "Builder")],
        ),
      ),
      body: TabBarView(
        children: [
          Tab(
            child: Center(
              child: RichText(
                text: TextSpan(
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.black,
                    ),
                    children: [
                      TextSpan(text: 'Its '),
                      TextSpan(text: 'my ', style: TextStyle(fontSize: 30, color: Colors.purple)),
                      TextSpan(text: 'rich', style: TextStyle(fontSize: 30, color: Colors.blue)),
                      TextSpan(text: 'text!', style: TextStyle(fontSize: 40, color: Colors.pinkAccent))
                    ]),
              ),
            ),
          ),
          Tab(child: MySlider()),
          Tab(
            child: Center(
              child: Builder(builder: (ctx) {
                return RaisedButton.icon(
                    onPressed: () {
                      Scaffold.of(ctx).showSnackBar(
                        const SnackBar(
                          content: Text('This SnackBar was created using Builder'),
                        ),
                      );
                    },
                    icon: const Icon(Icons.details),
                    label: const Text('Snackbar'));
              }),
            ),
          )
        ],
      ),
    );
  }
}
