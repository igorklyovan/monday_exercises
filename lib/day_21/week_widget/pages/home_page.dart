import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_21/week_widget/pages/another_page.dart';
import 'package:exercises/day_21/week_widget/pages/colored_page.dart';
import 'package:exercises/day_21/week_widget/pages/page_view_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(400)),
                child: InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => AnotherPage()));
                    },
                    child: SizedBox(
                        height: 100,
                        width: 100,
                        child: Hero(tag: 'snowman', child: Image.asset('assets/images/snowman.png'))))),
            ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(400)),
                child: InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ColoredPage()));
                    },
                    child: SizedBox(
                        height: 100,
                        width: 100,
                        child: Hero(tag: 'snowman_colored', child: Image.asset('assets/images/snowman.png'))))),
            RaisedButton(
                child: Text('PageView'),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => PageViewPage()));
                }),
          ],
        ),
      ),
    );
  }
}
