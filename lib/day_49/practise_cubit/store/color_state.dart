import 'package:flutter/material.dart';

abstract class ColorState {
  const ColorState();
}

class ColorInitial extends ColorState {
  const ColorInitial();
}

class ColorChange extends ColorState {
  final Color color;

  const ColorChange({@required this.color});

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ColorChange && o.color == color;
  }

  @override
  int get hashCode => color.hashCode;
}
