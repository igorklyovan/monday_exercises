import 'package:exercises/day_8/layout_building/button_size_screen.dart';
import 'package:exercises/day_8/layout_building/text_size_screen.dart';
import 'package:flutter/material.dart';

class ExamplesScreen extends StatelessWidget {
  final String textSizeString = 'Text Size Example';
  final String buttonSizeString = 'Button Size Example';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Layout Building'),
      ),
      body: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedButton(
            textColor: Colors.white,
            color: Theme.of(context).primaryColor,
            child: Text(textSizeString),
            onPressed: () {
              Navigator.of(context).pushNamed(TextSizeExampleScreen.routeName, arguments: {'title': textSizeString});
            },
          ),
          SizedBox(
            width: 20,
          ),
          RaisedButton(
            textColor: Colors.white,
            color: Theme.of(context).accentColor,
            child: Text(buttonSizeString),
            onPressed: () {
              Navigator.of(context)
                  .pushNamed(ButtonSizeExampleScreen.routeName, arguments: {'title': buttonSizeString});
            },
          )
        ],
      )),
    );
  }
}
