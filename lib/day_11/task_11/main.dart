import 'package:exercises/day_11/task_11/my_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/items.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Items(),
      child: MaterialApp(
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
//  String title = ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Consumer<Items>(
                builder: (ctx, item, child) => Container(
                  decoration: BoxDecoration(border: Border.all(width: 5,color: Colors.orange)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                          item.selectedItem,
                          style: TextStyle(fontSize: 100),
                        ),
                  ),
                )),
            SizedBox(height: 50),
            MyDropDown(),
          ],
        ),
      ),
    );
  }
}
