import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  static const String routeName = '/home';

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    final _color = Color.fromRGBO(233, 93, 85, 1);

    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(color: Color.fromRGBO(244, 234, 221, 1)),
          ),
          Positioned(
            right: mediaQuery.width * 0.06,
            bottom: mediaQuery.height * 0.44,
            child: Container(
              height: mediaQuery.height * 0.65,
              width: mediaQuery.width * 0.6,
              decoration: BoxDecoration(border: Border.all(width: 2, color: _color), shape: BoxShape.circle),
            ),
          ),
          Positioned(
            left: mediaQuery.width * 0.5,
            bottom: mediaQuery.height * 0.56,
            child: Container(
              height: mediaQuery.height * 0.6,
              width: mediaQuery.width * 0.85,
              decoration: BoxDecoration(shape: BoxShape.circle, color: _color),
            ),
          ),
          Positioned(
            left: mediaQuery.width * 0.85,
            top: mediaQuery.height * 0.1,
            child: Container(
              height: mediaQuery.height * 0.3,
              width: mediaQuery.width * 0.3,
              decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(width: 2, color: Colors.white)),
            ),
          ),
          Positioned(
            left: mediaQuery.width * 0.03,
            top: mediaQuery.height * 0.52,
            child: Container(
              height: mediaQuery.height * 0.965,
              width: mediaQuery.width * 0.35,
              decoration: BoxDecoration(
                border: Border.all(width: 2, color: _color),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            right: mediaQuery.width * 0.75,
            top: mediaQuery.height * 0.45,
//            320,
            child: Container(
              height: mediaQuery.height * 1,
              width: mediaQuery.width * 0.45,
              decoration: BoxDecoration(color: _color, shape: BoxShape.circle),
            ),
          ),
          Positioned(
            right: mediaQuery.width * 0.9,
            top: mediaQuery.height * 0.78,
            child: Container(
              height: mediaQuery.height * 0.11,
              width: mediaQuery.width * 0.2,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 2,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Positioned(
            child: SafeArea(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: mediaQuery.height * 0.01, vertical: mediaQuery.height * 0.02),
                padding: EdgeInsets.only(bottom: mediaQuery.height * 0.001),
                height: mediaQuery.height * 0.09,
                width: mediaQuery.width * 0.9,
                child: AppBar(
                  title: Text(
                    "Custom AppBar",
                    style: TextStyle(color: Colors.black, fontSize: mediaQuery.height * 0.02),
                  ),
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  shape: Border.symmetric(horizontal: BorderSide(color: Colors.black)),
                  actions: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.menu,
                        color: Colors.black,
                        size: mediaQuery.height * 0.02,
                      ),
                      onPressed: () {},
                      tooltip: 'Menu',
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
