import 'package:exercises/day_23/layout_building_app_store/data/routes.dart';
import 'package:exercises/day_23/layout_building_app_store/pages/article_page.dart';
import 'package:exercises/day_23/layout_building_app_store/pages/today_page.dart';
import 'package:exercises/day_23/layout_building_app_store/pages/unknown_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class RouteHelper {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.kTodayPage:
        {
          return MaterialPageRoute(builder: (_) => TodayPage(), settings: settings);
        }
      case Routes.kArticlePage:
        {
          return PageRouteBuilder(
            pageBuilder: (ctx, animation, secondAnimation) {
              return ArticlePage();
            },
            settings: settings,
            transitionDuration: const Duration(milliseconds: 700),
            reverseTransitionDuration: const Duration(milliseconds: 700),
          );
        }

      default:
        return _defaultRoute(
          settings: settings,
          page: UnknownPage(),
        );
    }
  }

  static MaterialPageRoute _defaultRoute({RouteSettings settings, Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
