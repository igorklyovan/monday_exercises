import './models/human.dart';

final List<Human> humans = [
  Human('Jack', 'Dickinson', 'Seller', 25),
  Human('Paul', 'Walson', 'Driver', 21),
  Human('Eugene', 'Egenus', 'Enterpreuner', 28),
  Human('John', 'Dicen', 'SEO', 29),
  Human('Jerry', 'Tom', 'Viewer', 30),
  Human('Som', 'Dom', 'Seller', 31),
  Human('Dickinson', 'Dom', 'Seller', 32),
  Human('Eugene', 'Dom', 'Seller', 71),
  Human('Walson', 'Paul', 'Seller', 91),
];
