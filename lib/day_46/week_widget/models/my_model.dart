import 'package:flutter/material.dart';

class MyModel {
  String id;
  String title;

  MyModel({
    @required this.id,
    @required this.title,
  });
}
