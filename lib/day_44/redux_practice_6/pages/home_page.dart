import 'package:exercises/day_44/redux_practice_6/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/pages/home_page_vm.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
      converter: HomePageViewModel.fromStore,
      builder: (BuildContext context, HomePageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(),
          backgroundColor: viewModel.mainColor,
          drawer: CustomDrawer(),
          body: Center(
            child: Text(
              viewModel.text,
              style: TextStyle(
                fontSize: 26.0,
                fontWeight: FontWeight.bold,
                color: viewModel.secondColor,
              ),
            ),
          ),
        );
      },
    );
  }
}
