import 'package:exercises/day_42/redux_practice_5/store/pages/counter_state/counter_state.dart';
import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  CounterState counterState;

  AppState({@required this.counterState});

  factory AppState.initial() {
    return AppState(
      counterState: CounterState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    return AppState(
      counterState: state.counterState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([]);
}
