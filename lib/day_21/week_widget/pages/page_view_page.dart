import 'package:exercises/day_21/week_widget/pages/another_page.dart';
import 'package:exercises/day_21/week_widget/pages/colored_page.dart';
import 'package:exercises/day_21/week_widget/pages/home_page.dart';
import 'package:flutter/material.dart';

class PageViewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final controller = PageController(initialPage: 1);

    return Scaffold(
        body: PageView(
      controller: controller,
      children: [
        AnotherPage(),
        ColoredPage(),
        HomePage(),
      ],
    ));
  }
}
