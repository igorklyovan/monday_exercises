import 'package:flutter/material.dart';
class ChangeMainColorAction{
  final Color color;

  ChangeMainColorAction({@required this.color});
}

class ChangeSecondColorAction{
  final Color color;

  ChangeSecondColorAction({@required this.color});
}