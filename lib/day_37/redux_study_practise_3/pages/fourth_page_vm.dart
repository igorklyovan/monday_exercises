import 'package:exercises/day_37/redux_study_practise_3/store/app/app_state.dart';
import 'package:exercises/day_37/redux_study_practise_3/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class FourthPageViewModel {
  final void Function(String) push;

  FourthPageViewModel({
    @required this.push,
  });

  static FourthPageViewModel fromStore(Store<AppState> store) {
    return FourthPageViewModel(
      push: NavigationSelectors.push(store),
    );
  }
}
