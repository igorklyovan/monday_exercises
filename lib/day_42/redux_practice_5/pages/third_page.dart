import 'package:exercises/day_42/redux_practice_5/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:exercises/day_42/redux_practice_5/pages/third_page_vm.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: CustomDrawer(),
      backgroundColor: Colors.purple,
      body: StoreConnector<AppState, ThirdPageViewModel>(
        converter: ThirdPageViewModel.fromStore,
        builder: (BuildContext context, ThirdPageViewModel viewModel) {
          return Center(
            child: Text(
              '${viewModel.counter*10}',
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          );
        },
      ),
    );
  }
}
