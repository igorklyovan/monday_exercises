import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/titles.dart';

class MyButton extends StatelessWidget {
  String title;

  MyButton(this.title);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Colors.blue)),
        child: Text(title),
        onPressed: () {
          Provider.of<Titles>(context, listen: false).pageTitle = title;
        },
      ),
    );
  }
}
