import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/repository/brewery_repository.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/another_page_state/another_page_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/model/brewer.dart';
import 'package:rxdart/rxdart.dart';

class AnotherPageStateEpics{
  static final indexEpic = combineEpics<AppState>([
    getDataEpic,
//    getData2Epic,
  ]);

  static Stream<dynamic> getDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetBrewerAction>().asyncMap((event) async{
      print('EPIC');

//      return AddBrewerAction(brewers: getListFromBody((await http.get('')).body));
      List<Brewer> brewers = await BreweryRepository().getBreweries();
      return AddBrewerAction(brewers: brewers);
    });
  }

}