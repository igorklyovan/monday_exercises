import '../res/consts.dart';
import 'package:http/http.dart' as http;

class ShowsService {
  Future<dynamic> getShows(String showsName) async {
    var response = await http.get('${Constants.kShowsQUrl}$showsName');
    return response.body;
  }
}
