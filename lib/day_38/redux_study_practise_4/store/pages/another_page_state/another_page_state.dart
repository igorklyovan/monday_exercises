import 'dart:collection';

import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/model/brewer.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/another_page_state/another_page_actions.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/reducer.dart';
import 'package:flutter/material.dart';

class AnotherPageState {
  final List<Brewer> brewers;

  AnotherPageState({@required this.brewers});

  factory AnotherPageState.initial() {
    return AnotherPageState(
      brewers: [],
    );
  }

  AnotherPageState copyWith({List<Brewer> brewers}) {
    return AnotherPageState(
      brewers: brewers ?? this.brewers
    );
  }

  AnotherPageState reducer(dynamic action) {
    return Reducer<AnotherPageState>(
      actions: HashMap.from({
        AddBrewerAction: (dynamic action) => _addBrewers(action.brewers),
      }),
    ).updateState(action, this);
  }

  AnotherPageState _addBrewers(List<Brewer> brewers) {
    return copyWith(brewers: brewers);
  }

//  AnotherPageState _changeHomeColor(ChangeHomeColorAction action) {
//    return copyWith();
//  }
}
