import 'package:exercises/day_33/redux_study_practise_1/store/pages/counter_page_state/counter_page_state.dart';
import 'package:flutter/foundation.dart';

class AppState {
  final CounterPageState counterPageState;

  AppState({
    @required this.counterPageState,
  });

  factory AppState.initial() {
    return AppState(
      counterPageState: CounterPageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    return AppState(
      counterPageState: state.counterPageState.reducer(action),
    );
  }
}
