import 'dart:convert';

import 'package:exercises/day_24/api_practise_2/data/models/show.dart';
import 'package:exercises/day_24/api_practise_2/service/shows_service.dart';

class ShowsRepository {
  Future<List<Show>> getShows(String showsName) async {
    List<Show> shows = [];

    var response = await ShowsService().getShows(showsName);
    final jsonData = json.decode(response);

    for (int i = 0; i < jsonData.length; i++) {
      shows.add(Show.fromJson(jsonData[i]['show']));
    }

    return shows;
  }
}
