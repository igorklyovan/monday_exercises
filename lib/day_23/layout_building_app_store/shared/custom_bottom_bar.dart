import 'package:flutter/material.dart';

import 'article.dart';

class CustomBottomBar extends StatelessWidget {
  final Article article;

  CustomBottomBar({@required this.article});

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFFCF4FD),
//        color: Colors.blue,

        borderRadius: BorderRadius.circular(12.0),
      ),
      width: double.infinity,
      height: 50.0,
      margin: EdgeInsets.symmetric(
        horizontal: 5.0,
        vertical: 10.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            height: deviceSize.height * 0.035,
            width: deviceSize.width * 0.2,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              color: Color(0xFFECE4EE),
            ),
            child: Center(
              child: Text(
                '\$${article.price}',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8, right: 2, top: 8),
            child: Row(
              children: [
                SizedBox(
                  height: deviceSize.height * 0.05,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        article.title,
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                      Text(
                        article.genre,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14.0,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: deviceSize.height * 0.01,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8),
                    ),
                    color: Color(0xFFFFFAF6),
                  ),
                  child: Center(
                    child: Icon(
                      article.icon,
                      size: 23.0,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
