import 'package:flutter/material.dart';
import '../advanced_routing/layout_building/button_size_screen.dart';
import '../advanced_routing/layout_building/text_size_screen.dart';

class MainDrawer extends StatelessWidget {
  final String textSizeString = 'Text Size Example';
  final String buttonSizeString = 'Button Size Example';

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Colors.grey,
            child: Text(
              "Navigation Menu",
              style: TextStyle(fontSize: 25),
            ),
          ),
          Divider(),
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: InkWell(
              child: Container(
                height: 50,
                child: Card(
                  elevation: 5,
                  color: Colors.white,
                  child: Center(child: Text('TextSize')),
                ),
              ),
              onTap: () {
                Navigator.of(context).pushNamed(TextSizeExampleScreen.routeName, arguments: {'title': textSizeString});
              },
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: InkWell(
              child: Container(
                height: 50,
                child: Card(
                  elevation: 5,
                  child: Center(child: Text('ButtonSize')),
                ),
              ),
              onTap: () {
                Navigator.of(context)
                    .pushNamed(ButtonSizeExampleScreen.routeName, arguments: {'title': buttonSizeString});
              },
            ),
          ),
        ],
      ),
    );
  }
}
