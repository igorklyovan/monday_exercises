import 'package:exercises/day_49/practise_cubit/store/color_cubit.dart';
import 'package:exercises/day_49/practise_cubit/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_49/practise_cubit/store/color_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ColorCubit, ColorState>(
//        listener: (ctx, state) {
//      if (state is ColorChange) {
//        print(state.color);
//      }
//    },
        builder: (ctx, state) {
      if (state is ColorChange) {
        return Scaffold(
          backgroundColor: state.color,
          appBar: AppBar(),
          drawer: CustomDrawer(),
          body: Container(),
        );
      } else {
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(),
          drawer: CustomDrawer(),
          body: Container(),
        );
      }
    });
  }

}
