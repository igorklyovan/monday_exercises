import 'package:exercises/day_44/redux_practice_6/pages/widgets/custom_drawer_vm.dart';
import 'package:exercises/day_44/redux_practice_6/res/routes.dart';
import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_49/practise_cubit/store/color_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FlatButton(
              onPressed: () {
                changeColor(context, Color(0xFFeb5757));
//                    viewModel.changeMainColor(Color(0xFF9C27B0));
//                    viewModel.changeSecondColor(Color(0xFF000000));
              },
              color: Colors.red,
              child: Icon(
                Icons.palette,
                color: Colors.black,
              ),
            ),
            FlatButton(
              onPressed: () {
                changeColor(context, Color(0xFF5DB075));
//                    viewModel.changeMainColor(Color(0xFFFF5722));
//                    viewModel.changeSecondColor(Color(0xFFFFFFFF));
              },
              child: Icon(
                Icons.palette,
                color: Colors.white,
              ),
              color: Colors.green,
            ),
            FlatButton(
              onPressed: () {
                changeColor(context, Color(0xFF6e767e));
//                    viewModel.changeLanguage();
              },
              child: Icon(
                Icons.language,
                color: Colors.white,
              ),
              color: Color(0xFF6e767e),
            ),
            Row(
              children: [
                const SizedBox(width: 5.0),
                FlatButton(
                  onPressed: () {
//                        viewModel.push(Routes.kHomePage);
                  },
                  child: Text(
                    "1",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.grey,
                ),
                const SizedBox(width: 5.0),
                FlatButton(
                  onPressed: () {
//                        viewModel.push(Routes.kAnotherPage);
                  },
                  child: Text(
                    "2",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.grey,
                ),
                const SizedBox(width: 5.0),
                FlatButton(
                  onPressed: () {
//                        viewModel.push(Routes.kThirdPage);
                  },
                  child: Text(
                    "3",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.grey,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void changeColor(BuildContext ctx, Color color) {
    final colorCibut = ctx.bloc<ColorCubit>();
    colorCibut.changeColor(color);
  }
}
