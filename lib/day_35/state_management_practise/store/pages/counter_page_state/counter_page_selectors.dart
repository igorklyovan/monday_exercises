import 'package:exercises/day_35/state_management_practise/store/app/app_state.dart';
import 'package:exercises/day_35/state_management_practise/store/pages/counter_page_state/counter_page_actions.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class CounterPageSelectors {

  static Color getHomeColorValue(Store<AppState> store) {


    return store.state.counterPageState.homeColor;
  }







  static void Function(Color color) getChangeHomeColorFunction(Store<AppState> store) {

    return (Color color) => store.dispatch(ChangeHomeColorAction(newColor: color));
  }
//  static void Function() getChangeAppBarColorFunction(Store<AppState> store) {
//    return () => store.dispatch(ChangeAppBarColorAction());
//  }
//
//  static void Function(double counter) getMultiplyCounterFunction(Store<AppState> store) {
//    return (double counter) {
//      double counter = store.state.counterPageState.counter;
//      store.dispatch(MultiplyCounterAction(counter2: counter));
//    };
//  }
}
