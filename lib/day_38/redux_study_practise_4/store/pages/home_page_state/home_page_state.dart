import 'dart:collection';

import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/model/brewer.dart';
import 'package:exercises/day_38/redux_study_practise_4/model/show.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/reducer.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/home_page_state/home_page_actions.dart';
import 'package:flutter/material.dart';

class HomePageState {
  final List<Show> shows;


  HomePageState({@required this.shows});

  factory HomePageState.initial() {
    return HomePageState(shows: []);
  }

  HomePageState copyWith({List<Show> shows}) {
    return HomePageState(shows: shows ?? this.shows);
  }

  HomePageState reducer(dynamic action) {
    return Reducer<HomePageState>(
      actions: HashMap.from({
        AddShowsAction: (dynamic action) => _addShows(action.shows),

      }),
    ).updateState(action, this);
  }

  HomePageState _addShows(List<Show> shows) {
    return copyWith(shows: shows);
  }

//  HomePageState _changeHomeColor() {
//    return copyWith();
//  }
}
