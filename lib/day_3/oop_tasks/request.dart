
class Request {
 final String _url;
 final String _type = 'Https';

  Request(this._url);

 String get type => _type;

  String get url => _url;
  }
