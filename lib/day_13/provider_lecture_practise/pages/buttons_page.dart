import 'package:exercises/day_13/provider_lecture_practise/widgets/my_button.dart';
import 'package:flutter/material.dart';
import '../main_drawer.dart';

class ButtonsPage extends StatelessWidget {
  static const String routeName = '/buttons-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter Buttons'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            MyButton(Icons.add, ButtonAction.plus),
            MyButton(Icons.remove, ButtonAction.minus),
          ],
        ),
      ),
    );
  }
}
