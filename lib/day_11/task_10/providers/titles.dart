import 'package:flutter/material.dart';

class Titles with ChangeNotifier {
  String _pageTitle = "defaultTitle";

  String get pageTitle => _pageTitle;

  set pageTitle(String value) {
    _pageTitle = value;
    notifyListeners();
  }
}
