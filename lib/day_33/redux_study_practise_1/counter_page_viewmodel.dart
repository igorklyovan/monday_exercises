import 'package:exercises/day_33/redux_study_practise_1/store/app/app_state.dart';
import 'package:exercises/day_33/redux_study_practise_1/store/pages/counter_page_state/counter_page_selectors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class CounterPageViewModel {
  final double counter;
  final Color appBarColor;
  final bool isAppColorSwitch;
  final void Function() incrementCounter;
  final void Function() decrementCounter;
  final void Function() cleanCounter;

//  final void Function(Color) changeAppBarColor;
  final void Function() changeAppBarColor;
  final void Function(double) multiplyCounter;

  CounterPageViewModel({
    @required this.counter,
    @required this.appBarColor,
    @required this.incrementCounter,
    @required this.decrementCounter,
    @required this.cleanCounter,
    @required this.changeAppBarColor,
    @required this.isAppColorSwitch,
    @required this.multiplyCounter,
  });

  static CounterPageViewModel fromStore(Store<AppState> store) {
    return CounterPageViewModel(
      counter: CounterPageSelectors.getCounterValue(store),
      appBarColor: CounterPageSelectors.getAppBarColorValue(store),
      isAppColorSwitch: CounterPageSelectors.getIsAppBarColorSwitch(store),
      incrementCounter: CounterPageSelectors.getIncrementCounterFunction(store),
      decrementCounter: CounterPageSelectors.getDecrementCounterFunction(store),
      cleanCounter: CounterPageSelectors.getCleanCounterFunction(store),
      changeAppBarColor: CounterPageSelectors.getChangeAppBarColorFunction(store),
      multiplyCounter: CounterPageSelectors.getMultiplyCounterFunction(store),
    );
  }
}
