import 'package:flutter/material.dart';
import '../widgets/button.dart';
import '../pages/counter_page.dart';

class ImagesPage extends StatefulWidget {
  static final String routeName = '/images';

  @override
  _ImagesPageState createState() => _ImagesPageState();
}

class _ImagesPageState extends State<ImagesPage> {
  @override
  Widget build(BuildContext context) {
    const List<String> images = const <String>[
      'https://upload.wikimedia.org/wikipedia/commons/5/59/500_x_300_Ramosmania_rodriguesii_%28Rubiaceae%29.jpg',
      "https://lh3.googleusercontent.com/MtCnY_7Vhj31WTS5Vs9fKabjWr2FzJXgYvoBV-iT_yTnUeSevEb6CbmYg6o4EbMS_r0pN07ab8XybcQyM5e4OyFJ5Lc2Xypjb7qDvQ=w760-h380",
      "https://cdn.eso.org/images/thumb300y/eso1907a.jpg"
    ];

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.2,
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  MyButton(Icons.arrow_back, CounterPage.routeName),
                  MyButton(Icons.arrow_forward, ImagesPage.routeName),
                ],
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.8,
            child: ListView.builder(
              itemBuilder: (ctx, index) => Card(
                  child: Image.network(
                images[index],
                fit: BoxFit.cover,
              )),
              itemCount: images.length,
            ),
          )
        ],
      ),
    );
  }
}
