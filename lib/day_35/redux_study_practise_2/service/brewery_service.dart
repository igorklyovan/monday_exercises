import 'package:exercises/day_35/redux_study_practise_2//res/consts.dart';
import 'package:http/http.dart' as http;

class BreweryService {
  Future<dynamic> getBreweries() async {
    print('SERVICE');
    var response = await http.get('${Constants.kkBreweryUrl}');
    return response.body;
  }
}
