import 'package:exercises/day_20/api_practise_1/data/models/user.dart';
import 'package:exercises/day_20/api_practise_1/service/user_service.dart';
import 'dart:convert';

class UserRepository {
  Future<List<User>> getUsers() async {
    List<User> users = [];

    var response = await UserService().getUsers();
    final jsonData = json.decode(response)['results'];



    for (int i = 0; i < jsonData.length; i++) {
      users.add(User.fromJson(jsonData[i]));
    }

    return users;
  }
}
