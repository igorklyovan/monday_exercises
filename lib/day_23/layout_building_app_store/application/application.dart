import 'package:exercises/day_23/layout_building_app_store/data/routes.dart';
import 'package:exercises/day_23/layout_building_app_store/helpers/route_helper.dart';
import 'package:exercises/day_23/layout_building_app_store/layouts/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.black,
          systemNavigationBarDividerColor: Colors.black),
      child: MaterialApp(
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: CleanBehavior(),
            child: child,
          );
        },
        debugShowCheckedModeBanner: false,
        initialRoute: Routes.kTodayPage,
        onGenerateRoute: RouteHelper.onGenerateRoute,
      ),
    );
  }
}
