import 'package:flutter/material.dart';
import 'dart:math';

class ListViewProvider with ChangeNotifier {
  List<String> _list = [];

  List<String> get list => _list;

  void generateLength() {
    int listLength = Random().nextInt(100);
    _list = List.generate(listLength, (index) {
      return '${index + 1}';
    });
    notifyListeners();
  }

  void changeSortType() {
    _list = _list.reversed.toList();
    notifyListeners();
  }

  void removeRandom() {
    if (_list.length > 0) {
      int randomIndex = Random().nextInt(_list.length);
      _list.removeAt(randomIndex);
      notifyListeners();
    }
    notifyListeners();
  }
}
