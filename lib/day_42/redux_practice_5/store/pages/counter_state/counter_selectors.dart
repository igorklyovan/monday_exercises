import 'package:exercises/day_42/redux_practice_5/store/pages/counter_state/counter_actions.dart';
import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:redux/redux.dart';

class CounterSelectors {
  static double getCounter(Store<AppState> store) {
    double counter = store.state.counterState.counter;
    return counter;
  }

  static void Function() incrementCounter(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() decrementCounter(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }
}
