// main.dart
import 'package:flutter/material.dart';

class MyDraggable extends StatefulWidget {
  @override
  _MyDraggableState createState() => _MyDraggableState();
}

class _MyDraggableState extends State<MyDraggable> {
  String _targetString;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Draggable<String>(
                data: "Draggable",
                child: Container(
                  width: 300,
                  height: 200,
                  alignment: Alignment.center,
                  color: Colors.purple,
                  child: Text(
                    'Draggable',
                    style: TextStyle(fontSize: 25),
                  ),

                ),
                feedback: Opacity(
                  opacity: 0.4,
                  child: Container(
                    color: Colors.purple,
                    width: 300,
                    height: 200,
                    alignment: Alignment.center,

                  ),
                ),
              ),
              SizedBox(height: 50),
              DragTarget<String>(
                onAccept: (value) {
                  print('lol');
                  setState(() {
                    _targetString = value;
                    print(value);
                  });
                },
                builder: (_, candidateData, rejectedData) {
                  return Container(
                    width: 300,
                    height: 200,
                    color: Colors.amber,
                    alignment: Alignment.center,
                    child: _targetString != null ? Text(_targetString) : Container(),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
