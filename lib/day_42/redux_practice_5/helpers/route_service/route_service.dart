import 'package:exercises/day_42/redux_practice_5/pages/another_page.dart';
import 'package:exercises/day_42/redux_practice_5/pages/home_page.dart';
import 'package:exercises/day_42/redux_practice_5/pages/third_page.dart';
import 'package:exercises/day_42/redux_practice_5/res/routes.dart';
import 'package:exercises/day_42/redux_practice_5/store/pages/unknown_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class RouteService {
  RouteService._privateConstructor();

  static final RouteService _instance = RouteService._privateConstructor();

  static RouteService get instance => _instance;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    print(settings.name);
    switch (settings.name) {
      case Routes.kHomePage:
        return PageTransition(
          child: HomePage(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      case Routes.kAnotherPage:
        return PageTransition(
          child: AnotherPage(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      case Routes.kThirdPage:
        return PageTransition(
          child: ThirdPage(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      default:
        return _defaultRoute(
          settings: settings,
          page: UnknownPage(),
        );
    }
  }

  static MaterialPageRoute _defaultRoute({RouteSettings settings, Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
