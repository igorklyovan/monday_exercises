import 'package:firebase_auth/firebase_auth.dart';

import '';

class AuthService{
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future signInAnon() async {
    try{
      AuthResult result = await _auth.signInAnonymously();
//      FireBaseUser user = result.user;
//      return user;
    }catch(error){
      print(error.toString());
      return null;
    }

  }


}