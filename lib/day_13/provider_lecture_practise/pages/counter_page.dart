import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/counter_provider.dart';
import '../main_drawer.dart';

class CounterPage extends StatelessWidget {
  static const String routeName = '/counter-page';

  @override
  Widget build(BuildContext context) {
    final counterData = Provider.of<CounterProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Text(
          '${counterData.counter}',
          style: TextStyle(fontSize: 50),
        ),
      ),
    );
  }
}
