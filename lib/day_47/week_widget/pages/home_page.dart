import 'package:exercises/day_47/week_widget/pages/widgets/inherited_beard.dart';
import 'package:exercises/day_47/week_widget/pages/widgets/inherited_model_beard.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Flexible(
            child: FractionallySizedBox(
              widthFactor: 0.9,
              heightFactor: 0.2,
              child: Image.asset('assets/images/snowman.png'),
            ),
          ),
          InheritedBeard(
            beardType: 'normal',
            child: Container(
              height: 200,
              width: 200,
              child: Builder(
                builder: (ctx) {
                  return Row(
                    children: [
                      Text('Inherited type ${InheritedBeard.of(context).beardType}'),
//                      Text('Inherited type ${InheritedModel.inheritFrom<InheritedModelBeard>(context, aspect: 'first')}'),
                    ],
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
