import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/model/brewer.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/another_page_state/another_page_actions.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:redux/redux.dart';

class AnotherPageSelectors {
  static List<Brewer> getBrewers(Store<AppState> store) {
    return store.state.anotherPageState.brewers;
  }

  static void Function() getBrewersFunction(Store<AppState> store) {
    return () => store.dispatch(GetBrewerAction());
  }
}
