import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  Function _presentDatePicker;

  MyButton(this._presentDatePicker);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      height: 40,
      child: RaisedButton(
        color: Colors.green,
        textColor: Colors.white,
        child: Text(
          'Choose Date',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        onPressed: _presentDatePicker,
      ),
    );
  }
}
