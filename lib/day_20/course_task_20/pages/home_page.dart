import 'package:flutter/material.dart';
import 'package:location/location.dart';
import '../helpers/location_helper.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String imagePreview;

  Future<void> _getCurrentUserLocation() async {
    final locData = await Location().getLocation();
    print(locData.latitude);
    final staticMapImageUrl =
        LocationHelper.generateLocationPreviewImage(latitude: locData.latitude, longitude: locData.longitude);
    setState(() {
      imagePreview = staticMapImageUrl;
    });
  }

  @override
  void initState() {
    _getCurrentUserLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: imagePreview == null
          ? CircularProgressIndicator()
          : InteractiveViewer(
              child: Image.network(
              imagePreview,
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.cover,
            )),
    );
  }
}
