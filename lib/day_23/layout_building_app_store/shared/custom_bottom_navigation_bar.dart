import 'package:exercises/day_23/layout_building_app_store/data/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Container(
      height: deviceSize.height * 0.1,
      color: Colors.white10,
      padding: EdgeInsets.only(top: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Icon(
                CupertinoIcons.search,
                color: AppColors.kGrey,
              ),
              Text("Search")
            ],
          ),
          Column(
            children: [
              Icon(
                CupertinoIcons.game_controller_solid,
                color: AppColors.kGrey,
              ),
              Text('Arcade')
            ],
          ),
          Column(
            children: [
              Icon(
                CupertinoIcons.square_stack_3d_up_fill,
                color: AppColors.kGrey,
              ),
              Text('Apps')
            ],
          ),
          Column(
            children: [
              Icon(
                CupertinoIcons.rocket_fill,
                color: AppColors.kGrey,
              ),
              Text('Games')
            ],
          ),
          Column(
            children: [
              Icon(CupertinoIcons.doc_append, color: Colors.blue),
              Text(
                'Today',
                style: TextStyle(color: Colors.blue),
              )
            ],
          ),
        ],
      ),
    );
  }
}
