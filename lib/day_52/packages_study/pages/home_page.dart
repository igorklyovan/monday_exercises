import 'dart:async';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:get_version/get_version.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final deviceInfo = DeviceInfoPlugin();

//  String deviceInfoSC = '';
//  String androidInfoSC = '';

//  StreamController sc = StreamController();

//  @override
//  void initState() {
//    super.initState();
//    sc.stream.listen((event) async* {
//
//      if (await event.runtimeType == AndroidDeviceInfo) {
//        deviceInfoSC = (await event as AndroidDeviceInfo).model;
//        print(await event);
//      }
//      if (event.runtimeType == String) {
//        print(await event);
//      }
//    });
//  }
//
//  @override
//  void dispose() {
//    super.dispose();
//    sc.close();
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //stream
//          Text('$deviceInfoSC'),
//          Text(androidInfoSC),
//          RaisedButton(onPressed: () {
//            sc.add(deviceInfo.androidInfo);
//            sc.add(GetVersion.projectVersion);
//          },child: Icon(Icons.view_stream),),
          // end stream
          //future
          Center(
            child: RaisedButton(
              child: Icon(Icons.info),
              onPressed: () => showDialog(
                context: context,
                child: AboutDialog(
                  children: [
                    FutureBuilder(
                      future: GetVersion.projectVersion,
                      builder: (ctx, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          return Center(child: Text('Project version:${snapshot.data}'));
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                    FutureBuilder(
                      future: GetVersion.platformVersion,
                      builder: (ctx, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          return Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('Android version:${snapshot.data}'),
                                Icon(
                                  Icons.android,
                                  color: Colors.green,
                                ),
                              ],
                            ),
                          );
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                    FutureBuilder(
                      future: deviceInfo.androidInfo,
                      builder: (ctx, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          return Center(child: Text('Phone model:${(snapshot.data as AndroidDeviceInfo).model}'));
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          //end future
        ],
      ),
    );
  }
}
