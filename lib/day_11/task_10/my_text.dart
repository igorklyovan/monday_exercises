import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/titles.dart';

class MyText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = Provider.of<Titles>(context).pageTitle;
    return Text(title);
  }
}
