import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:exercises/day_9/buttons_and_routes/main_drawer.dart';


class ButtonSizeExampleScreen extends StatelessWidget {
  static const routeName = '/button-size-example';

  final Map<String, Object> data;

  ButtonSizeExampleScreen(this.data);

  @override
  Widget build(BuildContext context) {
    final appBarTitle = data['title'];
    ScreenUtil.init(context, allowFontScaling: false);

    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                height: 0.2.sh,
                width: 0.4.sw,
                child: RaisedButton(
                  color: Colors.blue,
                  child: Text("Press Me!"),
                  onPressed: () {},
                )),
            SizedBox(
              width: 20,
            ),
            SizedBox(
                height: 0.2.sh,
                width: 0.4.sw,
                child: RaisedButton(
                  color: Colors.blue,
                  child: Text("Press Me!"),
                  onPressed: () {},
                )),
          ],
        ),
      ),
    );
  }
}
