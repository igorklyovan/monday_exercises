import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../pages/home_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home:HomePage(),
//        routes: {CounterPage.routeName: (ctx) => CounterPage(), ButtonsPage.routeName: (ctx) => ButtonsPage()},
    );
  }
}
