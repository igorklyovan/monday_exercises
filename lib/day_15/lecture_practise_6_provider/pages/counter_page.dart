import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/counter_provider.dart';
import '../main_drawer.dart';

class CounterPage extends StatelessWidget {
  static const String routeName = '/counter-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter'),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Selector<CounterProvider, int>(
              builder: (ctx, firstCounter, _) {
                return Text(
                  '$firstCounter',
                  style: TextStyle(fontSize: 50),
                );
              },
              selector: (context, countPro) => countPro.firstCounter,
            ),
            SizedBox(
              width: 20,
            ),
            Selector<CounterProvider, int>(
              builder: (ctx, secondCounter, _) {
                return Text(
                  '$secondCounter',
                  style: TextStyle(fontSize: 50),
                );
              },
              selector: (context, countPro) => countPro.secondCounter,
            ),
          ],
        ),
      ),
    );
  }
}
