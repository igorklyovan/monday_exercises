import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/list_view_provider.dart';

import '../pages/home_page.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ChangeNotifierProvider.value(value: ListViewProvider(), child: HomePage()),
    );
  }
}
