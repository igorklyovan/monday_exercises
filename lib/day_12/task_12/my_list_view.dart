import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/counters.dart';
import 'package:auto_size_text/auto_size_text.dart';


class MyListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int counter = Provider.of<Counters>(context).counter;
    return Container(
      height: 500,
      width: 500,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: counter > 0 ? counter : 1,
        itemBuilder: (ctx, index) {
          return SizedBox(
            width: 200,
            height: 400,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Stack(
                children: [
                  Container(
//                width: 400,
//                height: 200,
                    color: index % 2 == 0 ? Colors.yellow : Colors.purple,
                  ),
                  Center(child: Text('$index')),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
