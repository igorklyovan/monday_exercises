import 'package:exercises/day_21/painter_gesture_detector/my_painter.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Offset _localPos = Offset(0, 0);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
            height: deviceSize.height * 0.1,
            child: Text(
              '${_localPos.dx > deviceSize.width * 0.885 ? '${(deviceSize.width * 0.885 / 50).round()}' : _localPos.dx.round() / 50}',
              style: TextStyle(fontSize: 25),
            )),
        GestureDetector(
          onHorizontalDragUpdate: (details) {
            setState(() {
              _localPos = details.localPosition;
            });
          },
          child: CustomPaint(
            painter: MyPainter(_localPos),
            size: Size(deviceSize.width, deviceSize.height * 0.2),
          ),
        ),
      ],
    ));
  }
}
