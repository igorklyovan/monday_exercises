import 'dart:math';

import 'package:exercises/day_23/layout_building_app_store/data/dummy_data.dart';
import 'package:exercises/day_46/week_widget/models/my_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
  List<int> _items = [];
  int counter = 0;
  int stackCounter = 0;
  AnimationController iconAnimation;
  List<MyModel> taskList = [
    MyModel(
      id: '1',
      title: 'Test app 1',
    ),
    MyModel(
      id: '2',
      title: 'Test app 2',
    ),
    MyModel(
      id: '3',
      title: 'Test app 3',
    )
  ];
  final ValueNotifier<int> _counter = ValueNotifier<int>(0);
  final Widget goodJob = const Text('ValueListenable!');

  @override
  void initState() {
    super.initState();
    iconAnimation = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
      reverseDuration: Duration(milliseconds: 400),
    );
    iconAnimation.addListener(update);
  }

  @override
  void dispose() {
    super.dispose();
    iconAnimation.dispose();
    iconAnimation.removeListener(update);
  }

  void update() => setState(() {});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 300.0,
                child: ReorderableListView(
                  children: taskList.map((e) => anotherColoredCard(e)).toList(),
                  onReorder: _onReorder,
                ),
              ),
//            RaisedButton(
//              onPressed: () {
//                setState(() {
//                  listKey.currentState.removeItem(0, (_, animation) => coloredCard(0, animation),
//                      duration: const Duration(milliseconds: 500));
//
//                  if (_items.length > 0) _items.removeAt(0);
//                });
//              },
//              child: Icon(Icons.remove),
//            ),
              SizedBox(
                height: 300.0,
                child: AnimatedList(
                  key: listKey,
                  initialItemCount: _items.length,
                  itemBuilder: (ctx, index, animation) {
                    return coloredCard(index, animation);
                  },
                ),
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    listKey.currentState.insertItem(0, duration: const Duration(milliseconds: 500));
                    _items = []
                      ..add(counter++)
                      ..addAll(_items);
                  });
                  iconAnimation.forward();
                  _counter.value += 1;
                },
                child: AnimatedIcon(
                  icon: AnimatedIcons.add_event,
                  progress: iconAnimation,
                ),
              ),
              ValueListenableBuilder(
                builder: (ctx, value, child) {
                  return Center(
                      child: Row(children: [
                    Text(
                      '$value',
                      style: TextStyle(fontSize: 25),
                    ),
                    child
                  ]));
                },
                valueListenable: _counter,
                child: goodJob,
              ),
              SizedBox(
                height: 200,
                child: IndexedStack(
                  index: stackCounter,
                  children: [
                    Image.asset(
                      'assets/images/snowman.png',
                      height: 300.0,
                      width: 200.0,
                    ),
                    FlutterLogo(
                      size: 100.0,
                    )
                  ],
                ),
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    stackCounter = stackCounter+1;
                  });
                },
                child: Icon(
                  Icons.arrow_forward_ios,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget coloredCard(int index, animation) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(-1, 0),
        end: Offset(0, 0),
      ).animate(animation),
      child: SizedBox(
        height: 100.0,
        child: Card(
          color: Colors.purple,
          child: Center(
            child: Text('Animated item $index'),
          ),
        ),
      ),
    );
  }

  Widget anotherColoredCard(MyModel model) {
    return SizedBox(
      key: Key(model.id),
      height: 100.0,
      child: Card(
        color: Colors.purple,
        child: Center(
          child: Text('${model.title}  ${model.id}'),
        ),
      ),
    );
  }

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      if (newIndex > oldIndex) {
        newIndex -= 1;
      }
      final MyModel item = taskList.removeAt(oldIndex);
      taskList.insert(newIndex, item);
    });
  }
}
