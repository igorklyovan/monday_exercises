import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './orientation_helper.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  OrientationHelper.setPreferredOrientations(DeviceOrientation.values);
  OrientationHelper.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  runApp(MaterialApp(home: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  DeviceOrientation _deviceOrientation;

  StreamSubscription<DeviceOrientation> subscription;

  @override
  void initState() {
    super.initState();

    subscription = OrientationHelper.onOrientationChange.listen((value) {
      if (!mounted) return;

      setState(() {
        _deviceOrientation = value;
        print('$_deviceOrientation');
      });

      OrientationHelper.forceOrientation(value);
    });
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    subscription?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Orientation Example'),
      ),
      body: Center(
          child: Container(
        width: mediaQuery.size.width * 0.75,
        height: mediaQuery.size.height * 0.75,
        color: Colors.red,
      )),
    );
  }
}
