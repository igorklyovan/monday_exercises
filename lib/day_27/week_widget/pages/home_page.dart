import 'dart:math';

import 'package:exercises/day_27/week_widget/pages/tween_example.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  List colors = [Colors.red, Colors.green, Colors.yellow, Colors.black];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('SliverAppBar'),
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              background: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => TweenExample(),
                    ),
                  );
                },
                child: Image.asset('assets/images/snowman.png'),
              ),
            ),
          ),
          Builder(builder: (BuildContext ctx) {
            return SliverList(
              delegate: SliverChildBuilderDelegate((ctx, index) {
                return Dismissible(
                  key: ValueKey('SliverList'),
                  onDismissed: (_) {
                    Scaffold.of(ctx).showSnackBar(
                      SnackBar(
                        content: Text('Sliver ${index + 1} was dismissed'),
                      ),
                    );
                    print('hah');
                  },
                  child: Container(
                    color: colors[Random().nextInt(4)],
                    height: 60,
                    width: double.infinity,
                  ),
                );
              }, childCount: 15),
            );
          }),
          SliverGrid.extent(
            maxCrossAxisExtent: 80,
            children: [
              for (int i = 0; i < 15; i++)
                Container(
                  color: colors[Random().nextInt(4)],
                  height: 40,
                  width: 40,
                )
            ],
          )
        ],
      ),
    );
  }
}
