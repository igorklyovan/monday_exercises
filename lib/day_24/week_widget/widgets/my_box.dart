import 'package:flutter/material.dart';

class MyBox extends StatelessWidget {
  Color _color;

  MyBox(this._color);

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Container(
      height: deviceSize.height * 0.1,
      width: deviceSize.width * 0.1,
      color: _color,
    );
  }
}
