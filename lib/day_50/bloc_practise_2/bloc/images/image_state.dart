import 'package:exercises/day_50/bloc_practise_2/model/splash_photo.dart';
import 'package:flutter/cupertino.dart';

abstract class ImageState {}

class ImageFetchedState extends ImageState {
  final List<SplashPhoto> photos;

  ImageFetchedState({@required this.photos});
}

class ImageEmptyState extends ImageState {}
