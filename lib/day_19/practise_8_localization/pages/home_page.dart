import 'package:exercises/day_13/provider_lecture_practise/widgets/my_button.dart';
import 'package:exercises/day_19/practise_8_localization/dictionary/dictionary_classes/general_language.dart';
import 'package:exercises/day_19/practise_8_localization/dictionary/flutter_delegate.dart';
import 'package:exercises/day_19/practise_8_localization/dictionary/flutter_dictionary.dart';
import 'package:exercises/day_19/practise_8_localization/dictionary/models/supported_locales.dart';
import 'package:exercises/day_19/practise_8_localization/dropdown.dart';
import 'package:exercises/day_19/practise_8_localization/providers/language_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:exercises/day_19/practise_8_localization/dictionary/data/en.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    GeneralLanguage language = FlutterDictionary.instance.language?.generalLanguage ?? en.generalLanguage;
    return Scaffold(
      appBar: AppBar(
        title: Text('${language.appTitle}'),
      ),
      body: Consumer<LanguageProvider>(
        builder: (ctx, provider, ch) {
          return Directionality(
            textDirection: TextDirection.ltr,
            child: Center(child: MainDropDown()),
          );
        },
      ),
    );
  }
}
