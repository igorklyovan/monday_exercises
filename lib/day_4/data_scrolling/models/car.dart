import 'package:flutter/cupertino.dart';

class Car {
  final String id;
  final String carName;
  final String carColor;
  final DateTime manufactureDate;
  final double carCost;


  Car(
      {@required this.id,
      @required this.carName,
      @required this.carColor,
      @required this.manufactureDate,
      @required this.carCost});
}
