import 'package:flutter/material.dart';

class TweenExample extends StatefulWidget {
  @override
  _TweenExampleState createState() => _TweenExampleState();
}

class _TweenExampleState extends State<TweenExample> {
  Color _color = Colors.orange;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TweenAnimationBuilder<Color>(
          tween: ColorTween(begin: Colors.white, end: _color),
          duration: const Duration(seconds: 3),
          builder: (ctx, color, ch) {
            return InkWell(
              child: Image.asset(
                'assets/images/snowman.png',
                color: color,
              ),
              onTap: () {
                setState(() {
                  _color = Colors.green;
                });
              },
            );
          },
        ),
      ),
    );
  }
}
