import 'package:flutter/material.dart';

import '../main_drawer.dart';

class YellowPage extends StatelessWidget {
  static const String routeName = '/yellow';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: MainDrawer(),
      body: Container(
        color: Colors.yellow,
      ),
    );
  }
}
