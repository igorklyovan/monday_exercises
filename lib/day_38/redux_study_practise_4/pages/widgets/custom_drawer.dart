import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/pages/home_page_vm.dart';
import 'package:exercises/day_38/redux_study_practise_4/res/routes.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageViewModel>(
      converter: HomePageViewModel.fromStore,
      builder: (BuildContext context, HomePageViewModel viewModel) {
        return Drawer(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlatButton(
                  onPressed: () {
//                      viewModel.changeHomerColor(Colors.green);
                    viewModel.push(Routes.kHomePage);


                  },
                  color: Colors.green,
                  child: Text("Home page"),
                ),
//            FlatButton(
//              onPressed: () {
////                      viewModel.getBrewers();
//              },
//              child: Text("Get Brewers"),
//              color: Colors.red,
//            ),
                FlatButton(
                  onPressed: () {
//                      viewModel.changeHomerColor(Colors.orangeAccent);
                    viewModel.push(Routes.kAnotherPage);
                  },
                  child: Text("Another page"),
                  color: Colors.orangeAccent,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
