import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  Offset localPosition;

  MyPainter(this.localPosition);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.grey;
    paint.strokeWidth = 10;
    if (localPosition == Offset(0, 0)) localPosition = Offset(size.width * 0.125, size.height / 2);
    localPosition = Offset(localPosition.dx, localPosition.dy);
    canvas.drawLine(Offset(size.width * 0.125, size.height / 2), Offset(size.width * 0.885, size.height / 2), paint);

    if (localPosition.dx > size.width * 0.885) {
      localPosition = Offset(size.width * 0.885, localPosition.dy);
    } else if (localPosition.dx < size.width * 0.125) {
      localPosition = Offset(size.width * 0.125, localPosition.dy);
    }

    paint.color = Colors.red;
    canvas.drawLine(Offset(size.width * 0.125, size.height / 2), Offset(localPosition.dx, size.height / 2), paint);
    canvas.drawCircle(Offset(localPosition.dx, size.height / 2), 10, paint);

    paint.color = Colors.black12;
    canvas.drawCircle(Offset(localPosition.dx, size.height / 2), 15, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
