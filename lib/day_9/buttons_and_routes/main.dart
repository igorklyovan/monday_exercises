import 'package:exercises/day_9/buttons_and_routes/main_scaffold.dart';
import 'package:flutter/material.dart';

import '../advanced_routing/helpers/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainScaffold(),
      onGenerateRoute: MyRouter.onGenerateRoute,
      initialRoute: '/',
    );
  }
}
