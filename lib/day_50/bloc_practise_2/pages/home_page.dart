import 'package:exercises/day_50/bloc_practise_2/bloc/images/image_bloc.dart';
import 'package:exercises/day_50/bloc_practise_2/bloc/images/image_state.dart';
import 'package:exercises/day_50/bloc_practise_2/bloc/images/image_event.dart';
import 'package:exercises/day_50/bloc_practise_2/pages/details_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ImageBloc, ImageState>(
      builder: (ctx, state) {
        if (state is ImageEmptyState) {
          return Scaffold(
            backgroundColor: Colors.grey,
            body: Center(
              child: RaisedButton(
                child: Icon(Icons.image),
                onPressed: () {
                  BlocProvider.of<ImageBloc>(context).add(FetchImageEvent());
                },
              ),
            ),
          );
        } else if (state is ImageFetchedState) {
          return Scaffold(
            body: GridView.builder(
                itemCount: state.photos.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 2,
                  crossAxisCount: 2,
                ),
                itemBuilder: (BuildContext ctx, int index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) => DetailsPage(splashPhoto: state.photos[index]),
                        ),
                      );
                    },
                    child: Image.network(
                      state.photos[index].url,
                      height: 100.0,
                      width: 100.0,
                    ),
                  );
                }),
            appBar: AppBar(),
          );
        }
        return Scaffold(
          body: Center(
            child: Text('Nothing to show!'),
          ),
        );
      },
    );
  }
}
