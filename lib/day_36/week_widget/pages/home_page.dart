import 'package:exercises/day_36/week_widget/widgets/my_custom_clipper.dart';
import 'package:exercises/day_36/week_widget/widgets/my_custom_clipper_triangle.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isAbsorb = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 50.0),
            Wrap(
              alignment: WrapAlignment.end,
              spacing: 8.0,
              runSpacing: 20.0,
              children: [
                for (int i = 0; i < 10; i++) FlutterLogo(size: 40.0),
              ],
            ),
            const SizedBox(height: 50.0),
            ClipPath(
              child: Semantics(
                label: 'Just blue box',
                enabled: true,
                child: Container(
                  width: 150.0,
                  height: 200.0,
                  color: Colors.red,
                ),
              ),
              clipper: MyCustomClipper(),
            ),
            const SizedBox(height: 50.0),
            ClipPath(
              child: Container(
                width: 150.0,
                height: 200.0,
                color: Colors.green,
              ),
              clipper: MyCustomClipperTriangle(),
            ),
            AbsorbPointer(
              absorbing: _isAbsorb,
              child: RaisedButton(
                onPressed: () {
                  print('ABSORb');
                  setState(() {
                    _isAbsorb = true;
                  });
                },
                color: Colors.purple,
                child: Text('RaisedButton with AbsorbPointer'),
              ),
            ),
            const SizedBox(height: 50.0),
            FlatButton(
              onPressed: () {
                showAboutDialog(context: context, applicationIcon: Icon(Icons.title), applicationVersion: '2.2.2');
              },
              child: Text(
                'More Info',
              ),
            ),
            const SizedBox(height: 50.0),
          ],
        ),
      )),
    );
  }
}
