import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/counters.dart';
import './my_list_view.dart';
import './my_button.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyHomePage());
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: Counters(),
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            MyListView(),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [MyButton(Icons.add, ButtonAction.plus), MyButton(Icons.remove, ButtonAction.minus)],
              ),
            )
          ],
        ),
      ),
    );
  }
}

//List<Widget> _containers(int count) {
//  List<Widget> list = [];
//
//  for (int i = 0; i < count; i++) {
//    list.add(Container(
//      width: 200,
//      height: double.infinity,
//      color: Colors.black,
//    ));
//  }
//  return list;
//}
