import 'package:flutter/material.dart';

class Counters with ChangeNotifier {
  int _counter = 2;

  int get counter => _counter;

  set counter(int value) {
    if (_counter > 0) {
      _counter = value;
    } else {
      _counter = 1;
    }
    notifyListeners();
  }
}
