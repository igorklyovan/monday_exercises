import 'package:exercises/day_35/redux_study_practise_2/counter_page_viewmodel.dart';
import 'package:exercises/day_35/redux_study_practise_2/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
//      onInit: (Store<AppState> store) => CounterPageSelectors.getCounterValue(store),
//      onInitialBuild: (CounterPageViewModel vm) => {print('Initial Bild Hello')},
      builder: (BuildContext context, CounterPageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(),
          backgroundColor: viewModel.homeColor,
          drawer: Drawer(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlatButton(
                    onPressed: () {
//                      viewModel.changeHomerColor(Colors.green);
                    },
                    color: Colors.green,
                    child: Text("GSd"),
                  ),
                  FlatButton(
                    onPressed: () {
                      viewModel.getBrewers();
                    },
                    child: Text("Get Brewers"),
                    color: Colors.red,
                  ),
                  FlatButton(
                    onPressed: () {
//                      viewModel.changeHomerColor(Colors.orangeAccent);
                    },
                    child: Text("Orange"),
                    color: Colors.orangeAccent,
                  ),
                ],
              ),
            ),
          ),
          body: Center(child: ListView.builder(itemBuilder: (ctx, index) {
            return Card(
              elevation: 5,
              child: Column(
                children: [
                  Text(
                    viewModel.brewers[index].name,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                    child: Text(
                      viewModel.brewers[index].city,
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            );
          },itemCount: viewModel.brewers.length,),),
        );
      },
    );
  }
}
