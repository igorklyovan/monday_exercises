import 'package:exercises/day_42/redux_practice_5/pages/home_page_vm.dart';
import 'package:exercises/day_42/redux_practice_5/pages/widgets/custom_drawer_vm.dart';
import 'package:exercises/day_42/redux_practice_5/res/routes.dart';
import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CustomDrawerViewModel>(
      converter: CustomDrawerViewModel.fromStore,
      builder: (BuildContext context, CustomDrawerViewModel viewModel) {
        return Drawer(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlatButton(
                  onPressed: () {
                    viewModel.incrementCounter();
                  },
                  color: Colors.green,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    viewModel.decrementCounter();
                  },
                  child: Icon(
                    Icons.remove,
                    color: Colors.white,
                  ),
                  color: Colors.deepOrange,
                ),
                Row(
                  children: [
                    const SizedBox(width: 5.0),
                    FlatButton(
                      onPressed: () {
                        viewModel.push(Routes.kHomePage);
                      },
                      child: Text(
                        "1",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.grey,
                    ),
                    const SizedBox(width: 5.0),
                    FlatButton(
                      onPressed: () {
                        viewModel.push(Routes.kAnotherPage);
                      },
                      child: Text(
                        "2",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.grey,
                    ),
                    const SizedBox(width: 5.0),
                    FlatButton(
                      onPressed: () {
                        viewModel.push(Routes.kThirdPage);
                      },
                      child: Text(
                        "3",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.grey,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
