import 'dart:convert';

import 'package:exercises/day_51/packages_study/model/splash_photo.dart';
import 'package:exercises/day_51/packages_study/service/photo_service.dart';

class PhotoRepository {
  Future<List<SplashPhoto>> getPhotos() async {
    List<SplashPhoto> photos = [];

    var response = await PhotoService().getPhotos();
    List jsonData = json.decode(response);
    for (int i = 0; i < jsonData.length; i++) {
      photos.add(SplashPhoto.fromJson(jsonData[i]));
    }
    return photos;
  }
}
