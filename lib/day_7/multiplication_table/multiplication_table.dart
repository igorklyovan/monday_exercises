import 'package:flutter/material.dart';
import './numbers_list.dart';

class MultiplicationTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Container(
      margin: EdgeInsets.all(8),
      height: (mediaQuery.size.height - mediaQuery.padding.top) * 0.9,
//      child: GridView(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(),),
      child: GridView.count(
        crossAxisCount: 11,
        children: numbers(),
      ),
    );
  }


}
