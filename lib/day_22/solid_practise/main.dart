import 'package:exercises/day_22/solid_practise/area_calculator.dart';
import 'package:exercises/day_22/solid_practise/shape.dart';
import 'package:exercises/day_22/solid_practise/square.dart';
import 'package:exercises/day_22/solid_practise/circle.dart';

void main() {
  final List<Shape> shapes = [
     Circle(),
     Square(),
  ];

  AreaCalculator areaCalculator = AreaCalculator(shapes);
  areaCalculator.output();
  areaCalculator.sum();
}
