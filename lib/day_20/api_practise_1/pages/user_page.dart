import 'package:exercises/day_20/api_practise_1/data/models/user.dart';
import 'package:exercises/day_20/api_practise_1/repository/user_repository.dart';
import 'package:flutter/material.dart';

class UserPage extends StatefulWidget {
  int index = 0;

  @override
  _UserPageState createState() => _UserPageState();
}
List<User> _users;

class _UserPageState extends State<UserPage> {
  Future<void> _getUsers() async {
    List<User> users = await UserRepository().getUsers();
    _users = users;
  }

  @override
  void initState() {
    _getUsers().then((_) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: _users.length == 0
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Center(
              child: Container(
                height: deviceSize.height * 0.5,
                width: deviceSize.width * 0.9,
                child: Card(
                  elevation: 5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          child: Image.network(
                            _users[widget.index].imgUrl,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text(
                        '${_users[widget.index].firstName}   ${_users[widget.index].lastName}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('gender: ${_users[widget.index].gender == Gender.male ? "Male" : "Female"}'),
                      Text('city: ${_users[widget.index].city}'),
                      Text(
                        'email: ${_users[widget.index].email}',
                        maxLines: 2,
                      ),
                      Text('phone: ${_users[widget.index].phone}'),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          RaisedButton(
                              child: Icon(Icons.arrow_back),
                              onPressed: () {
                                if (widget.index > 0) {
                                  setState(() {
                                    widget.index--;
                                  });
                                } else {
                                  return null;
                                }
                              }),
                          RaisedButton(
                              child: Icon(Icons.arrow_forward),
                              onPressed: () {
                                setState(() {
                                  if (widget.index < _users.length - 1) {
                                    widget.index++;
                                  }
                                });
                              }),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
