import 'dart:async';

import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/helpers/route_service/route_service.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/pages/home_page.dart';
import 'package:redux_epics/redux_epics.dart';


//void meow() {
//  Stream stream = Stream.value(5);
//  StreamSubscription streamSubscription = stream.listen((event) { print('event $event');});
//  Stream.value(10).listen((event) { print('neow $event');});
//
//
//  //stream.asyncMap((event) async* { yield 10; yield 15; yield 20;}).listen((event) { });
//}

void main() {
//  meow();
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      EpicMiddleware(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(
    MyApp(
      store: store,
    ),
  );
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({@required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Redux practise',
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: RouteService.onGenerateRoute,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(title: 'Redux practise Home Page'),
      ),
    );
  }
}
