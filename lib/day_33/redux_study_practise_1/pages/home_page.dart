import 'package:exercises/day_33/redux_study_practise_1/counter_page_viewmodel.dart';
import 'package:exercises/day_33/redux_study_practise_1/store/app/app_state.dart';
import 'package:exercises/day_33/redux_study_practise_1/store/pages/counter_page_state/counter_page_selectors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterPageViewModel>(
      converter: CounterPageViewModel.fromStore,
      onInit: (Store<AppState> store) => CounterPageSelectors.getCounterValue(store),
      onInitialBuild: (CounterPageViewModel vm) => {print('Initial Bild Hello')},
      builder: (BuildContext context, CounterPageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: viewModel.isAppColorSwitch ? Colors.orangeAccent : Colors.grey[900],
            title: Text(title),
          ),
          drawer: Drawer(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(height: 100),
                  Switch(
                      value: viewModel.isAppColorSwitch,
                      onChanged: (val) {
                        viewModel.changeAppBarColor();
                      }),
                  Spacer(),
                  InkWell(
                      onTap: viewModel.cleanCounter,
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        child: Text(
                          'Clean Counter',
                          style: TextStyle(fontSize: 32.0),
                        ),
                      ))
                ],
              ),
            ),
          ),
          body: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.remove,
                    color: Colors.red,
                  ),
                  onPressed: viewModel.decrementCounter,
                ),
                Text(
                  viewModel.counter.toString(),
                  style: Theme.of(context).textTheme.headline4,
                ),
                IconButton(
                  icon: Icon(
                    Icons.add,
                    color: Colors.green,
                  ),
                  onPressed: viewModel.incrementCounter,
                ),
                IconButton(
                  icon: Icon(
                    Icons.star,
                    color: Colors.green,
                  ),
                  onPressed: () => viewModel.multiplyCounter(viewModel.counter),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
