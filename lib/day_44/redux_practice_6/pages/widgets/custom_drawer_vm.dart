import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/language_state/language_selectors.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_selectors.dart';
import 'package:exercises/day_44/redux_practice_6/store/shared/navigation_selector.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class CustomDrawerViewModel {
  final void Function(String) push;
  final void Function(Color) changeMainColor;
  final void Function(Color) changeSecondColor;
  final void Function() changeLanguage;

  CustomDrawerViewModel({
    @required this.push,
    @required this.changeMainColor,
    @required this.changeSecondColor,
    @required this.changeLanguage,
  });

  static CustomDrawerViewModel fromStore(Store<AppState> store) {
    return CustomDrawerViewModel(
      push: NavigationSelectors.push(store),
      changeMainColor: ThemeSelectors.changeMainColor(store),
      changeSecondColor: ThemeSelectors.changeSecondColor(store),
      changeLanguage: LanguageSelectors.changeLanguage(store,)
    );
  }
}
