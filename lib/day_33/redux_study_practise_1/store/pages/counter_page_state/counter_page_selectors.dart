import 'package:exercises/day_33/redux_study_practise_1/store/app/app_state.dart';
import 'package:exercises/day_33/redux_study_practise_1/store/pages/counter_page_state/counter_page_actions.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class CounterPageSelectors {
  static double getCounterValue(Store<AppState> store) {
    double counterCounter = store.state.counterPageState.counter;
    return counterCounter;
  }

  static bool getIsAppBarColorSwitch(Store<AppState> store) {
    bool isAppBarColorSwitch = store.state.counterPageState.isAppBarColorSwitch;
    return isAppBarColorSwitch;
  }

  static Color getAppBarColorValue(Store<AppState> store) {
    Color appBarColor = store.state.counterPageState.appBarColor;

    return appBarColor;
  }

  static void Function() getIncrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(IncrementAction());
  }

  static void Function() getDecrementCounterFunction(Store<AppState> store) {
    return () => store.dispatch(DecrementAction());
  }

  static void Function() getCleanCounterFunction(Store<AppState> store) {
    return () => store.dispatch(CleanCounterAction());
  }

//  static void Function(Color color) getChangeAppBarColorFunction(Store<AppState> store) {
//
//    return (Color color) => store.dispatch(ChangeAppBarColorAction(newColor: color));
//  }
  static void Function() getChangeAppBarColorFunction(Store<AppState> store) {
    return () => store.dispatch(ChangeAppBarColorAction());
  }

  static void Function(double counter) getMultiplyCounterFunction(Store<AppState> store) {
    return (double counter) {
      double counter = store.state.counterPageState.counter;
      store.dispatch(MultiplyCounterAction(counter2: counter));
    };
  }
}
