import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';

class NavigationSelectors {
  static void Function(String route) push(Store<AppState> store) {
    return (String route) {
      return store.dispatch(NavigateToAction.push(route));
    };
  }
}
