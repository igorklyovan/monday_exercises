import 'package:exercises/day_4/widget_of_the_week/my_stack.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(WeekWidget());

class WeekWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          dividerTheme: DividerThemeData(space: 50, thickness: 5, color: Colors.black, indent: 80, endIndent: 80)),
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.all(25),
              color: Colors.green,
              height: 80,
              width: 80,
            ),
            Spacer(
              flex: 1,
            ),
            Container(
              color: Colors.red,
              height: 80,
              width: 80,
            ),
            Spacer(
              flex: 2,
            ),
            Container(
              color: Colors.yellow,
              height: 80,
              width: 80,
            ),
            Divider(),
            MyStack()
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.settings),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      ),
    );
  }
}
