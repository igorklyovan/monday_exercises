import '../res/consts.dart';
import 'package:http/http.dart' as http;

class UserService {
  Future<dynamic> getUsers() async {
    var response = await http.get(Constants.kUserUrl);
    return response.body;
  }
}
