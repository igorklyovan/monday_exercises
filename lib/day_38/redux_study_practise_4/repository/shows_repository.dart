import 'dart:convert';

import 'package:exercises/day_38/redux_study_practise_4/model/show.dart';
import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/service/shows_service.dart';


class ShowsRepository {
  Future<List<Show>> getShows(String showsName) async {
    List<Show> shows = [];

    var response = await ShowsService().getShows(showsName);
    final jsonData = json.decode(response);

    for (int i = 0; i < jsonData.length; i++) {
      shows.add(Show.fromJson(jsonData[i]['show']));
    }

    return shows;
  }
}
