import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/main_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime _selectedDate = DateTime.now();
  TimeOfDay _selectedTime = TimeOfDay(hour: 12, minute: 00);
  double _currentSliderValue = 0;
  Gender _selectedGender = Gender.male;

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2015),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  void _presentTimePicker() {
    showTimePicker(context: context, initialTime: TimeOfDay(hour: 00, minute: 00)).then((pickedTime) {
      if (pickedTime == null) {
        return;
      }
      setState(() {
        _selectedTime = pickedTime;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FlatButton(
            child: Text('Choose time!'),
            onPressed: _presentTimePicker,
          ),
          FlatButton(
            child: Text('Choose date!'),
            onPressed: _presentDatePicker,
          ),
          Slider(
            value: _currentSliderValue,
            min: 0,
            max: 1,
            divisions: 5,
            label: _currentSliderValue.toString(),
            onChanged: (double value) {
              setState(() {
                _currentSliderValue = value;
              });
            },
          ),
          DropdownButton<String>(
            value: _selectedGender == Gender.male ? 'Male' : 'Female',
            icon: Icon(Icons.arrow_downward),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (String newValue) {
              setState(() {
                newValue == 'Male' ? _selectedGender = Gender.male : _selectedGender = Gender.female;
              });
            },
            items: ['Male', 'Female'].map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem(
                child: Text(value),
                value: value,
              );
            }).toList(),
          ),
          RaisedButton(
              child: Icon(Icons.save),
              onPressed: () {
                final myModel = MainModel(
                    date: _selectedDate, time: _selectedTime, gender: _selectedGender, smallValue: _currentSliderValue);
                print(myModel.toString());
              })
        ],
      ),
    );
  }
}
