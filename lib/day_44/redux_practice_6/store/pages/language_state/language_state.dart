import 'dart:collection';

import 'package:exercises/day_44/redux_practice_6/store/app/reducer.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/language_state/language_actions.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_actions.dart';
import 'package:flutter/material.dart';

class LanguageState {
  final String mainText;
  final String anotherText;
  final String thirdText;

  LanguageState({
    @required this.mainText,
    @required this.anotherText,
    @required this.thirdText,
  });

  factory LanguageState.initial() {
    return LanguageState(
      mainText: 'HomePage',
      anotherText: 'AnotherPage',
      thirdText: 'ThirdPage',
    );
  }

  LanguageState copyWith({
    String mainText,
    String anotherText,
    String thirdText,
  }) {
    return LanguageState(
      mainText: mainText ?? this.mainText,
      anotherText: anotherText ?? this.anotherText,
      thirdText: thirdText ?? this.thirdText,
    );
  }

  LanguageState reducer(dynamic action) {
    print('${action.runtimeType}');
    return Reducer<LanguageState>(
      actions: HashMap.from({
        ChangeLanguageAction: (dynamic action) => _changeLanguage(),
      }),
    ).updateState(action, this);
  }

  LanguageState _changeLanguage() {
    return copyWith(
      mainText: 'Главная страница',
      thirdText: 'Третья страница',
      anotherText: 'Другая страница',
    );
  }
}
