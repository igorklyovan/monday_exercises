import 'package:exercises/day_4/data_scrolling/models/car.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() => runApp(ScoresList());

class ScoresList extends StatelessWidget {
  final List<Car> _cars = [
    Car(
        id: 't1',
        carName: 'Volkswagen Golf 4',
        carColor: 'Grey',
        manufactureDate: DateTime.parse('2001-08-29'),
        carCost: 5500),
    Car(
        id: 't2',
        carName: 'Volkswagen Golf 3',
        carColor: 'Blue',
        manufactureDate: DateTime.parse('1998-08-29'),
        carCost: 3500),
    Car(id: 't3',
        carName: 'Supra 4',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2001-07-10'),
        carCost: 5500),
    Car(
        id: 't4',
        carName: 'Bmw 10',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2012-08-29'),
        carCost: 2000.99),
    Car(id: 't5',
        carName: 'Bmw 4',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2012-08-29'),
        carCost: 3000.99),
    Car(
        id: 't6',
        carName: 'Honda 4',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2012-08-29'),
        carCost: 5000.99),
    Car(
        id: 't7',
        carName: 'Honda 3',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2012-08-29'),
        carCost: 1000.99),
    Car(
        id: 't8',
        carName: 'Bmw 8',
        carColor: 'Green',
        manufactureDate: DateTime.parse('20113-08-29'),
        carCost: 4500.99),
    Car(id: 't9',
        carName: 'Bmw 3',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2012-08-29'),
        carCost: 3500.99),
    Car(
        id: 't10',
        carName: 'Nissan 2',
        carColor: 'Yellow',
        manufactureDate: DateTime.parse('2006-08-29'),
        carCost: 3000.99),
    Car(
        id: 't11',
        carName: 'Nissan 3',
        carColor: 'Green',
        manufactureDate: DateTime.parse('2010-08-29'),
        carCost: 8000.99),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Cars',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Cars List'),
          ),
          body: Container(
            height: double.infinity,
            child: ListView.builder(
              itemBuilder: (ctx, index) {
                return Card(
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 15,
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.blue,
                            width: 2,
                          ),
                        ),
                        padding: EdgeInsets.all(10),
                        child: Text(
                          '\$${_cars[index].carCost}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            _cars[index].carName,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            DateFormat.yMMMd().format(_cars[index].manufactureDate),
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Row(
                            children: [Text('Color: ',style:TextStyle(fontSize: 12,color: Colors.green, fontWeight: FontWeight.bold),),
                              Text(
                                '${_cars[index].carColor}',
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          ))
                    ],
                  ),
                );
              },
              itemCount: _cars.length,
            ),
          ),
        ));
  }
}
