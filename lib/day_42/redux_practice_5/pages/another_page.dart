import 'package:exercises/day_42/redux_practice_5/pages/widgets/custom_drawer.dart';
import 'package:exercises/day_42/redux_practice_5/pages/another_page_vm.dart';
import 'package:exercises/day_42/redux_practice_5/store/app/app_state.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:flutter/material.dart';

class AnotherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: CustomDrawer(),
      backgroundColor: Colors.deepOrange,
      body: StoreConnector<AppState, AnotherPageViewModel>(
        converter: AnotherPageViewModel.fromStore,
        builder: (BuildContext context, AnotherPageViewModel viewModel) {
          return Center(
            child: Text(
              '${viewModel.counter/2}',
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          );
        },
      ),
    );
  }
}
