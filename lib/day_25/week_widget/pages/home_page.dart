import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_25/week_widget/pages/my_draggable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isColor = false;
  bool _isOpacity = false;
  bool _isPositioned = false;
  bool _isPadding = false;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AnimatedPadding(
              duration: const Duration(seconds: 2),
              padding: _isPadding ? EdgeInsets.only(bottom: 50) : EdgeInsets.only(bottom: 10),
              child: AnimatedContainer(
                height: deviceSize.height * 0.2,
                width: deviceSize.width * 0.9,
                curve: Curves.bounceIn,
                duration: Duration(seconds: 1),
                child: AnimatedOpacity(
                  opacity: _isOpacity ? 1 : 0,
                  duration: Duration(seconds: 2),
                  child: Text(
                    'Hello world! ',
                    textAlign: TextAlign.center,
                  ),
                ),
                color: _isColor ? Colors.redAccent : Colors.blue,
              ),
            ),
            RaisedButton(
                child: Icon(Icons.animation),
                onPressed: () {
                  setState(() {
                    _isColor = !_isColor;
                    _isOpacity = !_isOpacity;
                  });
                }),
            InkWell(
              onTap: () {
                setState(() {
                  _isPositioned = !_isPositioned;
                  _isPadding = !_isPadding;
                });
              },
              child: Stack(
                children: [
                  Container(
                    color: Colors.blue,
                    height: deviceSize.height * 0.3,
                    width: deviceSize.width * 0.5,
                  ),
                  Positioned(
                    bottom: 20,
                    right: 10,
                    child: Text(
                      "U gay",
                      style: TextStyle(fontSize: deviceSize.height * 0.02),
                    ),
                  ),
                  AnimatedPositioned(
                      right: _isPositioned ? 10 : 30,
                      bottom: _isPositioned ? 20 : 40,
                      child: Container(
                        color: Colors.red,
                        height: deviceSize.height * 0.02,
                        width: deviceSize.width * 0.15,
                      ),
                      duration: const Duration(seconds: 2))
                ],
              ),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => MyDraggable(),
                  ),
                );
              },
              child: Text(
                'Draggable',
                style: TextStyle(fontSize: 25),
              ),
            )
          ],
        ),
      ),
    );
  }
}
