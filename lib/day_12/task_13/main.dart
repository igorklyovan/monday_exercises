import 'package:exercises/day_12/task_13/helpers/dialog_helper.dart';

import './default_error_dialog.dart';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: MyHomePage());
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final dialog = DefaultErrorDialog(Dialog(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: 200,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                  child: Text(
                'You have an error!',
                style: TextStyle(fontSize: 25),
              )),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Text(
                "Please check your settings or do nothing !",
                style: TextStyle(color: Colors.black),
              )),
              SizedBox(
                height: 25,
              ),
              SizedBox(
                width: 320.0,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "Close",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  color: Colors.red,
                ),
              )
            ],
          ),
        ),
      ),
    ));

    return Scaffold(
      body: Center(
        child: RaisedButton(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Colors.blue)),
          color: Colors.green,
          child: Text('Tap me !'),
          onPressed: () {
            DialogHelper.show(dialog, context);
          },
        ),
      ),
    );
  }
}
