//import 'package:Provider_template_v1/repositories/auth_repository.dart';
import 'package:flutter/material.dart';

class UnknownPage extends StatelessWidget {
  @override
  // TODO(Alex): remove unused functions from template
  // TODO(Ihor): done
  /// also, make this page stateless

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        child: Text(
          'Unknown Page',
          style: TextStyle(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
