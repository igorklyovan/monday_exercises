import 'package:exercises/day_19/practise_8_localization/dictionary/dictionary_classes/general_language.dart';
import 'package:flutter/cupertino.dart';

class Language {
  final GeneralLanguage generalLanguage;

  const Language({
    @required this.generalLanguage,
  });
}
