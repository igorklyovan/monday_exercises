import 'package:exercises/day_35/redux_study_practise_2/model/brewer.dart';
import 'package:exercises/day_35/redux_study_practise_2/repository/brewery_repository.dart';
import 'package:exercises/day_35/redux_study_practise_2/store/app/app_state.dart';
import 'package:exercises/day_35/redux_study_practise_2/store/pages/counter_page_state/counter_page_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class CounterPageStateEpics {

  static final indexEpic = combineEpics<AppState>([
    getDataEpic,
//    getData2Epic,
  ]);

  static Stream<dynamic> getDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetBrewerAction>().asyncMap((event) async{
      print('EPIC');

//      return AddBrewerAction(brewers: getListFromBody((await http.get('')).body));
      List<Brewer> brewers = await BreweryRepository().getBreweries();
    return AddBrewerAction(brewers: brewers);
    });
  }


}