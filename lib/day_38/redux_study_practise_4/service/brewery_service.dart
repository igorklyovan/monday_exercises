import 'package:exercises/day_38/redux_study_practise_4//res/consts.dart';
import 'package:http/http.dart' as http;

class BreweryService {
  Future<dynamic> getBreweries() async {
    var response = await http.get('${Constants.kkBreweryUrl}');
    return response.body;
  }
}
