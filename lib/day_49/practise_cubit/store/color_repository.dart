import 'package:flutter/material.dart';

class ColorRepository {
  ColorRepository(this.color);

  final Color color;
}
