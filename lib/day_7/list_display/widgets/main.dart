import 'package:exercises/day_7/list_display/widgets/my_grid_view.dart';
import 'package:flutter/cupertino.dart';

import '../models/human.dart';
import 'package:flutter/material.dart';
import '../humans_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: MyListView(),
      ),
    );
  }
}

class MyListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          MyListView(),
          Divider(
            thickness: 5,
            indent: 20,
            endIndent: 20,
            height: 20,
          ),
          MyGridView()
        ],
      ),
    );
  }
}
