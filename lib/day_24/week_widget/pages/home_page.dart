import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_25/week_widget/pages/my_draggable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isShow = false;
  bool isCircular = false;
  bool isCupertino = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            RaisedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  child: AlertDialog(
                    title: Text('Alert'),
                    elevation: 5,
                    content: Text("What r u doing ?"),
                    actions: [
                      FlatButton(
                        child: Text('Yes'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                );
              },
              child: Icon(Icons.add_alert),
            ),
            Builder(
              builder: (ctx) {
                return RaisedButton(
                  onPressed: () {
                    Scaffold.of(ctx).showSnackBar(SnackBar(content: Text('WTF!!')));
                    setState(() {
                      isShow = !isShow;
                    });
                  },
                  child: Icon(Icons.info),
                );
              },
            ),
            isCircular
                ? CircularProgressIndicator()
                : SizedBox(
                    height: 10,
                  ),
            isCupertino
                ? CupertinoActivityIndicator()
                : SizedBox(
                    height: 10,
                  ),
            isShow
                ? CupertinoActionSheet(
                    actions: [
                      CupertinoActionSheetAction(
                          onPressed: () {
                            setState(() {
                              isCircular = !isCircular;
                            });
                          },
                          child: Text('Circular indicator')),
                      CupertinoActionSheetAction(
                          onPressed: () {
                            setState(() {
                              isCupertino = !isCupertino;
                            });
                          },
                          child: Text('Cupertino indicator'))
                    ],
                    cancelButton: CupertinoActionSheetAction(
                      child: Text('Cancel'),
                      onPressed: () {
                        setState(() {
                          isShow = false;
                        });
                      },
                    ),
                  )
                : const SizedBox(height: 20)
          ],
        ),
      ),
    );
  }
}
