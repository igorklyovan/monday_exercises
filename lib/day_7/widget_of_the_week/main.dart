import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          margin: EdgeInsets.all(20),
          child: WeekWidget(),
        ),
      ),
    );
  }
}

class WeekWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Column(
      children: [
        Container(
          height: mediaQuery.size.height * 0.5,
          width: mediaQuery.size.width * 0.8,
          margin: EdgeInsets.all(20),
          child: ListView(
            children: [
              for (var i = 0; i < 10; i++)
                ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text('This my ${i} ListTile'),
                  subtitle: Text('This my ${i} Subtitile'),
                  trailing: Icon(Icons.delete),
                ),
            ],
          ),
        ),
        const SizedBox(
            height: 50,
            child: const Card(
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('I am a  SizedBox with Card '),
                ))),
        const SizedBox(
          height: 20,
        ),
        const SelectableText(
          'Select me!',
          style: TextStyle(color: Colors.purple),
          showCursor: true,
          toolbarOptions: ToolbarOptions(copy: true),
        )
      ],
    );
  }
}
