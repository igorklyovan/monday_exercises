import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum Gender {
  male,
  female,
}

class MainModel {
  DateTime date;
  TimeOfDay time;
  double smallValue;
  Gender gender;

  @override
  String toString() {
    return 'MainModel{date: ${DateFormat.yMd().format(date)}, time: $time, smallValue: $smallValue, gender: $gender}';
  }

  MainModel({@required this.date, @required this.time, @required this.smallValue, @required this.gender});
}
