import 'package:exercises/day_44/redux_practice_6/store/pages/language_state/language_state.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_state.dart';
import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  ThemeState themeState;
  LanguageState languageState;

  AppState({
    @required this.themeState,
    @required this.languageState,
  });

  factory AppState.initial() {
    return AppState(
      themeState: ThemeState.initial(),
      languageState: LanguageState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    return AppState(
      themeState: state.themeState.reducer(action),
      languageState: state.languageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([]);
}
