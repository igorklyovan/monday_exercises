import 'package:exercises/day_50/bloc_practise_2/model/splash_photo.dart';
import 'dart:convert';

import 'package:exercises/day_50/bloc_practise_2/service/photo_service.dart';

class PhotoRepository {
  Future<List<SplashPhoto>> getPhotos() async {
    List<SplashPhoto> photos = [];
    print('222');

    var response = await PhotoService().getPhotos();
    List jsonData =  [];
   jsonData = json.decode(response);

    for (int i = 0; i < jsonData.length; i++) {
      photos.add(SplashPhoto.fromJson(jsonData[i]));
    }
    return photos;
  }
}
