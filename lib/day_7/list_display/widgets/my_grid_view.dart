import 'package:flutter/material.dart';

import '../humans_list.dart';

class MyGridView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final orientation = MediaQuery.of(context).orientation;

    return Expanded(
      child: Container(
        height: (mediaQuery.size.height - mediaQuery.padding.top) * 0.4,
        child: GridView.builder(
            itemCount: humans.length,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: orientation == Orientation.portrait ? 3 : 4),
            itemBuilder: (ctx, index) {
              return Card(
                elevation: 5,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GridTile(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.portrait),
                        Text('${humans[index].firstName} ${humans[index].lastName}'),
                        Text(
                          ' ${humans[index].age}',
                        ),
                      ],
                    ),
                    footer: Center(
                        child: Text(
                      "${humans[index].profession}",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
