import 'package:exercises/day_38/redux_study_practise_4/model/show.dart';
import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/repository/shows_repository.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/app/app_state.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/home_page_state/home_page_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class HomePageStateEpics{
  static final indexEpic = combineEpics<AppState>([
    getDataEpic,
//    getData2Epic,
  ]);

  static Stream<dynamic> getDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetShowsAction>().asyncMap((action) async{

//      return AddBrewerAction(brewers: getListFromBody((await http.get('')).body));
      List<Show> shows = await ShowsRepository().getShows(action.showName);
      return AddShowsAction(shows: shows);
    });
  }

}