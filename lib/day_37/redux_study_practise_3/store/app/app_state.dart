import 'package:flutter/foundation.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  AppState();

  factory AppState.initial() {
    return AppState();
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    return AppState();
  }

  static final getAppEpic = combineEpics<AppState>([]);
}
