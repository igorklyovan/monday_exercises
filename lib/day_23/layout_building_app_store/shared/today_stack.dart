import 'package:flutter/material.dart';

class TodayStack extends StatelessWidget {
  IconData icon;
  String image;
  String title;
  String genre;
  String type;
  double price;
  bool isRounded;

  TodayStack({
    @required this.icon,
    @required this.image,
    @required this.title,
    @required this.genre,
    @required this.type,
    @required this.price,
    @required this.isRounded,
  });

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Material(
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(
              isRounded ? 15.0 : 0.0,
            ),
            child: Image.asset(
              image,
              fit: BoxFit.cover,
              height: deviceSize.height * 0.6,
            ),
          ),
          Positioned(
            bottom: 100.0,
            right: 10.0,
            child: SizedBox(
              width: 120.0,
              child: Text(
                type,
                maxLines: 3,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 2.0,
            right: 10.0,
            left: 10.0,
            child: Container(
              height: deviceSize.height * 0.08,
              width: deviceSize.width * 0.9,
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: deviceSize.height * 0.05,
                    width: deviceSize.width * 0.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Text(
                        '\$$price',
                        style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: deviceSize.height * 0.05,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              title,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14.0,
                              ),
                            ),
                            Text(
                              genre,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: deviceSize.height * 0.01,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                          color: Color(0xFFFFFAF6),
                        ),
                        padding: EdgeInsets.all(10),
                        child: Icon(
                          icon,
                          size: 26.0,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
