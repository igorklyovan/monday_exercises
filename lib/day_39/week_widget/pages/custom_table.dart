import 'package:flutter/material.dart';

class CustomTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Table(
        columnWidths: {1: FractionColumnWidth(.2)},
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: [
          TableRow(children: [
            Container(
              height: 200,
              width: 200,
              color: Colors.blue,
            ),
            FlutterLogo(size: 30),
            FadeInImage.assetNetwork(
                height: 200,
                placeholder: 'assets/images/snowman.png',
                image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M'
                    'Xx8aHVtYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80'),
          ]),
          TableRow(children: [
            Container(
              height: 200,
              width: 200,
              color: Colors.purple,
            ),
            FlutterLogo(size: 30),
            FadeInImage.assetNetwork(
                height: 200,
                placeholder: 'assets/images/snowman.png',
                image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8M'
                    'Xx8aHVtYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80'),
          ])
        ],
      ),
    ));
  }
}
