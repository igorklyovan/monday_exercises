import 'package:flutter/material.dart';
import './pages/buttons_page.dart';
import './pages/counter_page.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Colors.grey,
            child: Text(
              "Navigation Menu",
              style: TextStyle(fontSize: 25),
            ),
          ),
          Divider(),
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: InkWell(
              child: Container(
                height: 50,
                child: Card(
                  elevation: 5,
                  color: Colors.white,
                  child: Center(child: Text('Counter')),
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacementNamed(CounterPage.routeName);
              },
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: InkWell(
              child: Container(
                height: 50,
                child: Card(
                  elevation: 5,
                  child: Center(child: Text('Buttons')),
                ),
              ),
              onTap: () {
                Navigator.of(context).pushReplacementNamed(ButtonsPage.routeName);
              },
            ),
          ),
        ],
      ),
    );
  }
}
