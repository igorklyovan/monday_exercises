import 'package:flutter/material.dart';

enum Gender { male, female }

class UserDTO {
  Gender gender;
  String firstName;
  String lastName;
  String city;
  String email;
  String phone;
  String imgUrl;

  UserDTO(
      {@required this.gender,
      @required this.firstName,
      @required this.lastName,
      @required this.city,
      @required this.email,
      @required this.phone,
      @required this.imgUrl});

  factory UserDTO.fromJson(Map<String, dynamic> jsonData) {
    return UserDTO(
        gender: jsonData['gender'] == 'male' ? Gender.male : Gender.female,
        firstName: jsonData['name']['first'],
        lastName: jsonData['name']['last'],
        city: jsonData['location']['city'],
        email: jsonData['email'],
        phone: jsonData['phone'],
        imgUrl: jsonData['picture']['medium']);
  }
}
