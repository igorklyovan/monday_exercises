import 'package:exercises/day_23/layout_building_app_store/shared/today_stack.dart';
import 'package:flutter/material.dart';

class DummyData {
  static const String dummyText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
      'Nunc tempus mauris non ante efficitur, quis convallis ipsum hendrerit. '
      'Proin lacus ligula, rhoncus nec cursus sed, gravida vel dui. '
      'Sed lacinia arcu ac ante lacinia porttitor. Proin sed odio accumsan, molestie eros nec, dapibus quam. '
      'Sed vehicula vitae eros eu consectetur. Donec convallis quis nisi et luctus. '
      'In pharetra ac ligula nec convallis. Aenean vel fermentum urna, ac viverra risus. '
      'Aliquam semper accumsan leo.Nullam cursus eros convallis, '
      'elementum dolor et, egestas tellus. '
      'Etiam mi magna, accumsan nec placerat quis, egestas a metus. '
      'Etiam nec felis augue. Sed accumsan pretium ligula, '
      'sit amet tempor libero volutpat nec. Vivamus accumsan nisl eros, '
      'ac malesuada lorem dignissim convallis. Nunc hendrerit lacus in nisl '
      'ulputate, sit amet ornare dui iaculis. Vivamus ut lectus nunc. Sed nec puru'
      's eget mauris sodales ultrices pulvinar et nisl. Aliquam erat volutpat. Suspendi'
      'sse volutpat magna sed nulla semper vehicula. Sed non nulla venenatis, '
      'finibus turpis tempus, tristique orci. Duis vestibulum elit quis '
      'diam malesuada pharetra. Proin tempor nibh et nunc cursus aliquet '
      'non hendrerit libero. Fusce hendrerit commodo diam vel interdum. '
      'Donec tincidunt nulla nec consectetur congue.'
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
      'Nunc tempus mauris non ante efficitur, quis convallis ipsum hendrerit. '
      'Proin lacus ligula, rhoncus nec cursus sed, gravida vel dui. '
      'Sed lacinia arcu ac ante lacinia porttitor. Proin sed odio accumsan, molestie eros nec, dapibus quam. '
      'Sed vehicula vitae eros eu consectetur. Donec convallis quis nisi et luctus. '
      'In pharetra ac ligula nec convallis. Aenean vel fermentum urna, ac viverra risus. '
      'Aliquam semper accumsan leo.Nullam cursus eros convallis, '
      'elementum dolor et, egestas tellus. '
      'Etiam mi magna, accumsan nec placerat quis, egestas a metus. '
      'Etiam nec felis augue. Sed accumsan pretium ligula, '
      'sit amet tempor libero volutpat nec. Vivamus accumsan nisl eros, '
      'ac malesuada lorem dignissim convallis. Nunc hendrerit lacus in nisl '
      'ulputate, sit amet ornare dui iaculis. Vivamus ut lectus nunc. Sed nec puru'
      's eget mauris sodales ultrices pulvinar et nisl. Aliquam erat volutpat. Suspendi'
      'sse volutpat magna sed nulla semper vehicula. Sed non nulla venenatis, '
      'finibus turpis tempus, tristique orci. Duis vestibulum elit quis '
      'diam malesuada pharetra. Proin tempor nibh et nunc cursus aliquet '
      'non hendrerit libero. Fusce hendrerit commodo diam vel interdum. '
      'Donec tincidunt nulla nec consectetur congue.';

  static const IconData icon = Icons.tag_faces;
  static const String image = 'assets/images/game_of_the_day.jpg';
  static const String title = 'Wilmot\`s Warehouse';
  static const String genre = 'Puzzle';
  static const String type = 'GAME OF THE DAY';
  static const double price = 4.99;

  static const bool isRounded = true;
}
