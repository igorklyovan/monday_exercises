import 'dart:collection';

import 'package:exercises/day_44/redux_practice_6/store/app/reducer.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_actions.dart';
import 'package:flutter/material.dart';

class ThemeState {
  final Color mainColor;
  final Color secondColor;

  ThemeState({
    @required this.mainColor,
    @required this.secondColor,
  });

  factory ThemeState.initial() {
    return ThemeState(mainColor: Colors.purple, secondColor: Colors.orange);
  }

  ThemeState copyWith({Color mainColor, Color secondColor}) {
    return ThemeState(
      mainColor: mainColor ?? this.mainColor,
      secondColor: secondColor ?? this.secondColor,
    );
  }

  ThemeState reducer(dynamic action) {
    print('${action.runtimeType}');
    return Reducer<ThemeState>(
      actions: HashMap.from({
        ChangeMainColorAction: (dynamic action) => _changeMainColor(action.color),
        ChangeSecondColorAction: (dynamic action) => _changeSecondColor(action.color),
      }),
    ).updateState(action, this);
  }

  ThemeState _changeMainColor(Color color) {
    return copyWith(mainColor: color);
  }

  ThemeState _changeSecondColor(Color color) {
    return copyWith(secondColor: color);
  }
}
