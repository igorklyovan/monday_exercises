import 'package:exercises/day_27/api_practise_3/res/consts.dart';
import 'package:http/http.dart' as http;

class ProductService {
  Future<dynamic> getProducts() async {
    var response = await http.get('${Constants.kMkBreweryUrl}');
    return response.body;
  }
}
