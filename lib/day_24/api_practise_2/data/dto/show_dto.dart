import 'package:flutter/material.dart';

class ShowDTO {
  int id;
  String url;
  String name;
  String language;
  List<String> genres;
  double rating;
  String image;

  ShowDTO({
    @required this.id,
    @required this.url,
    @required this.name,
    @required this.language,
    @required this.genres,
    @required this.rating,
    @required this.image,
  });

  factory ShowDTO.fromJson(Map<String, dynamic> jsonData) {
    return ShowDTO(
      id: jsonData['id'],
      name: jsonData['name'],
      genres: List.from(jsonData['genres']),

      image: jsonData['image'] == null
          ? 'https://acceleratetv.com/wip/wp-content/plugins/masvideos/assets/images/placeholder.png':jsonData['image']['medium'],
      language: jsonData['language'],
      rating: jsonData['rating']['average'],
      url: jsonData['url'],);
  }
}
