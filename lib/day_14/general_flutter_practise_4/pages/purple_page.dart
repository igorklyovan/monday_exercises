import 'package:flutter/material.dart';

import '../main_drawer.dart';

class PurplePage extends StatelessWidget {
  static const String routeName = '/purple';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: MainDrawer(),
      body: Container(
        color: Colors.purple,
      ),
    );
  }
}
