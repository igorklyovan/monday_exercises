class Routes {
  static const kHomePage = '/home';
  static const kAnotherPage = '/another';
  static const kThirdPage = '/third';
  static const kFourthPage = '/fourth';
}
