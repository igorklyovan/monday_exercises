import 'dart:math';
import 'package:exercises/day_22/solid_practise/shape.dart';

class Circle implements Shape {
  int radius = 5;

  @override
  double area() {
    return pi * (radius * radius);
  }
}
