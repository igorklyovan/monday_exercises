import 'package:exercises/day_44/redux_practice_6/store/app/app_state.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/language_state/language_selectors.dart';
import 'package:exercises/day_44/redux_practice_6/store/pages/theme_state/theme_selectors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class HomePageViewModel {
  final Color mainColor;
  final Color secondColor;
  final String text;
  final void Function(Color) changeMainColor;
  final void Function(Color) changeSecondColor;

  HomePageViewModel({
    @required this.mainColor,
    @required this.secondColor,
    @required this.changeMainColor,
    @required this.changeSecondColor,
    @required this.text
  });

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      mainColor: ThemeSelectors.getMainColor(store),
      secondColor: ThemeSelectors.getSecondColor(store),
      changeMainColor: ThemeSelectors.changeMainColor(store),
      changeSecondColor: ThemeSelectors.changeSecondColor(store),
      text: LanguageSelectors.getMainName(store),
    );
  }
}
