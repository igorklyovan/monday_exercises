import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/pages/another_page.dart';
import 'file:///C:/Users/AppVesto/Projects/exercises/lib/day_38/redux_study_practise_4/pages/home_page.dart';
import 'package:exercises/day_38/redux_study_practise_4/res/routes.dart';
import 'package:exercises/day_38/redux_study_practise_4/store/pages/unknown_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:page_transition/page_transition.dart';

class RouteService {
  RouteService._privateConstructor();

  static final RouteService _instance = RouteService._privateConstructor();

  static RouteService get instance => _instance;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    print(settings.name);

    switch (settings.name) {
      case Routes.kHomePage:
        return PageTransition(
          child: HomePage(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      case Routes.kAnotherPage:
        return PageTransition(
          child: AnotherPage(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      default:
        return _defaultRoute(
          settings: settings,
          page: UnknownPage(),
        );
    }
  }

  static MaterialPageRoute _defaultRoute({RouteSettings settings, Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
