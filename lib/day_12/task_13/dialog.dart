import 'package:flutter/material.dart';

abstract class MyDialog {
  Widget widget;

  void show(BuildContext ctx) {}
}
