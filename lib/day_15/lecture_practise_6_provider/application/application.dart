import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../pages/buttons_page.dart';
import '../pages/counter_page.dart';
import '../providers/counter_provider.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: CounterProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: CounterPage.routeName,
        routes: {CounterPage.routeName: (ctx) => CounterPage(), ButtonsPage.routeName: (ctx) => ButtonsPage()},
      ),
    );
  }
}
