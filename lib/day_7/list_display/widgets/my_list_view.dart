import 'package:flutter/material.dart';

import '../humans_list.dart';

class MyListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Expanded(
      child: Container(
        height: (mediaQuery.size.height - mediaQuery.padding.top) * 0.5,
        child: ListView.builder(
          itemBuilder: (ctx, index) {
            return ListTile(
              leading: Icon(Icons.accessibility),
              title: Text('${humans[index].firstName} ${humans[index].lastName}'),
              subtitle: Text("${humans[index].profession}"),
              trailing: Text("age: ${humans[index].age}"),
            );
          },
          itemCount: humans.length,
        ),
      ),
    );
  }
}
