import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ArticleAppBar extends StatefulWidget implements PreferredSizeWidget {
  bool isShow;
  final bool isAppBarScroll;

  ArticleAppBar({
    @required this.isShow,
    @required this.isAppBarScroll,
  });

  @override
  _ArticleAppBarState createState() => _ArticleAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(100.0);
}

class _ArticleAppBarState extends State<ArticleAppBar> {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return SafeArea(
      child: Container(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Navigator.of(context).pop();
            setState(() {
              widget.isShow = false;
            });
          },
          child: widget.isShow
              ? Row(
                  children: [
                    SizedBox(width: 20.0),
                    Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: widget.isAppBarScroll ? Color(0xFF574F58) : Colors.white38,
                        ),
                        padding: EdgeInsets.all(2),
                        child: Icon(
                          Icons.close,
                          color: widget.isAppBarScroll ? Colors.white : Colors.black,
                        )),
                  ],
                )
              : SizedBox(),
        ),
      ),
    );
  }
}
