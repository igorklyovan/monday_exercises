import 'package:exercises/day_18/provider_listview/my_button.dart';
import 'package:exercises/day_18/provider_listview/providers/list_view_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: deviceSize.height * 0.1),
          Consumer<ListViewProvider>(
            builder: (ctx, listData, child) => Container(
              height: deviceSize.height * 0.5,
              width: double.infinity,
              child: listData.list.length <= 0
                  ? Center(
                      child: Text('Press + button to generate list data!'),
                    )
                  : ListView.builder(
                      itemCount: listData.list.length,
                      itemBuilder: (_, index) {
                        return ListTile(
                          leading: Text('$index'),
                          title: Text('human ${listData.list[index]}'),
                          trailing: Icon(Icons.update),
                        );
                      }),
            ),
          ),
          SizedBox(
            height: deviceSize.height * 0.3,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              MyButton(Icons.add, ButtonAction.generateLength, Colors.green),
              MyButton(Icons.update, ButtonAction.updateSort, Colors.yellow),
              MyButton(Icons.remove, ButtonAction.removeRandom, Colors.red)
            ],
          )
        ],
      ),
    );
  }
}
