import 'package:flutter/material.dart';

class Items with ChangeNotifier {
  List<String> _items = List<String>.generate(100, (index) => '$index');

//  List<String> _items = ['1','2','3'];
  String _selectedItem = '1';

  List<String> get items {
    return [..._items];
  }

  String get selectedItem => _selectedItem;

  set selectedItem(String value) {
    _selectedItem = value;
    notifyListeners();
  }
}
